//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CNewWizPage
// File     :   NewWizPage.h
// Author   :   isc03meal
// Date     :   02.12.2003
// Version  :   1.0
//
//----------------------------------------------------------------------------------
#pragma once

class CNewWizDialog;

class CNewWizPage : public CDialog
{
  friend class CNewWizDialog;

// Construction
public:
	CNewWizPage(CWnd* pParent = NULL);   // standard constructor
	CNewWizPage(LPCTSTR lpszTemplateName, CWnd* pParent = NULL);
	CNewWizPage(UINT nIDTemplate, CWnd* pParent = NULL);
    virtual ~CNewWizPage();

// Attributes
public:
// Font for titles
	CFont m_LargeFont; 

protected:
    CNewWizDialog *m_pParent;

private:
// Tell the dialog has been created
    BOOL m_bCreated;
// Tell the dialog is the active page
    BOOL m_bActive;
// resource ID for thie page
    UINT m_nDialogID;


// Operations
public:
    virtual BOOL OnCreatePage();
    virtual void OnDestroyPage();

// these functions are the same as CPropertyPage
    virtual void OnCancel();
    virtual BOOL OnKillActive();
    virtual void OnSetActive();
    virtual BOOL OnQueryCancel( );
    virtual LRESULT OnWizardBack();
    virtual CString GetData();
    virtual void SetData(CString* data);
    virtual LRESULT OnWizardNext();
    virtual BOOL OnWizardFinish();
    
// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);

// Implementation
protected:
	virtual BOOL OnInitDialog();
//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()
};