//-----------------------------------------------------------------------
//	Project PixGrabber
//-----------------------------------------------------------------------
//
// CLASS	:	CThumbnailButton
// File		:	ThumbnailButton.cpp
// Author	:	Thierry "real" Haven
//				Based on Rex Fong rexfong@bac98.net control (www.codeproject.com)
//				New features implemented in this control thanks to PixGrabber are :
//				* Jpg and Gif loading
//				* New mouse handling
//				* Delete thumbnails on request
//				* Fixed scrollbar bugs
//				* Fixed message bug (SendMessageTimeout(HWND_BROADCAST...) didn't work
//				  for some reason..)
//				* Fixed display bug thanks to SetCapture / ReleaseCapture
//				-> still have to fix some update/display artifacts FIXME
// Date		:	09.12.2003
// Version	:	0.1
// Descr	:	This class represents a thumbnail button. Thumbnailbuttons
//				are attributes of a CThumbNailControl. A CThumbNailControl
//				may have many CThumbnailButton. A CThumbnailButton
//				is the "real" ThumbNail since it displays a tiny picture
//				preview (unlike the picture in CPixImgView which is much
//				bigger) and user can select it by clicking it.
//				Clicking a Thumbnailbuttons causes the CPixImgView to update
//				to the currently selected picture. See how
//				PreTranslateMessage() deals with mouse events.
//-----------------------------------------------------------------------
#include "stdafx.h"

#include <math.h>

#include "ThumbNailControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RGBTHUMBCTRLBKGD RGB( 0, 0, 0 )
#define RGBTHUMBCTRLBORDER RGB( 0, 0, 0 )

#define CTHUMBNAILCONTROL_CLASSNAME _T("CThumbNailControl")

//--[MFC message mapping]------------------------------------------------
BEGIN_MESSAGE_MAP(CThumbNailControl, CWnd)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

//--[constructor]--------------------------------------------------------
CBrush  CThumbNailControl::m_bkBrush;
CThumbNailControl::CThumbNailControl()
{
	if( !RegisterWindowClass() )
		return;

	m_arPtrData.RemoveAll();

	m_nCol = 0;
	m_nRow = 0;

	m_nStartX = 0;
	m_nStartY = 0;

	m_nThumbWidth  = 0;
	m_nThumbHeight = 0;
}

CThumbNailControl::~CThumbNailControl()
{
	Reset();
}

//-----------------------------------------------------------------------
//	Function void InitializeVariables(int, int)
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		Redraw bounding box in background, depending of the thumb position.
//
// @param  int	 			DEFAULT_THUMBWIDTH
// @param  int				DEFAULT_THUMBHEIGHT
//
//-----------------------------------------------------------------------
void CThumbNailControl::InitializeVariables( int cX /* = DEFAULT_THUMBWIDTH */, 
											int cY /* = DEFAULT_THUMBHEIGHT */ )
{
	m_arPtrData.RemoveAll();

	m_nThumbWidth     = cX;
	m_nThumbHeight    = cY;

	if( ::IsWindow( m_hWnd  ) )
	{
		CRect rect;
		GetClientRect( &rect );

		m_nCol  = (int) floor ( ((float) rect.Width()) / (m_nThumbWidth + DEFAULT_SEPERATOR) );
	}

}

//-----------------------------------------------------------------------
//	Function void InitializeVariables(int, int)
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		Calculate buttons positions
//
//-----------------------------------------------------------------------
void CThumbNailControl::RecalButtonPos()
{
	CRect rect;
	GetClientRect( &rect );

	int nX=0;
	int nY=0;
	m_nRow=0;

	for( int i=0; i<m_arPtrData.GetSize(); i++ )
	{
		if( nX == 0 )
			m_nRow++;

		CThumbnailButton *pBtn = (CThumbnailButton*) m_arPtrData.GetAt(i);

		ASSERT( AfxIsValidAddress(pBtn, sizeof(CThumbnailButton), TRUE) );
		ASSERT( ::IsWindow( pBtn->m_hWnd ) );

		pBtn->SetWindowPos( NULL, 
			m_nStartX + DEFAULT_SEPERATOR+nX, 
			m_nStartY + DEFAULT_SEPERATOR+nY, 
			0, 0, SWP_NOSIZE|SWP_NOZORDER );

		nX += m_nThumbWidth + DEFAULT_SEPERATOR;

		if( ( rect.Width() - nX ) < 0.8*(m_nThumbWidth+DEFAULT_SEPERATOR) )
		{
			nX = 0;
			nY += m_nThumbHeight + DEFAULT_SEPERATOR;
		}

	}

}

//-----------------------------------------------------------------------
//	Function BOOL RegisterWindowClass()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		Does some "administrative" work with its friend the windows OS.
//		Needed because this class is a custom control, so windows has to
//		know about it.
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CThumbNailControl::RegisterWindowClass()
{
	WNDCLASS wndcls;
	HINSTANCE hInst = AfxGetInstanceHandle();

	if (!(::GetClassInfo(hInst, CTHUMBNAILCONTROL_CLASSNAME, &wndcls)))
	{
		// otherwise we need to register a new class
		m_bkBrush.CreateSolidBrush( RGBTHUMBCTRLBKGD );

		wndcls.style            = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
		wndcls.lpfnWndProc      = ::DefWindowProc;
		wndcls.cbClsExtra       = wndcls.cbWndExtra = 0;
		wndcls.hInstance        = hInst;
		wndcls.hIcon            = NULL;
		wndcls.hCursor          = AfxGetApp()->LoadStandardCursor(IDC_ARROW);
		wndcls.hbrBackground    = (HBRUSH) m_bkBrush.GetSafeHandle();
		wndcls.lpszMenuName     = NULL;
		wndcls.lpszClassName    = CTHUMBNAILCONTROL_CLASSNAME;

		if (!AfxRegisterClass(&wndcls))
		{
			AfxThrowResourceException();
			return FALSE;
		}
	}

	return TRUE;
}


//-----------------------------------------------------------------------
//	Function BOOL Add( const CString& sPath, CWnd *owner )
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		Add a thumbnail to the current thumbnail list.
//		We specify also the parent window to the thumbnail button
//		so it knows where to send messages if it is clicked.
//
//	@param : CString&			Picture name to transform into a thumbnail
//  @param : CWnd *				parent frame (actually it is the cpixthumbview class)
//
//-----------------------------------------------------------------------
void CThumbNailControl::Add( const CString& sPath, CWnd *owner )
{
	ASSERT( ::IsWindow(this->m_hWnd) );

	// Initialize Variables Not done!
	ASSERT( (m_nThumbWidth + m_nThumbHeight) > 0 );

	Invalidate( FALSE );

	long nThumbImgWidth  = (int)(0.75f*m_nThumbWidth);
	long nThumbImgHeight = (int)(0.75f*m_nThumbHeight);
	CThumbnailButton *pBtn = new CThumbnailButton( sPath, 
		nThumbImgWidth, nThumbImgHeight, owner );
	pBtn->Create( _T("CThumbnailButton"), 
		_T(""), 
		WS_CHILD|WS_VISIBLE, 
		CRect(0,0,m_nThumbWidth,m_nThumbHeight), this, 0 );

	if( pBtn->IsValid() )
	{
		VERIFY( ::IsWindow(pBtn->m_hWnd) );

		m_arPtrData.Add( (void*) pBtn);

		RecalButtonPos();
		RecalScrollBars();
	}
	else
	{
		pBtn->DestroyWindow();
		delete pBtn;
	}
}

//-----------------------------------------------------------------------
//	Function BOOL Remove( const CString& sPath)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Remove a thumbnail of the current thumbnail list.
//		(ie. free memory etc..)
//
//	@param : CString&			Picture name to transform into a thumbnail
//
//-----------------------------------------------------------------------
void CThumbNailControl::Remove( const CString& sPath)
{
	int size = (int)m_arPtrData.GetSize();
	int i,j;
	CString tmppath = sPath;

	for(i=0;i<size;i++)
	{
		CThumbnailButton *pBtn = (CThumbnailButton *)m_arPtrData.ElementAt(i);
		CString add = pBtn->GetFullpath();
		if (!lstrcmp(add.GetBuffer(), tmppath.GetBuffer()))
		{
			if (!::DeleteFile(pBtn->GetFullpath()))
			{
				::MessageBox(this->m_hWnd, "Could not delete file", 0, 0);
			}
			delete(pBtn);
			for(j=i;j<(size-1);j++)
			{
				m_arPtrData.SetAt(j, m_arPtrData.ElementAt(j+1));
			}
			m_arPtrData.RemoveAt(size-1);
	//		::MessageBox(0,sPath,0,0);
			size--;
		}
	}
	RecalButtonPos();
}

//-----------------------------------------------------------------------
//	Function void Reset()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		Reset the content of the control. No thumbnails anymore !
//
//-----------------------------------------------------------------------
void CThumbNailControl::Reset()
{
	int i;

	for(i=(int)m_arPtrData.GetSize()-1; i>=0; i-- )
	{
		CThumbnailButton *pBtn = (CThumbnailButton*) m_arPtrData.GetAt(i);
		m_arPtrData.RemoveAt(i);

		if( pBtn != NULL )
			delete pBtn;
	}

	m_arPtrData.RemoveAll();
}


//-----------------------------------------------------------------------
//	overriden Function void OnHScroll(UINT,UINT,CScrollBar*)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		Perform Horizontal Scrolling
//
// @param  UINT 			see msdn
// @param  UINT 			see msdn
// @param  CScrollBar*		see msdn
//
//-----------------------------------------------------------------------
void CThumbNailControl::OnHScroll(UINT nSBCode, UINT /* nPos */ , CScrollBar* /* pScrollBar */) 
{
	int nScrollPos = GetScrollPos32(SB_HORZ);

	switch( nSBCode )
	{
	case SB_LEFT:
		break;

	case SB_ENDSCROLL:
		RedrawWindow();
		break;

	case SB_LINELEFT :
		SetScrollPos32(SB_HORZ, nScrollPos - 1 );
		break;

	case SB_LINERIGHT:
		SetScrollPos32(SB_HORZ, nScrollPos + 1);
		break;

	case SB_PAGELEFT :
		SetScrollPos32(SB_HORZ, nScrollPos - 20 );
		break;

	case SB_PAGERIGHT:
		SetScrollPos32(SB_HORZ, nScrollPos + 20);
		break;

	case SB_RIGHT:
		break;

	case SB_THUMBPOSITION:  // Go down...
	case SB_THUMBTRACK:
		SetScrollPos32( SB_HORZ, GetScrollPos32(SB_HORZ, TRUE) );
		break;

	default:
		break;
	}

	m_nStartX = -GetScrollPos32(SB_HORZ);
	RecalButtonPos();
}


//-----------------------------------------------------------------------
//	Function void RecalScrollBars()
//-----------------------------------------------------------------------
//
// @author	:  Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		recalc scrollbars positions
//
//-----------------------------------------------------------------------
void CThumbNailControl::RecalScrollBars()
{
	CRect rect;
	GetClientRect( &rect );
	ClientToScreen( &rect );

	long nTotalWidth  = m_nThumbWidth * m_nCol + DEFAULT_SEPERATOR * ( 1 + m_nCol );
	long nTotalHeight = m_nThumbHeight * m_nRow + DEFAULT_SEPERATOR * ( 2 + m_nRow );

	long nWidDiff = nTotalWidth  - rect.Width();
	long nHeiDiff = nTotalHeight - rect.Height();

	if( nWidDiff > DEFAULT_SEPERATOR && m_arPtrData.GetSize() >= m_nCol )
	{
		SCROLLINFO si;
		memset( &si, 0, sizeof(SCROLLINFO) );

		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_PAGE | SIF_RANGE;
		si.nPage = (int)(0.25*(nWidDiff+14));
		si.nMin = 0;
		si.nMax = (int)(1.25*(nWidDiff+14));

		SetScrollInfo( SB_HORZ, &si, TRUE );

		EnableScrollBarCtrl( SB_HORZ );
		EnableScrollBar( SB_HORZ );
	}

	if( nHeiDiff > DEFAULT_SEPERATOR )
	{
		SCROLLINFO si;
		memset( &si, 0, sizeof(SCROLLINFO) );

		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_PAGE | SIF_RANGE;
		si.nPage = (int)(0.25*(nHeiDiff+14));
		si.nMin = 0;
		si.nMax = (int)(1.25*(nHeiDiff+14));

		SetScrollInfo( SB_VERT, &si, TRUE );

		EnableScrollBarCtrl( SB_VERT );
		EnableScrollBar( SB_VERT );
	}

}


//-----------------------------------------------------------------------
//	Function BOOL SetScrollPos32(int nBar, int nPos, BOOL bRedraw)
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		get scroll position...
//
// @param  int				SB_VERT / SB_HORZ
// @param  int 				new position
// @param  BOOL  			redraw flag
// @return					0 if failed
//
//-----------------------------------------------------------------------
BOOL CThumbNailControl::SetScrollPos32(int nBar, int nPos, BOOL bRedraw)
{
	SCROLLINFO si;
	si.cbSize = sizeof(SCROLLINFO);
	si.fMask  = SIF_POS;
	si.nPos   = nPos;
	return SetScrollInfo(nBar, &si, bRedraw);
}

//-----------------------------------------------------------------------
//	Function int GetScrollPos32( int nBar, BOOL bGetTrackPos )
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		get scroll position...
//
// @param  int 				scrollbar type (SB_VERT / SB_HORZ)
// @param  BOOL 			tracking ?
// @return					0 if failed, else track position
//
//-----------------------------------------------------------------------
int CThumbNailControl::GetScrollPos32( int nBar, BOOL bGetTrackPos )
{
	SCROLLINFO si;
	si.cbSize = sizeof(SCROLLINFO);

	if (bGetTrackPos)
	{
		if (GetScrollInfo(nBar, &si, SIF_TRACKPOS))
			return si.nTrackPos;
	}
	else
	{
		if (GetScrollInfo(nBar, &si, SIF_POS))
			return si.nPos;
	}

	return 0;
}

//-----------------------------------------------------------------------
//	overriden Function void OnHScroll(UINT,UINT,CScrollBar*)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		Perform Vertical Scrolling
//
// @param  UINT 			see msdn
// @param  UINT 			see msdn
// @param  CScrollBar*		see msdn
//
//-----------------------------------------------------------------------
void CThumbNailControl::OnVScroll(UINT nSBCode, UINT /*nPos*/, CScrollBar* /*pScrollBar */) 
{
	int nScrollPos = GetScrollPos32(SB_VERT);
	switch( nSBCode )
	{
	case SB_BOTTOM:
		break;

	case SB_ENDSCROLL:
		RedrawWindow();
		break;

	case SB_LINEDOWN:
		SetScrollPos32(SB_VERT, nScrollPos + 1 );
		break;

	case SB_LINEUP:
		SetScrollPos32(SB_VERT, nScrollPos - 1 );
		break;

	case SB_PAGEDOWN:
		SetScrollPos32(SB_VERT, nScrollPos + 20 );
		break;

	case SB_PAGEUP:
		SetScrollPos32(SB_VERT, nScrollPos - 20 );
		break;

	case SB_THUMBPOSITION: // Go down..
	case SB_THUMBTRACK:
		SetScrollPos32( SB_VERT, GetScrollPos32(SB_VERT, TRUE) );
		break;

	case SB_TOP:
		break;

	default:
		break;
	}

	m_nStartY = -GetScrollPos32(SB_VERT);
	RecalButtonPos();
	//Invalidate();
}

//-----------------------------------------------------------------------
//	overriden Function BOOL OnMouseWheel(UINT , short , CPoint )
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		Perform Horizontal Scrolling
//
// @param  UINT 			see msdn
// @param  short 			see msdn
// @param  CPoint			see msdn
// @return BOOL				see msdn
//
//-----------------------------------------------------------------------
BOOL CThumbNailControl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	int nScrollPos = GetScrollPos32(SB_VERT);
	int nStep = zDelta/WHEEL_DELTA;

	SetScrollPos32(SB_VERT, nScrollPos - nStep*5 );

	m_nStartY = -GetScrollPos32(SB_VERT);
	RecalButtonPos();

	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}
