//-----------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CThumbNailControl
// File		:	ThumbNailControl.h
// Author	:	Thierry "real" Haven
//				Based on Rex Fong rexfong@bac98.net control (www.codeproject.com)
//				New features implemented in this control thanks to PixGrabber are :
//				* Jpg and Gif loading
//				* New mouse handling
//				* Delete thumbnails on request
//				* Fixed scrollbar bugs
//				* Fixed message bug (SendMessageTimeout(HWND_BROADCAST...) didn't work
//				  for some reason..)
//				* Fixed display bug thanks to SetCapture / ReleaseCapture
//				-> still have to fix some update/display artifacts FIXME
//				
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once

#include "ThumbnailButton.h"

//--[class]--------------------------------------------------------------
class CThumbNailControl : public CWnd
{

//--[variables]----------------------------------------------------------
public:

enum {  DEFAULT_THUMBWIDTH  = 100, 
		DEFAULT_THUMBHEIGHT = 100,
		DEFAULT_SEPERATOR   = 4 };

	long	m_nStartX;
	long	m_nStartY;

private:
	static CBrush	m_bkBrush;

	CPtrArray	m_arPtrData;			// array containing pointers to CThumbnailButton

	long	m_nCol;						// number of columns
	long	m_nRow;						// nb of rows

	long	m_nThumbWidth;				// thumbnail width
	long	m_nThumbHeight;				// guess ... :]

//--[construction]-------------------------------------------------------
public:
	CThumbNailControl();
	virtual ~CThumbNailControl();

//--[functions]----------------------------------------------------------
public:
	void Add(const CString& sPath, CWnd *);
	void Remove(const CString& sPath);
	void Reset();
	void InitializeVariables( int cX = DEFAULT_THUMBWIDTH, int cY = DEFAULT_THUMBHEIGHT);
	int GetScrollPos32(int nBar, BOOL bGetTrackPos = FALSE );
	BOOL SetScrollPos32( int nBar, int nPos, BOOL bRedraw = TRUE );
	void RecalScrollBars( void );

private:
	BOOL RegisterWindowClass();
	void RecalButtonPos( void );

//--[event handling]-----------------------------------------------------
protected:
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
