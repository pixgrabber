//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixWizSub4
// File		:	PixWizSub4.h
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
//
//----------------------------------------------------------------------------------
#pragma once

#include "NewWizPage.h"
#include "resource.h"

class CPixWizSub4 : public CNewWizPage
{
// Construction
public:
	CPixWizSub4(CWnd* pParent = NULL);   // standard constructor
	void SetData(CString data);
	CString GetData();
// Dialog Data
	enum { IDD = IDD_WIZSUB4 };
	CStatic	m_CaptionCtrl;
	CString	m_PixLinks;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	LRESULT OnWizardNext();
	afx_msg void OnCheckLink();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};