//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeView
// File     :   PixTreeView.cpp
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class creates the Tabs for the Tree View
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixGrabber.h"

#include "PixTreeView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPixGrabberView

IMPLEMENT_DYNCREATE(CPixTreeView, CFormView)

BEGIN_MESSAGE_MAP(CPixTreeView, CFormView)
	ON_WM_SIZE()
	ON_REGISTERED_MESSAGE( UWM_ON_TREE_LCLICKED, OnTreeLClicked )
	ON_WM_CREATE()
END_MESSAGE_MAP()

// CPixGrabberView construction/destruction

//----------------------------------------------------------------------------------
//  Function CPixTreeGlobDlg
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Constructor
// 
//----------------------------------------------------------------------------------
CPixTreeView::CPixTreeView()
	: CFormView(CPixTreeView::IDD)
{
	wait = ::CreateDialog(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDD_WAITER), this->m_hWnd, NULL);
}

//----------------------------------------------------------------------------------
//  Function CPixTreeGlobDlg
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Destructor
// 
//----------------------------------------------------------------------------------
CPixTreeView::~CPixTreeView()
{
}

//----------------------------------------------------------------------------------
//  Function PreCreateWindow
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Called by the framework before the creation of the window 
//			  attached to this CWnd object.
//
// @param	: CREATESTRUCT cs, A CREATESTRUCT structure. 
// 
//----------------------------------------------------------------------------------
BOOL CPixTreeView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CFormView::PreCreateWindow(cs);
}


// CPixGrabberView diagnostics

#ifdef _DEBUG
void CPixTreeView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPixTreeView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CPixGrabberDoc* CPixTreeView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPixGrabberDoc)));
	return (CPixGrabberDoc*)m_pDocument;
}
#endif //_DEBUG

//----------------------------------------------------------------------------------
//  Function OnDataChange
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : XCVB
// 
//----------------------------------------------------------------------------------
void CPixTreeView::OnDataChange()
{
	if (!UpdateData())
		return;

	CPixGrabberDoc* pDoc = GetDocument();
	pDoc->UpdateAllViews(this);
	
}

//----------------------------------------------------------------------------------
//  Function OnSize
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Resizes the Tree View area, when an OnSize message is received.
//			  The Tree always occupies the entire area Dialog area.
// 
// @param	: UINT nType, Specifies the type of resizing requested.
// @param	: int cx, Specifies the new width of the client area. 
// @param	: int cy, Specifies the new height of the client area. 
//
//----------------------------------------------------------------------------------
void CPixTreeView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);
	CRect rctrl;
	CWnd *ctrl;

	ctrl = this->GetDlgItem(IDC_TAB);
	if (ctrl != NULL)
	{
		this->GetWindowRect(rctrl);
		this->ScreenToClient(rctrl);
		ctrl->MoveWindow(rctrl);
	}
	this->Invalidate();
}


//----------------------------------------------------------------------------------
//  Function DoDataExchange
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Called by the framework to exchange and validate dialog data.
//			  Creates the Tab (titels).  
// 
// @param	: CDataExchange* pDX, The pointer to a CDataExchange object. 
//
//----------------------------------------------------------------------------------
void CPixTreeView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB, TreeTabCtrl);

	//Insert Tab Names
	TreeTabCtrl.InsertItem(0, _T("Global"));
	TreeTabCtrl.InsertItem(1, _T("Local"));

	TreeTabCtrl.Init(this);
}

void CPixTreeView::TreeDataChange(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	CPixGrabberDoc* pDoc = GetDocument();
	if (pDoc)
	{
		//::MessageBox(0, zetree.GetItemText(pNMTreeView->itemNew.hItem), zetree.GetItemText(zetree.GetParentItem(pNMTreeView->itemNew.hItem)), 0);
		//if (!lstrcmp(zetree.GetItemText(zetree.GetParentItem(pNMTreeView->itemNew.hItem)), "Grabbed Websites"))
		//{
		//	pDoc->SetProject(CString(zetree.GetItemText(pNMTreeView->itemNew.hItem)));
		//}
		//pDoc->UpdateAllViews(this);
	}
	*pResult = 0;
}

LRESULT CPixTreeView::OnTreeLClicked( WPARAM wParam, LPARAM /* lParam  */ )
{
	CPixTreeCtrl *pTree = (CPixTreeCtrl *) wParam;

	if( ::IsWindow(pTree->m_hWnd))
	{
		CPixGrabberDoc* pDoc = GetDocument();
		if (!pDoc)
		{
			AfxMessageBox("Internal error while retrieving the document...");		
			return 0;
		}
		
		::ShowWindow(wait, SW_SHOW);
		::UpdateWindow(wait);
		srand(::GetTickCount());
		switch(rand()%5)
		{
			case 0:
				::SetDlgItemText( wait, IDC_WAITER, "Loading...");
			break;
			case 1:
				::SetDlgItemText(wait, IDC_WAITER, "Hold tight, we are loading !");
			break;
			case 2:
				::SetDlgItemText(wait, IDC_WAITER, "Hey. Watch behind you ! We load.");
			break;
			case 3:
				::SetDlgItemText(wait, IDC_WAITER, "Loading again...");
			break;
			case 4:
				::SetDlgItemText(wait, IDC_WAITER, "Investigating hard drive. And we load.");
			break;
			default:

			break;
		}
		pDoc->SetProject(pTree->GetSelectedFolder());
		pDoc->UpdateAllViews(this);
		::ShowWindow(wait, SW_HIDE);
		::EndDialog(wait, 0);
		return 0;
	}

	return 1;
}