//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixWizSub5
// File		:	PixWizSub5.h
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
//
//----------------------------------------------------------------------------------
#pragma once

#include "NewWizPage.h"
#include "resource.h"

class CPixWizSub5 : public CNewWizPage
{
// Construction
public:
	CPixWizSub5(CWnd* pParent = NULL);   // standard constructor
	void SetData(CString data);
	CString GetData();
// Dialog Data
	enum { IDD = IDD_WIZSUB5 };
	CStatic	m_CaptionCtrl;
	int		m_PixMax;
	int		m_PixMin;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	LRESULT OnWizardNext();
	afx_msg void OnAllsizes();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};
