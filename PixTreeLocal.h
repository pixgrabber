//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeLocDlg
// File     :   PixTreeLocal.h
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class controls the appearance of the Local Tree.
//
//----------------------------------------------------------------------------------


#pragma once
#include "PixGrabberDoc.h"

#ifndef __PIXTREECTRL_H__
#define __PIXTREECTRL_H__
	#include "PixTreeCtrl.h"
#endif //__PIXTREECTRL_H__


class CPixTreeLocDlg : public CDialog
{
	DECLARE_DYNAMIC(CPixTreeLocDlg)

public:
	CPixTreeLocDlg(CWnd* pParent = NULL);   
	virtual ~CPixTreeLocDlg();
	enum { IDD = IDD_TAB_LOCAL };

	CPixTreeCtrl m_TreeLocFile;

public:
	void SetParentHWnd(CWnd *parent);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);

	DECLARE_MESSAGE_MAP()

};
