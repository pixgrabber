//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixOptionDlg
// File     :   PixOptionDlg.h
// Author   :   isc03meal
// Date     :   25.11.2003
// Version  :   1.0
//
//----------------------------------------------------------------------------------

#include <afxwin.h>
#include "resource.h"

#pragma once

class CPixOptionDlg : public CDialog
{
// Construction
public:
// standard constructor
	CPixOptionDlg(CWnd* pParent = NULL);
	
	int min;	// File minimum size
	int max;	// File maximum size
	int link;	// Links to follow
	char pix;	//All (A) | jpg (J) | gif (G)	
	BOOL all;
	BOOL checklink;
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	
// Dialog Data
	enum { IDD = IDD_OPTIONS };

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	afx_msg void OnClickAll();
	afx_msg void OnChecklink();
	DECLARE_MESSAGE_MAP()
};