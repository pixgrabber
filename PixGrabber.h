//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixGrabberApp
// File		:	PixGrabber.h
// Author	:	Thierry "real" Haven
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once

//--[vc++ precompiled header stuff]--------------------------------------
#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

//--[class]--------------------------------------------------------------

class CPixGrabberApp : public CWinApp
{
//--[construction]-------------------------------------------------------
public:
	CPixGrabberApp();


//--[overrides]----------------------------------------------------------
public:
	virtual BOOL InitInstance();

//--[event handling]-----------------------------------------------------
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CPixGrabberApp theApp;	// just include this header if you want to use
extern char home[256];			// those global variables elsewhere