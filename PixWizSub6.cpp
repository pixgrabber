//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CPixWizSub6
// File		:	PixWizSub6.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class is used to handle the sub page 6 in the wizard. 
//              Sub page 6 is used to show a summary of the entered values to 
//				the user.
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixWizSub6.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CPixWizSub6::CPixWizSub6(CWnd* pParent /*=NULL*/)
	: CNewWizPage(CPixWizSub6::IDD, pParent)
{
		// NOTE: the ClassWizard will add member initialization here
}


void CPixWizSub6::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ST_CAPTION, m_CaptionCtrl);
	DDX_Control(pDX, IDC_SUMMARY, m_summary);	
}


BEGIN_MESSAGE_MAP(CPixWizSub6, CNewWizPage)

END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the font for the title and to set the 
//            default values in the edit fields.
//
// @return  : TRUE if successfull, ....
//
//---------------------------------------------------------------------------------- 

BOOL CPixWizSub6::OnInitDialog() 
{
	CNewWizPage::OnInitDialog();
	
// set font for header
	m_CaptionCtrl.SetFont(&m_LargeFont, TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
}



//----------------------------------------------------------------------------------
//  Function OnWizardNext
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the "next" button on the last sub page
// 
//
// @return  : 0 if successfull, ....
//
//----------------------------------------------------------------------------------

LRESULT CPixWizSub6::OnWizardNext()
{
	CNewWizPage::OnWizardNext();
	return 0;
}



//----------------------------------------------------------------------------------
//  Function SetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the information in the the static field
//            from the sub page 6.
// 
//---------------------------------------------------------------------------------- 

void CPixWizSub6::SetData(CString *data)
{

	CString output;
	CString temp;
	int followLinks;
	int minPix;
	int maxPix;

	// format our summary output
	output = "\nURL\t\t:\t" + (CString)data[0].GetString() + "\n";
	if(data[1].GetAt(0) == 'A')
		output += "PixType\t\t:\tAll pix types\n";
	if(data[1].GetAt(0) == 'G')
		output += "PixType\t\t:\tGIF pix only\n";
	if(data[1].GetAt(0) == 'J')
		output += "PixType\t\t:\tJPG pix only\n";

	followLinks = atoi(data[2].GetBuffer());
	if(followLinks == 0)
		output += "FollowLinks\t:\tNO\n";
	else
	{
		temp.Format("%d Links", followLinks);
		output += "FollowLinks\t:\tYES, " + temp + "\n";
	}
	sscanf(data[3].GetBuffer(), "%d %d", &minPix, &maxPix); 
	if(minPix == 0 && maxPix == 0)
		output += "PixSize\t\t:\tPix size does not matter\n";
	else
	{
		temp.Format("PixSize\t\t:\tMinimum : %d, Maximum : %d\n", 
			minPix, maxPix);
		output += temp;
	}	
	m_summary.SetWindowText(output);	
}



//----------------------------------------------------------------------------------
//  Function GetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function has to be overwritten in this sub page. We do not enter
//            new values.
// 
//
//----------------------------------------------------------------------------------

CString CPixWizSub6::GetData()
{
	return CString("");
}