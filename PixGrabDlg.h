//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixGrabDlg
// File		:	PixGrabDlg.h
// Author	:	Reto Buerki
// Date		:	25.11.2003
// Version	:	1.0
//
//----------------------------------------------------------------------------------
#pragma once

#include "PixGrabThread.h"
#include "afxwin.h"

class CPixGrabDlg : public CDialog
{
	DECLARE_DYNAMIC(CPixGrabDlg)

	// dialog Data
	enum { IDD = IDD_PROGRESS };

//--[functions]---------------------------------------------------------------------
protected:
	virtual BOOL OnInitDialog( void );
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnDestroy( );
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedVerboseo();
	afx_msg void OnBnClickedClearLog();
	afx_msg void OnBnClickedClearQ();

	DECLARE_MESSAGE_MAP()

	void InitImageList();

public:
	CPixGrabDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPixGrabDlg();

	// overrides
	BOOL Create(UINT nID, CWnd *pWnd);

	// thread controlling function
	void RegisterThread(CPixGrabThread* myThread);

	// display status functions
	void ShowFollowedLinks(int f_links);
	void ShowDownloadedPix(int d_pix);
	void ShowDownloadedData(double data);
	void ShowCachedLinks(int links);
	void ShowInformation(int IDC, LPCTSTR fmt, ...);
	void ShowCachedPix(int pix);
	void ShowSourceInfo(CString strName,
        int size,
		double speed,
		CString strStatus,
		BOOL success);
	void ShowMaxLink(int maxlink);

	// queue handling functions
	void AddQueueItem(CString strURL,
		int minSize,
		int maxSize,
		char pixTypes,
		BOOL followLinks,
		int linkCount);
	void RemoveQueueItem(int pos);
	BOOL GetNextQueueItem(CString &strURL,
		int &minSize,
		int &maxSize,
		char &pixTypes,
		BOOL &isFollowLinks,
		int &maxFollowCount);

	// dialog appearence handler functions
	void ResetStatus();
	void SetStatusFollowLinks(BOOL status, int followCount);

//--[member variables]--------------------------------------------------------------
protected:
	HICON			m_hIcon;
	CImageList		m_cImageList;

	// associated thread and parent Wnd
	CPixGrabThread* m_pThread;
	CWnd*			m_pWnd;

public:
	// CListCtrls
	CListCtrl		m_Log;
	CListCtrl		m_Q;
	// used to display current messages and URL
	CStatic			m_currMSG;
	CStatic			m_currURL;
	// ProgressBars
	CProgressCtrl	m_ProgressPix;
	CProgressCtrl	m_ProgressLinks;
	afx_msg void OnBnClickedDelete();
};
