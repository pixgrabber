//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixWizSub6
// File		:	PixWizSub6.h
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
//
//----------------------------------------------------------------------------------
#pragma once

#include "NewWizPage.h"
#include "resource.h"

class CPixWizSub6 : public CNewWizPage
{
// Construction
public:
	CPixWizSub6(CWnd* pParent = NULL);   // standard constructor
	void SetData(CString* data);
	CString GetData();

// Dialog Data
	enum { IDD = IDD_WIZSUB6 };
	CStatic	m_CaptionCtrl;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_summary;
};