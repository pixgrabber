//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeGlobDlg
// File     :   PixTreeGlobal.cpp
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class controls the appearance of the Global Tree.
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixGrabber.h"
#include "PixTreeGlobal.h"
#include ".\pixtreeglobal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CPixTreeGloDlg dialog

IMPLEMENT_DYNAMIC(CPixTreeGlobDlg, CDialog)

BEGIN_MESSAGE_MAP(CPixTreeGlobDlg, CDialog)
		ON_WM_SIZE()
		ON_WM_SETFOCUS()
END_MESSAGE_MAP()


//----------------------------------------------------------------------------------
//  Function CPixTreeGlobDlg
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Constructor
//
// @param	: CWnd* pParent, Pointer to the parent window of the object
// 
//----------------------------------------------------------------------------------
CPixTreeGlobDlg::CPixTreeGlobDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPixTreeGlobDlg::IDD, pParent)
{
}

//----------------------------------------------------------------------------------
//  Function CPixTreeGlobDlg
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Destructor
// 
//----------------------------------------------------------------------------------
CPixTreeGlobDlg::~CPixTreeGlobDlg()
{
}

//----------------------------------------------------------------------------------
//  Function DoDataExchange
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Called by the framework to exchange and validate dialog data.
// 
// @param	: CDataExchange* pDX, The pointer to a CDataExchange object. 
//
//----------------------------------------------------------------------------------
void CPixTreeGlobDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_GLOB, m_TreeFile);
}

//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Creates the tree when the Dialog is initialised.
// 
// @return	: BOOL, TRUE if tree could be created.
//
//----------------------------------------------------------------------------------
BOOL CPixTreeGlobDlg::OnInitDialog()
{
	CRect r;
	if (!m_TreeFile.Create(WS_VISIBLE | WS_CHILD | TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT | TVS_EDITLABELS | TVS_SHOWSELALWAYS, r, this, 100))
		return -1;	      

	return 0;
}


//----------------------------------------------------------------------------------
//  Function OnSize
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Resizes the Tree View area, when an OnSize message is received.
//			  The Tree always occupies the entire area Dialog area.
// 
// @param	: UINT nType, Specifies the type of resizing requested.
// @param	: int cx, Specifies the new width of the client area. 
// @param	: int cy, Specifies the new height of the client area. 
//
//----------------------------------------------------------------------------------
void CPixTreeGlobDlg::OnSize(UINT nType, int cx, int cy) 
{
	if (m_TreeFile)
	{
	//Let the tree control occupy all the space of the client area
	CRect r;  
	GetClientRect(&r);
	m_TreeFile.MoveWindow(r);
	}
}

//----------------------------------------------------------------------------------
//  Function OnSetFocus
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Resizes the Tab Dialogs, when an OnSize message is received.
//			  The Dialog always occupies the entire area
// 
// @param	: CWnd* pOldWnd, Contains the CWnd object that loses the input focus. 
//
//----------------------------------------------------------------------------------
void CPixTreeGlobDlg::OnSetFocus(CWnd* pOldWnd) 
{
	m_TreeFile.SetFocus();
}

void CPixTreeGlobDlg::SetParentHWnd(CWnd *parent)
{
	m_TreeFile.SetParentHWnd(parent);
}