//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixWizSub1
// File		:	PixWizSub1.h
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
//
//----------------------------------------------------------------------------------

#pragma once

#include "NewWizPage.h"
#include "resource.h"

class CPixWizSub1 : public CNewWizPage
{
// Construction
public:
	CPixWizSub1(CWnd* pParent = NULL);   // standard constructor
	void SetData(CString data);
	CString GetData();

// Dialog Data
	enum { IDD = IDD_WIZSUB1 };
	CStatic	m_CaptionCtrl;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	BOOL OnQueryCancel();
	LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};