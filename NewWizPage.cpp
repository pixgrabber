//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CNewWizPage
// File		:	NewWizPage.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class is used to handle the wizard with the class CNewWizPage
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "NewWizPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CNewWizPage::CNewWizPage(LPCTSTR lpszTemplateName, CWnd* pParent)
                   :CDialog(lpszTemplateName,pParent)
{
  m_bCreated = FALSE;
  m_bActive = FALSE;
  m_nDialogID = 0;
  m_pParent = NULL; 
}

CNewWizPage::CNewWizPage(UINT nIDTemplate, CWnd* pParent)
                   :CDialog(nIDTemplate,pParent)
{
  m_bCreated = FALSE;
  m_bActive = FALSE;
  m_nDialogID = nIDTemplate;
  m_pParent = NULL; 
}

CNewWizPage::~CNewWizPage()
{
}


CString CNewWizPage::GetData()
{
	return CString("");
}

void CNewWizPage::SetData(CString* data)
{
	return;
}

void CNewWizPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
		// NOTE: the ClassWizard will add DDX and DDV calls here
}

BEGIN_MESSAGE_MAP(CNewWizPage, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to create the font for the title in the Aizard.
//
// @return  : TRUE if successfull...
//----------------------------------------------------------------------------------

BOOL CNewWizPage::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_LargeFont.CreateFont(-16, 0, 0, 0, 
		FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, 
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("MS Sans Serif"));

  DWORD style = GetStyle();
  ASSERT((style & WS_CHILD) != 0);
  ASSERT((style & WS_BORDER) == 0);
  ASSERT((style & WS_DISABLED) != 0);

  return TRUE; 
}

//----------------------------------------------------------------------------------
//  Function OnCreatePage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @return  : TRUE
//----------------------------------------------------------------------------------
BOOL CNewWizPage::OnCreatePage()
{
	return TRUE;
}
//----------------------------------------------------------------------------------
//  Function OnDestroyPage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
//----------------------------------------------------------------------------------
void CNewWizPage::OnDestroyPage()
{
}
//----------------------------------------------------------------------------------
//  Function OnSetActive
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
//----------------------------------------------------------------------------------
void CNewWizPage::OnSetActive()
{
}

//----------------------------------------------------------------------------------
// For more information of this functions id like to refer to CPropertyPage class 
// members for a description of this function
//----------------------------------------------------------------------------------
void CNewWizPage::OnCancel()
{
}
BOOL CNewWizPage::OnKillActive()
{
	return TRUE;
}
BOOL CNewWizPage::OnQueryCancel()
{
	return TRUE;
}
LRESULT CNewWizPage::OnWizardBack()
{
	return 0;
}
LRESULT CNewWizPage::OnWizardNext()
{
	return 0;
}
BOOL CNewWizPage::OnWizardFinish()
{
	return TRUE;
}