//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CNewWizDialog
// File     :   NewWizDialog.h
// Author   :   isc03meal
// Date     :   02.12.2003
// Version  :   1.0
//
//----------------------------------------------------------------------------------
#pragma once

#include "resource.h"

class CNewWizPage;

class CNewWizDialog : public CDialog
{
// Construction
public:
    CNewWizDialog (LPCTSTR lpszTemplate, CWnd* pParent = NULL);
    CNewWizDialog (UINT nIDTemplate, CWnd* pParent = NULL);
    virtual ~CNewWizDialog ();
    
// Attributes
public:
    int m_curpage;
	CString summary[5];

protected:
    CObList m_PageList; 
// controls the ID for the placeholder
    UINT m_nPlaceholderID;

// Operations
public:
    void AddPage(CNewWizPage* pPage, UINT nID);
    void SetActivePageByResource(UINT nResourceID);
    BOOL SetFirstPage();
    void SetNextPage();
    void EnableFinish(BOOL bEnable);
	void EnableBack(BOOL bEnable);
	void EnableNext(BOOL bEnable);
	int GetActiveIndex() const;
	int GetPageIndex(CNewWizPage* pPage) const;
	int GetPageCount();
	CNewWizPage* GetPage(int nPage) const;
	BOOL SetActivePage(int nPage);
	BOOL SetActivePage(CNewWizPage* pPage);
	void SetTitle(LPCTSTR lpszText);
	void SetTitle(UINT nIDText);
	void SetFinishText(LPCTSTR lpszText);
	void SetFinishText(UINT nIDText);
	
protected:
    BOOL ActivatePage(CNewWizPage* pPage);
    BOOL DeactivatePage();
    void SetPlaceholderID(int nPlaceholderID);
    CNewWizPage* GetPageByResourceID(UINT nResourceID);
    
private:
    void Init();
    BOOL DestroyPage(CNewWizPage* pPage);
    CNewWizPage* GetFirstPage();
    CNewWizPage* GetLastPage();
    CNewWizPage* GetActivePage() const;
    CNewWizPage* GetNextPage();

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

// Implementation
protected:
	afx_msg void OnDestroy();
	afx_msg void OnWizardFinish();
	afx_msg void OnWizardBack(); 
	afx_msg void OnWizardNext(); 
	afx_msg void OnCancel(); 
	DECLARE_MESSAGE_MAP()
};