//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixGrabberDoc
// File		:	PixGrabberDoc.h
// Author	:	Thierry "real" Haven
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once
#include "Picture.h"

//--[class]--------------------------------------------------------------
class CPixGrabberDoc : public CDocument
{
//--[construction]-------------------------------------------------------
protected: // create from serialization only
	CPixGrabberDoc();
	DECLARE_DYNCREATE(CPixGrabberDoc)

//--[variables]----------------------------------------------------------
public:
	CString m_project;
	CString m_selected;
	CPicture m_pict;

//--[functions]----------------------------------------------------------
public:
	void SetProject(CString proj);
	CPicture* GetPicture();
	void LoadPicture();

//--[overrides]----------------------------------------------------------
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

//--[vc++ autogenerated]-------------------------------------------------
public:
	virtual ~CPixGrabberDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

//--[message map]--------------------------------------------------------
protected:
	DECLARE_MESSAGE_MAP()
};


