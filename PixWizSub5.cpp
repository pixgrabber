//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CPixWizSub5
// File		:	PixWixSub5.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class is used to handle the sub page 5 from the wizard.
//				Sub page 5 is used to get the values for the pictures
//				-> min,max,all Sizes
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixWizSub5.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CPixWizSub5::CPixWizSub5(CWnd* pParent /*=NULL*/)
	: CNewWizPage(CPixWizSub5::IDD, pParent)
{
	m_PixMax = 0;
	m_PixMin = 0;
}


void CPixWizSub5::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ST_CAPTION, m_CaptionCtrl);
	DDX_Text(pDX, IDC_MAX, m_PixMax);
	DDX_Text(pDX, IDC_MIN, m_PixMin);
}


BEGIN_MESSAGE_MAP(CPixWizSub5, CNewWizPage)
	ON_BN_CLICKED(IDC_ALLSIZES, OnAllsizes)
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnAllsizes
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the enabling of the edit fields by 
//            using the checkbox to define the sizes from the to downloading pictures
//
//
//---------------------------------------------------------------------------------- 

void CPixWizSub5::OnAllsizes() 
{
	if (IsDlgButtonChecked(IDC_ALLSIZES) == TRUE) 
	{
		GetDlgItem(IDC_MIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MAX)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_MIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_MAX)->EnableWindow(TRUE);
	}	
}



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the font for the title and to set the 
//		      default values in the edit fields.
//
// @return  : TRUE if successfull, ....
//
//---------------------------------------------------------------------------------- 

BOOL CPixWizSub5::OnInitDialog() 
{
	CNewWizPage::OnInitDialog();
	
// for this page, we will use the base classes' large font
	m_CaptionCtrl.SetFont(&m_LargeFont, TRUE);
	
// Set default values in edit fields
	int min = 2;
	int max = 500;
	
	SetDlgItemInt(IDC_MIN, min);
	SetDlgItemInt(IDC_MAX, max);
	
	return TRUE;  // return TRUE unless you set the focus to a control
}



//----------------------------------------------------------------------------------
//  Function OnWizardNext
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to check the edit field of a entered value, if 
//			  nothing is entered, the function shows a message.
//
// @return  : 0 if successfull, ....
//
//----------------------------------------------------------------------------------

LRESULT CPixWizSub5::OnWizardNext()
{
 	int min;
	int max;
	BOOL allSizes;
	
	min = GetDlgItemInt(IDC_MIN);
	max = GetDlgItemInt(IDC_MAX);
	allSizes = IsDlgButtonChecked(IDC_ALLSIZES);

	if (allSizes == TRUE) 
	{
		min = 0;
		max = 0;
	}
	else if (min == NULL || max == NULL)
	{
		AfxMessageBox("Please enter a value in the edit field.");
		return -1;
	}
	else if (max < min) 
	{
		AfxMessageBox("The maximum value can not be smaller than the minimum value");
		return -1;
	}
	else if (max == min)
	{
		AfxMessageBox("The values should be different");
		return -1;
	}
	else if(min < 0 || max < 0)
	{
		AfxMessageBox("Negativ values are not allowed here");
		return -1;
	}
	
	CNewWizPage::OnWizardNext();
	return 0;
}



//----------------------------------------------------------------------------------
//  Function GetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the entered values. All Values which are 
//			  no CStrings are going to be converted in CString.
//
// @return  : TRUE if successfull, ....
//
//---------------------------------------------------------------------------------- 

CString CPixWizSub5::GetData()
{
	int min;
	int max;
	BOOL allSizes;
	CString	m_CSPixMax;
	CString	m_CSPixMin;
	char temp_min[10];
	char temp_max[10];

	min = GetDlgItemInt(IDC_MIN);
	max = GetDlgItemInt(IDC_MAX);

	allSizes = IsDlgButtonChecked(IDC_ALLSIZES);

	if (allSizes == TRUE) 
	{
		m_CSPixMin = CString("0");
		m_CSPixMax = CString("0");
	}
	else
	{
		_itoa(min,temp_min,10);
		_itoa(max,temp_max,10);
		m_CSPixMin = CString(temp_min);
		m_CSPixMax = CString(temp_max);
	}
	return (m_CSPixMin + CString (" ") + m_CSPixMax);
}


//----------------------------------------------------------------------------------
//  Function SetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function has to be overwritten in this sub page. We do not Set
//            new values. 
//
// @param   : data CString
//
//---------------------------------------------------------------------------------- 

void CPixWizSub5::SetData(CString data)
{
	return;
}
