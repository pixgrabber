//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CFullScreenDlg
// File		:	FullScreenDlg.h
// Author	:	Thierry "real" Haven
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once
#include "imgcontrol.h"

//--[class]--------------------------------------------------------------
class CFullScreenDlg : public CDialog
{
	DECLARE_DYNAMIC(CFullScreenDlg)

private:
	BOOL m_escape;		// was escape key pressed ? if so we exit fullscreen mode
	POSITION m_spic;	// position of the picture currently shown on screen
	char m_pic[256];	// name of the picture displayed on screen
	CObList m_piclist;	// image list constructed with BuildList.
						// this list contains only picture names

	CImgControl m_image; // control holding the picture shown on screen

	// the following datas are only used in the recursive ScanFolderForPics
	// function.
	WIN32_FIND_DATA fd;	// keeps the data relative to a file
	DWORD attrib;		// attibutes of the file (archive, hidden, folder, etc.)
	bool ok;
	//DELETE_ME? int nitem;
	//DELETE_ME? char courant[256];
	//DELETE_ME? char preced[64];

	int m_smartmode;// actual "smart resizing" mode of the picture (keep aspect ratio)
					// can be SMART_DIM_ALWAYS (always resize to fit screen with aspect ratio kept)
					// or SMART_DIM_IFSMALLER (do not resize picture if smaller than screen)

	enum { IDD = IDD_FULLSCREEN };	// link to dialog resource


//--[operations]----------------------------------------------------------
public:
	CFullScreenDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFullScreenDlg();
	void Load(char *current);
	void MakeFullScreen();
	void MessagePump();
	void BuildList();
	void LoadNext();
	void LoadPrev();
	void ScanFolderForPics(char *dir);

//--[overrides]----------------------------------------------------------
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();								// future usage, see .cpp
	afx_msg	BOOL PreTranslateMessage(LPMSG lpmsg);		// custom message loop
	DECLARE_MESSAGE_MAP()
};
