//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeView
// File     :   PixTreeView.h
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class creates the Tabs for the Tree View
//
//----------------------------------------------------------------------------------

#pragma once
#include "PixGrabberDoc.h"
#include "PixTabCtrl.h"
#include "PixTreeCtrl.h"

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// CPixTreeView

class CPixTreeView : public CFormView
{
protected: // create from serialization only
	
	DECLARE_DYNCREATE(CPixTreeView)

// Attributes
public:
	CPixTreeView();
	virtual ~CPixTreeView();
	enum { IDD = IDD_CONTROL };
	CPixGrabberDoc* GetDocument() const;
	void TreeDataChange(NMHDR* pNMHDR, LRESULT* pResult);
	CPixTabCtrl TreeTabCtrl;
	HWND wait;								// wait dialog box

// Overrides
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif	

protected:
	afx_msg LRESULT OnTreeLClicked( WPARAM wParam, LPARAM);
	afx_msg void OnDataChange(); 
	afx_msg void OnSize(UINT nType, int cx, int cy); 

	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PixGrabberView.cpp
inline CPixGrabberDoc* CPixTreeView::GetDocument() const
   { return reinterpret_cast<CPixGrabberDoc*>(m_pDocument); }
#endif


