//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PixGrabber.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_OPTIONS                     101
#define IDR_MENU1                       127
#define IDR_MAINFRAME                   128
#define IDD_CONTROL                     130
#define IDD_WIZARD                      131
#define IDD_PROGRESS                    132
#define IDD_THUMB                       133
#define IDD_WAITER                      135
#define IDD_IMG                         136
#define IDI_ICON1                       139
#define IDI_NETWORK                     139
#define IDB_IMAGELIST                   140
#define IDR_GIF1                        142
#define IDI_ZPLUS                       143
#define IDI_ZMINUS                      145
#define IDI_ZEQUAL                      146
#define IDI_ZSMART                      147
#define IDI_ZFIT                        148
#define IDD_TAB_GLOBAL                  149
#define IDD_TAB_LOCAL                   150
#define IDD_FULLSCREEN                  151
#define IDC_GRAB                        1003
#define IDC_URL                         1005
#define IDC_PROGRESS1                   1006
#define IDC_LOG                         1007
#define IDC_TREE                        1008
#define IDC_SHEETRECT                   1010
#define IDC_ALL                         1014
#define IDC_FRAME_THUMB                 1015
#define IDC_WAITER                      1016
#define IDC_BUTTON_ZOOMPLUS             1017
#define IDC_IMGSH                       1019
#define IDC_IMGSV                       1020
#define IDC_BUTTON_ZOOMMINUS            1021
#define IDC_WIZURL                      1021
#define IDC_BUTTON_REAL                 1022
#define IDC_BUTTON_FIT                  1023
#define IDC_BUTTON_FULL                 1024
#define IDC_BUTTON_ZINIT                1025
#define IDC_BUTTON_NEXT                 1026
#define IDC_BUTTON_PREVIOUS             1027
#define IDC_BUTTON2                     1028
#define IDC_TITLE                       1029
#define IDC_TAB                         1030
#define IDC_TREE_GLOB                   1031
#define IDC_TREE_LOC                    1032
#define IDD_WIZSUB1                     1033
#define IDD_WIZSUB2                     1034
#define IDD_WIZSUB244                   1034
#define IDD_WIZSUB3                     1035
#define IDD_WIZSUB4                     1036
#define IDD_WIZSUB5                     1037
#define IDD_WIZSUB6                     1038
#define IDC_CURRMSG                     1044
#define IDC_MIN                         1045
#define IDC_CURRURL                     1046
#define IDC_LINKS                       1047
#define IDC_IMGVIEW                     1048
#define IDC_MAX                         1049
#define IDC_LINKSCACHE                  1050
#define ST_CAPTION                      1051
#define IDC_PIXCACHE                    1052
#define IDC_LINK                        1053
#define IDC_SERVERSOFT                  1054
#define IDC_CHECKLINK                   1055
#define IDC_LINKSFOLLOWED               1056
#define IDC_PIXDOWN                     1057
#define IDC_PIXCHECKALL                 1058
#define IDC_VERBOSEO                    1059
#define IDC_KBDOWN                      1060
#define IDC_DATADOWN                    1060
#define IDC_LIST2                       1061
#define IDC_ALLSIZES                    1062
#define IDC_CLEARQ                      1063
#define IDC_CLEARLOG                    1064
#define IDC_DELETE                      1065
#define IDC_PROGRESS2                   1066
#define IDC_CHECK_LINK                  1067
#define IDC_PMAXLINK                    1068
#define IDC_SUMMARY                     1069
#define IDC_Q                           1070
#define IDC_LFOLLOWED_S                 1071
#define IDC_JPG                         1072
#define IDC_GIF                         1073
#define IDC_CAPTION                     10332
#define IDC_COMBO1                      10335
#define IDB_LOGO                        15202
#define ID_OPTIONS_PREFERENCES          32771
#define ID_FILE_PROPERTIES              32775
#define ID_FILE_DELETE                  32776
#define ID_FILE_RENAME                  32777
#define ID_REFRESH                      32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        15203
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         10336
#define _APS_NEXT_SYMED_VALUE           10100
#endif
#endif
