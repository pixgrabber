//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CViewExSplitWnd / CMainFrame
// File		:	MainFrame.h
// Author	:	Thierry "real" Haven
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once

#include "PixGrabDlg.h"

//--[class]--------------------------------------------------------------
// we need to handle 2 CSplitWnd.
class CViewExSplitWnd : public CSplitterWnd
{
	DECLARE_DYNAMIC(CViewExSplitWnd)

//--[construction]-------------------------------------------------------
public:
	CViewExSplitWnd();
	~CViewExSplitWnd();

//--[overrides]----------------------------------------------------------
public:
	CWnd* GetActivePane(int* pRow = NULL, int* pCol = NULL);
};

//--[class]--------------------------------------------------------------
class CMainFrame : public CFrameWnd
{
	DECLARE_DYNCREATE(CMainFrame)

//--[construction]-------------------------------------------------------
protected:
	CMainFrame(); // vc++ made it protected for some reason...
public:
	virtual ~CMainFrame();

//--[variables]----------------------------------------------------------
public:

	typedef struct
	{
		int m_PixMin;
		int	m_PixMax;
		int m_PixLinks;
		char m_PixTypes;
		BOOL m_PixFollowLinks;
	}PIXOPTIONS;

	PIXOPTIONS pixoptions;
	CString* m_summary;

protected:
	CViewExSplitWnd m_wndSplitter;		// split window 1
	CViewExSplitWnd m_wndSplitter2;		// split 2
	WORD w_toolsize;				// current toolbar size

	// control bar embedded members
	CStatusBar  m_wndStatusBar;			// status bar
	CToolBar    m_wndToolBar;			// DELETE_ME?
	CReBar      m_wndReBar;				// toolbar is rebar
	CDialogBar	m_wndDlgBar;			// rebar contains this one only
	CPixGrabDlg m_networkStatusWnd;		// network dialog

private:
	int m_retryCounter;					// retry to close thread stuff

//--[functions]----------------------------------------------------------
private:
	void StartGrab(CString pszURL, int PixMin, int PixMax, char PixTypes,
                   BOOL PixFollowLinks, int PixLinks);

//--[overrides]----------------------------------------------------------
public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

//--[event handling]-----------------------------------------------------
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy( );
	afx_msg void OnClose( );
	afx_msg void OnFileNew( );
	afx_msg void OnBnClickedGrab();
	afx_msg void OnOptionsPreferences();
	DECLARE_MESSAGE_MAP()

//--[mfc stuff]----------------------------------------------------------
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};

