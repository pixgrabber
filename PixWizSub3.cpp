//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CPixWizSub3
// File		:	PixWizSub3.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class is used to handle the sub page 3 in the wizard. 
//				The sub page 3 is used to get the type of pictures which the 
//				application has to download
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixWizSub3.h"
#include "NewWizDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CPixWizSub3::CPixWizSub3(CWnd* pParent /*=NULL*/)
	: CNewWizPage(CPixWizSub3::IDD, pParent)
{
		// NOTE: the ClassWizard will add member initialization here
}


void CPixWizSub3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ST_CAPTION, m_CaptionCtrl);
}


BEGIN_MESSAGE_MAP(CPixWizSub3, CDialog)
	ON_BN_CLICKED(IDC_PIXCHECKALL, OnPixcheckall)
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle font of the titel from the sub page 
//            and for the default values from the checkbox and radiobutton 
//            (default and disable).
// 
// @return  : TRUE if successfull, ....
//
//----------------------------------------------------------------------------------

BOOL CPixWizSub3::OnInitDialog() 
{
	CNewWizPage::OnInitDialog();
	
//	m_Font.CreateFont(-16, 0, 0, 0, 
//		FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, 
//		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("MS Sans Serif"));

	m_CaptionCtrl.SetFont(&m_Font, TRUE);

	CheckDlgButton(IDC_PIXCHECKALL,1);
	
	CheckRadioButton(IDC_JPG, IDC_GIF, IDC_JPG);
	
	GetDlgItem(IDC_JPG)->EnableWindow(FALSE);
	GetDlgItem(IDC_GIF)->EnableWindow(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              
}



//----------------------------------------------------------------------------------
//  Function OnPixcheckall
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the enabling of the Radiobutton field 
//		      by using the checkbox
//
//----------------------------------------------------------------------------------

void CPixWizSub3::OnPixcheckall() 
{
	if (IsDlgButtonChecked(IDC_PIXCHECKALL) == TRUE) 
	{
		GetDlgItem(IDC_JPG)->EnableWindow(FALSE);
		GetDlgItem(IDC_GIF)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_JPG)->EnableWindow(TRUE);
		GetDlgItem(IDC_GIF)->EnableWindow(TRUE);
	}
}



//----------------------------------------------------------------------------------
//  Function OnWizardNext
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : In this sub page is nothing to check, so this function is not used.
//
// @return  : 0 if successfull, ....
//
//----------------------------------------------------------------------------------

LRESULT CPixWizSub3::OnWizardNext()
{
	CNewWizPage::OnWizardNext();
	return 0;
}



//----------------------------------------------------------------------------------
//  Function OnSetActive
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function was used to debug
//
//----------------------------------------------------------------------------------

void CPixWizSub3::OnSetActive()
{
	//m_pParent->SetTitle("Hello Wiz");
}



//----------------------------------------------------------------------------------
//  Function OnKillActive
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function was used to debug
//
// @return  : TRUE if successfull, ....
//
//----------------------------------------------------------------------------------

BOOL CPixWizSub3::OnKillActive()
{
	//m_pParent->SetTitle("KillActive was called");
	return TRUE;
}



//----------------------------------------------------------------------------------
//  Function GetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the entered values. It also convert the
//            Radiobutton and checkbox settings into CString values.->A,J,G
// 
// @return  : CString("")
//
//----------------------------------------------------------------------------------

CString CPixWizSub3::GetData()
{
	BOOL pixAll;
	pixAll = IsDlgButtonChecked(IDC_PIXCHECKALL);

	if(pixAll == TRUE)
	{
		return CString("A");;
	}
	else
	{
		switch(GetCheckedRadioButton(IDC_JPG,IDC_GIF))
		{
		case IDC_JPG:
				return CString("J");
			break;
		case  IDC_GIF:
				return CString("G");
			break;
		default:
			break;
		}
	}
	return CString("");
}



//----------------------------------------------------------------------------------
//  Function SetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function has to be overwritten in this sub page. We do not set 
//            new values
//
//----------------------------------------------------------------------------------

void CPixWizSub3::SetData(CString data)
{
	return;
}

