//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CPixGrabThread
// File		:	PixGrabThread.cpp
// Author	:	Reto Buerki
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class is derived from CWinThread and performs parsing/
//              downloading tasks in the background. It also sends it status
//              information to his network status window (grabDLG).
//
//----------------------------------------------------------------------------------
#include "stdafx.h"
#include "PixGrabber.h"
#include "PixGrabThread.h"
#include "PixGrabDlg.h"

// debug
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// thread start message
#define WM_START	WM_USER + 1
#define WM_CLEAN	WM_USER + 2

// extended status codes
#define HTTP_STATUS_SIZE	800
#define HTTP_STATUS_HOST_NF 810

//--[Globals]-----------------------------------------------------------------------
// the network status window
CPixGrabDlg* grabDLG;
//----------------------------------------------------------------------------------

IMPLEMENT_DYNCREATE(CPixGrabThread, CWinThread)

BEGIN_MESSAGE_MAP( CPixGrabThread, CWinThread )
	ON_THREAD_MESSAGE(WM_START, StartFunc)
	ON_THREAD_MESSAGE(WM_CLEAN, CleanFunc)
END_MESSAGE_MAP( )

//--[Construction / Deconstruction]-------------------------------------------------
CPixGrabThread::CPixGrabThread()
{

}

CPixGrabThread::~CPixGrabThread()
{

}

//--[thread message handler functions]----------------------------------------------

//----------------------------------------------------------------------------------
//	protected Function StartFunc(WPARAM n1, LPARAM n2)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This function is executed when the thread receives a WM_START message
//  from its associated window or from CMainFrame.
//  The function reads items from the Q list and handles the URL extracted from the
//  items with the member function HandleURL().
//
// @param WPARAM n1					usually NULL
// @param LPARAM n2					usually NULL
//
//----------------------------------------------------------------------------------
void CPixGrabThread::StartFunc(WPARAM n1, LPARAM n2)
{
	// thread has been started
	if(!m_hasStarted)
		m_hasStarted = TRUE;
	// init the COM subsystem used to process html
	CoInitialize(NULL);
	// indicate that we are running
	m_isCanceled = FALSE;

	grabDLG->GetDlgItem(IDCANCEL)->SetWindowText("Cancel");
	// get the actual URL to parse
	while(grabDLG->GetNextQueueItem(m_strURL,
		m_minSize,
		m_maxSize,
		m_pixTypes,
		m_isFollowLinks,
		m_maxFollowCount) && !m_isCanceled)
	{
		// remove the actual Item from the ListCtrl
		grabDLG->RemoveQueueItem(grabDLG->m_Q.GetItemCount()-1);

        // init the link Q or hide it if we don't follow links
        if(m_isFollowLinks)
        {
            // (re)init the progress bar
			grabDLG->SetStatusFollowLinks(TRUE, m_maxFollowCount);
		}
		else
		{
			// we don't follow links
			grabDLG->SetStatusFollowLinks(FALSE, 0);
		}
		// clear all arrays
		m_Images.RemoveAll();
		m_Links.RemoveAll();

		// reset follow link counter since this is a new session
		m_linkFollowed = 0;

       	// handle this URL
		if(!m_isCanceled)
            HandleURL(m_strURL);

	}
	// not canceled but stopped normally
    // Beep(400,500); <- used for thread debuging
	if (!m_isCanceled)
	{
		grabDLG->ResetStatus();
		grabDLG->GetDlgItem(IDCANCEL)->SetWindowText("Close");
	}
	m_isCanceled = TRUE;
}
//----------------------------------------------------------------------------------
//	protected Function CleanFunc(WPARAM n1, LPARAM n2)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This function is executed when the thread receives a WM_CLEAN message
//  from its associated window. That means the user pressed cancel to stop
//  the grabbing process.
//
// @param WPARAM n1					usually NULL
// @param LPARAM n2					usually NULL
//
//----------------------------------------------------------------------------------
void CPixGrabThread::CleanFunc(WPARAM n1, LPARAM n2)
{

	TRACE("PixGrabThread : CLEAN UP Thread ID = 0x%X\n",
		AfxGetThread()->m_nThreadID);

	ExitInstance();
}

//----------------------------------------------------------------------------------
//	protected Function InitInstance()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This function is an override of the CWinThread::InitInstance() function.
//  Typically, you override InitInstance to perform tasks that must be completed
//  when a thread is first created.
//
// @return TRUE if initialization is successful; otherwise FALSE.
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::InitInstance()
{
	// init global vars
	m_pSession = NULL;
	m_timeOut = 5;
	m_infoStatusCode=0;
	m_serverSoft = "";

	// init counters
	m_linkFollowed = 0;
	m_pixDownloaded = 1;
	m_downloadedData = 0;

	// set debug mode
	m_verboseMode = FALSE;

	// we are running
	m_isCanceled = FALSE;

	// init the link & image arrays
	m_Links.SetSize(MAX_LINK_COUNT);
	m_Images.SetSize(MAX_IMAGES_COUNT);

	// just to be sure
	m_Links.RemoveAll();

	// convert the m_pMainWnd to CPixGrabDlg
	grabDLG = (CPixGrabDlg *)m_pMainWnd;

	TRACE("PixGrabThread : Current Thread ID = 0x%X\n",
		AfxGetThread()->m_nThreadID);

	return TRUE;
}

//----------------------------------------------------------------------------------
//	protected Function ExitInstance()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This member function is called from within the Run member function.
//  Its used only in user-interface threads and it is responsible to delete the
//  Thread object if m_bAutoDelete is TRUE.
//  It's an Override of the CWinThread::ExitInstance function and
//  used also to perform additional clean-up when the thread terminates.
//
// @return The thread's exit code; 0 indicates no errors,
//         and values greater than 0 indicate an error.
//         This value can be retrieved by calling GetExitCodeThread.
//
//----------------------------------------------------------------------------------
int CPixGrabThread::ExitInstance()
{
	if(m_hasStarted)
	{
		// close the CInternetSession
		Close();
		// clean up the COM subsystem
		CoUninitialize();
	}
	TRACE("PixGrabThread : Thread ID = 0x%X exits...\n",
		AfxGetThread()->m_nThreadID);

	return CWinThread::ExitInstance();
}
//--[thread init / close functions]-------------------------------------------------

//----------------------------------------------------------------------------------
//	private Function Initialise(LPCTSTR szAgentName /*=NULL*/)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This function is responsible for creating the CInternetSession
//  needed in the GetWebFile() function. The session object is accessed through
//  m_pSession.
//
// @param  LPCTSTR szAgentName		the agent name to use, e.g. "PixGrabber"
//
// @return TRUE if successfull, FALSE if unsuccessfull
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::Initialise(LPCTSTR szAgentName /*=NULL*/)
{
    Close();
	m_infoStatusCode=0;
    m_pSession = new CInternetSession(szAgentName);

	if (m_timeOut != 0)
		m_pSession->SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT, m_timeOut);

	return (m_pSession != NULL);
}

//----------------------------------------------------------------------------------
//	private Function Close()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  If the CInternetSession in m_pSession is initialized, this member function
//  closes it.
//
//----------------------------------------------------------------------------------
void CPixGrabThread::Close()
{
    if (m_pSession)
    {
        m_pSession->Close();
        delete m_pSession;
    }
    m_pSession = NULL;
}

//--[URL processing functions]------------------------------------------------------

//----------------------------------------------------------------------------------
//	public Function HandleURL(CString strURL)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.5
//
// @task	:
//  This is the main member function of this class.
//  Most of the other member functions are called from within this class.
//  The function downloads the HTML page first, parses it and then it
//  uses the DownloadPix() function to download the pictures found.
//
//  If it's necessery to follow links, this function calls itself recursively
//  till the maximum count of links to follow is reached.
//
//
// @param CString strURL			the URL to hanlde
//
// @return TRUE is the URL has been handled successfully, FALSE if an error occured
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::HandleURL(CString strURL)
{
	CString htmlBuffer;
	DWORD dwRetStatus;

	// check if somebody pressed cancel
	if(m_isCanceled)
		return FALSE;

	// did the user forget to type http?
	if(strURL.Find("http://", 0) == -1)
		strURL = "http://" + strURL;

	// clear link array first
	m_Images.RemoveAll();

	// display actual URL
	grabDLG->ShowInformation(IDC_CURRURL, strURL);

	// download the URL
	GetWebFile(TRUE, strURL, htmlBuffer, "PixGrabber", 80);

	// retrieve and check the status code
	dwRetStatus = GetPageStatusCode();
	switch(dwRetStatus)
	{
	case(HTTP_STATUS_OK):
        /* we successfully downloaded the HTML page */
		// the page is stored in the htmlBuffer -> parse it
		if(ParseHTML(htmlBuffer))
		{
			// display amount of links cached
			grabDLG->ShowCachedLinks((int)m_Links.GetCount());

			// it's time to download these pix
			if(m_Images.GetCount() > 0 && !m_isCanceled)
                if(!DownloadPix())
				{
                    grabDLG->ShowInformation(IDC_CURRMSG,
						"DownloadPix() : Error in Page %s", strURL);
					return FALSE;
				}
		}else
			// could not parse this page
			grabDLG->ShowInformation(IDC_CURRMSG, "Could not parse this page...");
		break;
	case(HTTP_STATUS_NOT_FOUND):
		if(m_verboseMode)
			grabDLG->ShowInformation(IDC_CURRMSG, "Page not found : %s", strURL);
		break;
	case(HTTP_STATUS_AMBIGUOUS):
		// the server does not understand what we want
		if(m_verboseMode)
			grabDLG->ShowInformation(IDC_CURRMSG, "Server is confused...");
		break;
	case(HTTP_STATUS_DENIED):
		// page needs authentication -> just skip
		if(m_verboseMode)
			grabDLG->ShowInformation(IDC_CURRMSG, "Page needs authentication : %s", strURL);
		break;
	case(HTTP_STATUS_FORBIDDEN):
		// we don't have access here at all -> just skip
		if(m_verboseMode)
			grabDLG->ShowInformation(IDC_CURRMSG, "Access denied for : %s", strURL);
		break;
	case(HTTP_STATUS_GONE):
		// the ressource is no longer available -> just skip
		if(m_verboseMode)
			grabDLG->ShowInformation(IDC_CURRMSG, "URL is gone : %s", strURL);
		break;
	case(HTTP_STATUS_HOST_NF):
		// host does not exist
		if(m_verboseMode)
			grabDLG->ShowInformation(IDC_CURRMSG, "Host unknown : %s", m_strServer);
		break;
	default:
		grabDLG->ShowInformation(IDC_CURRMSG, "UNKNOWN Status: %d", dwRetStatus);
		break;
	}
	// if follow links is enabled, the function will call itself till the
	// max follow link counter is reached or the user pressed cancel
	if(m_isFollowLinks && !m_isCanceled)
	{
        // follow the links on this site
        while( m_Links.GetCount() != 0 && m_linkFollowed++ < m_maxFollowCount
			&& !m_isCanceled)
        {
			grabDLG->m_ProgressLinks.StepIt();
			// display links followed
			grabDLG->ShowFollowedLinks(m_linkFollowed);
            // get the link at 0
            m_strURL = m_Links.GetAt(0);
            // delete link to save memory
            m_Links.RemoveAt(0);
            // display current links in cache
            grabDLG->ShowCachedLinks((int)m_Links.GetCount());
            // handle this URL recursively
			if(!HandleURL(m_strURL) && !m_isCanceled)
			{
				grabDLG->ShowInformation(IDC_CURRMSG,
						"HandleURL() : Error in Page %s", m_strURL);
				continue;
			}
		}
	}
	// display ok message
    if (!m_isCanceled)
	      grabDLG->ShowInformation(IDC_CURRMSG, "DONE");
	return TRUE;
}

//----------------------------------------------------------------------------------
//	public Function OpenWebFile()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function opens a web ressource by use of the CInternetSession class.
//
// @param CString actURL			URL to process
// @param LPCTSTR szAgentName		agent name to use
// @param int nPort					port to connect to
//
// @return Pointer to a valid CHttpFile*, NULL if not successfull
//
//----------------------------------------------------------------------------------
CHttpFile* CPixGrabThread::OpenWebFile(CString actURL,
                                       LPCTSTR szAgentName,
                                       int nPort)
{
	// check if somebody pressed cancel
	if(m_isCanceled)
		return FALSE;

	INTERNET_PORT usPort = nPort;

	// use the default internet access
	DWORD dwAccessType = PRE_CONFIG_INTERNET_ACCESS;

	DWORD dwHttpRequestFlags = INTERNET_FLAG_DONT_CACHE
							   | INTERNET_FLAG_TRANSFER_BINARY;
	DWORD dwServiceType;
	CString strObject;
	// init the status code
	m_infoStatusCode=0;

	// init the session
    if (!m_pSession && !Initialise(szAgentName))
        return FALSE;

	// parse the URL (even though OpenURL will parse it again)
	// -> we need the server later
	if (!AfxParseURL(actURL, dwServiceType, m_strServer, strObject, usPort))
	{
        return FALSE;
	}

    // we only want pictures and html pages
	CString szHeaders = "Accept: image/jpeg, image/gif, image/jpg, image/png, image/mng, image/bmp, text/plain, text/html, text/htm\r\n";

	CHttpFile* pwebFile = NULL;

	// open the URL, catch thrown exceptions
    try
    {
        pwebFile = (CHttpFile*) m_pSession->OpenURL(actURL, 1,
												 dwHttpRequestFlags,
												 szHeaders
                                                 );

	}
    catch (CInternetException* e)
    {
        TCHAR   szCause[255];
        e->GetErrorMessage(szCause, 255);
		grabDLG->ShowInformation(IDC_CURRMSG, szCause);
        // e->ReportError();
        e->Delete();
        delete pwebFile;
        pwebFile = NULL;
        return NULL;
    }
	return pwebFile;
}

//----------------------------------------------------------------------------------
//	public Function GetWebFile(...)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.1
//
// @task	:
//  Function downloads a web ressource.
//  It calls the member function OpenWebFile() first to get a pointer to a
//  CHttpFile*. Then it stores the content of the web ressource in 'strBuffer'.
//  If the parameter isPage is set to TRUE, the function also retrives the
//  server software used by the server. If it's FALSE the function checks
//  if the size of the picture matches the range the user specified
//
// @param BOOL isPage				picture or HTML page?
// @param CString actURL			URL to process
// @param CString& strBuffer		pointer to a buffer to store downloaded data
// @param LPCTSTR szAgentName		agent name to use
// @param int nPort					port to connect to
//
// @return HTTP_STATUS_CODE (200=OK,..., 800=size does not match)
//
//----------------------------------------------------------------------------------
DWORD CPixGrabThread::GetWebFile(BOOL isPage,
								 CString actURL,
								 CString& strBuffer,
								 LPCTSTR szAgentName,
								 int nPort)
{
	// check if somebody pressed cancel
	if(m_isCanceled)
		return FALSE;

	// clear the buffer
	strBuffer.Empty();

	// define output messages
	if(isPage && !m_verboseMode)
		grabDLG->ShowInformation(IDC_CURRMSG, "Downloading page...");
	else if(isPage && m_verboseMode)
		grabDLG->ShowInformation(IDC_CURRMSG, "Downloading URL : %s", actURL);

	// open web file
	CHttpFile* pFile = NULL;
	if((pFile = OpenWebFile(actURL, szAgentName, 80)) == NULL)
		return -1;								// could not create pFile
	// query status
	pFile->QueryInfoStatusCode(m_infoStatusCode);

	// status = not found -> return
	if(m_infoStatusCode == HTTP_STATUS_NOT_FOUND)
	{
		pFile->Close();
        delete pFile;
		return m_infoStatusCode;
	}

	if(isPage)
	{
		// query server software
        pFile->QueryInfo(HTTP_QUERY_SERVER, m_serverSoft);
		if(m_serverSoft.IsEmpty())
            grabDLG->ShowInformation(IDC_SERVERSOFT, "unknown");
        else
            grabDLG->ShowInformation(IDC_SERVERSOFT, m_serverSoft);

	}
    else
    {
        // query picture size before downloading
		// -> is size is not between m_minSize and m_maxSize
		// return value -1
		CString size;
		int actSize;
		pFile->QueryInfo(HTTP_QUERY_CONTENT_LENGTH, size);
		actSize = atoi(size.GetBuffer());
		if(!CheckPictureSize(actSize))
		{
			m_lastSize = actSize;

			pFile->Close();
            delete pFile;
            return (m_infoStatusCode = HTTP_STATUS_SIZE);		// we define this as "file size does not match"
		}
	}
    DWORD startTick = ::GetTickCount();
	// we need to sleep a bit to have a tick difference for small pictures
	Sleep(20);
	// status = ok -> read web ressource into buffer
    LPSTR pBuf = NULL;
	DWORD dwCount = 0;
    if (pFile)
    {
        pBuf = (LPSTR) ::GlobalAlloc(GMEM_FIXED, BUFFER_SIZE+1);
        if (!pBuf)
        {
            pFile->Close();
            delete pFile;
            return FALSE;
        }

        BYTE buffer[BUFFER_SIZE+1];
        try {
            UINT nRead = 0;
            dwCount = 0;
			// read bytes and copy it to buffer
            do
            {
                nRead = pFile->Read(buffer, BUFFER_SIZE);
                if (nRead > 0)
                {
                    buffer[nRead] = 0;

                    LPTSTR ptr = strBuffer.GetBufferSetLength(dwCount + nRead + 1);
                    memcpy(ptr+dwCount, buffer, nRead);

                    dwCount += nRead;
                    strBuffer.ReleaseBuffer(dwCount+1);
                }
            }
            while (nRead > 0);
        }
        catch (CFileException *e)
        {
            TCHAR   szCause[255];
            e->GetErrorMessage(szCause, 255);
			grabDLG->ShowInformation(IDC_CURRMSG, szCause);
            e->Delete();
            delete pFile;
            ::GlobalFree(pBuf);
            return FALSE;
        }
		// measure time used for download
		DWORD endTick = ::GetTickCount();
		double msecElapsed = (double)endTick - (double)startTick;

		if (msecElapsed > 0.0)
        {
			// display Kb/s
            m_transferRate = (double)dwCount / 1024.0 / (msecElapsed / 1000);

		}
        else
        {
			// this should never happen since we sleep to get a tick difference
			grabDLG->ShowInformation(IDC_CURRMSG, "Error: Could not measure transfer rate");
			return FALSE;
        }

        pFile->Close();
	  ::GlobalFree(pBuf);
        delete pFile;
    }

	return m_infoStatusCode;
}

//----------------------------------------------------------------------------------
//	public Function ParseHTML(CString& htmlBuffer)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.1
//
// @task	:
//  This function uses the COM interface to extract all <img> and <a> tags
//  from a HTML file. The resulting URLs are passed to the member function
//  PrepareURL(), which checks wheter an URL is usefull.
//
// @return TRUE if successfull, FALSE if unsuccessfull
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::ParseHTML(CString& htmlBuffer)
{
	// check if somebody pressed cancel
	if(m_isCanceled)
		return FALSE;

	grabDLG->ShowInformation(IDC_CURRMSG, "Parsing Page...");


	// whe have to be sure m_strURL is the current path
	// without the current object
	RemoveObject(m_strURL);

	// is the service type missing?
	if(m_strURL.Find("http://") == -1)
		m_strURL = "http://" + m_strURL;


	// declare our MSHTML variables and create a document
	MSHTML::IHTMLDocument2Ptr pDoc;
	MSHTML::IHTMLDocument3Ptr pDoc3;
	MSHTML::IHTMLElementCollectionPtr pACollection;
	MSHTML::IHTMLElementCollectionPtr pICollection;
	MSHTML::IHTMLElementPtr pAElement;
	MSHTML::IHTMLElementPtr pIElement;

	// create instance
	HRESULT hr = CoCreateInstance(CLSID_HTMLDocument, NULL, CLSCTX_INPROC_SERVER,
        IID_IHTMLDocument2, (void**)&pDoc);
	// check if successfull
	if(hr != S_OK)
		return FALSE;

	// put the code into SAFEARRAY and write it into document
	SAFEARRAY* psa = SafeArrayCreateVector(VT_VARIANT, 0, 1);
	if(psa == NULL)
		return FALSE;

	VARIANT *param;
	bstr_t bsData = (LPCTSTR)htmlBuffer;
	hr = SafeArrayAccessData(psa, (LPVOID*)&param);
	if(hr != S_OK)
		return FALSE;

	param->vt = VT_BSTR;
	param->bstrVal = (BSTR)bsData;
	// -> this will disable pop up windows
	pDoc->put_designMode(bstr_t("On"));

	hr = pDoc->write(psa);
	if(hr != S_OK)
		return FALSE;

	hr = pDoc->close();
	if(hr != S_OK)
		return FALSE;

	// destroy the SAFEARRAY
	SafeArrayDestroy(psa);

	// NOTE: IHTMLDocument3 retrieve tag function is only available in IE5+
	pDoc3 = pDoc;

	// get all imgages (img tag) and all link tags (a tag)
	BSTR bstrITAG = ::SysAllocString( L"img" );
	BSTR bstrATAG = ::SysAllocString( L"a" );
	pACollection = pDoc3->getElementsByTagName(bstrATAG);
	pICollection = pDoc3->getElementsByTagName(bstrITAG);
	// free memory
	::SysFreeString( bstrATAG );
	::SysFreeString( bstrITAG );
	// go through all elements a check the URL
	for(long i=0; i<pACollection->length; i++){
		pAElement = pACollection->item(i, (long)0);
		if(pAElement != NULL)
		{
			CString checkURL = (LPCTSTR)bstr_t(pAElement->getAttribute("href", 2));
			checkURL.MakeLower();
			// handle a link to a pix
			if(checkURL.Find(".jpg") != -1
			|| checkURL.Find(".jpeg") != -1
			|| checkURL.Find(".gif") != -1
			|| checkURL.Find(".png") != -1)
			// its actually a picture
                PrepareURL(checkURL, FALSE);
			else
				PrepareURL(checkURL, TRUE);

		}

	}
	for(long i=0; i<pICollection->length; i++){
		pIElement = pICollection->item(i, (long)0);
		if(pIElement != NULL)
			PrepareURL((LPCTSTR)bstr_t(pIElement->getAttribute("src", 2)), FALSE);
	}

	return TRUE;
}

//----------------------------------------------------------------------------------
//	public Function PrepareURL(CString strURLObject, BOOL isLink)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.4
//
// @task	:
//  This function checks the URLs passed to it. It will only store valid links and
//  pictures to the member variables m_Images and m_Links.
//
// @param CString strURLObject		URL to parse
// @param BOOL isLink				is it a link or a picture?
//
// @return TRUE if the URL can be used, FALSE if the URL is not usefull for us
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::PrepareURL(CString strURLObject, BOOL isLink)
{
	// we did not receive anything at all
	if(strURLObject.IsEmpty())
		return FALSE;

	// this is not what we want
	if(strURLObject.Find("#", 0) != -1)
		return FALSE;

	// objects needed for parsing
	CString		  act_strServer;
	DWORD		  act_dwServiceType;
	CString		  act_strObject;
	INTERNET_PORT act_Port;
	CString       servicePrefix = "http://";

	// first parsing of the URL
	AfxParseURL(strURLObject, act_dwServiceType, act_strServer, act_strObject, act_Port);
	if(act_dwServiceType == AFX_INET_SERVICE_HTTP)
	{
		// everything is ok
	}
	else
	{
		// we need some parsing
		if(act_dwServiceType == AFX_INET_SERVICE_FTP)
			return FALSE;
		if(act_dwServiceType == AFX_INET_SERVICE_MAILTO)
			return FALSE;
		if(act_dwServiceType == AFX_INET_SERVICE_FILE)
			return FALSE;

		// is it the server without service type?
		if(m_strURL.Find(strURLObject) != -1)
			return FALSE;

		// is it a relative path (beginning with ../ ./)
		if(strURLObject[0] == '.')
		{
			if(strURLObject.Find("../") != -1)
				HandleRelativePath2P(strURLObject);
			else
                HandleRelativePath1P(strURLObject);
		}
		else if(strURLObject[0] == '/')
		{
			if(m_strServer.Find("http://") == -1)
				strURLObject = servicePrefix + m_strServer + strURLObject;
			else
				strURLObject = m_strServer + strURLObject;
		}
		// it's a relative path but without metachar as starter
		else if(strURLObject.Find(m_strServer) == -1)
            HandleRelativePath0P(strURLObject);

		// it's a link to itself -> dangerous because of loops
		if(strURLObject[0] == '/' && strURLObject.GetLength() == 1)
			return FALSE;

		// final check
		if (!AfxParseURL(strURLObject, act_dwServiceType, act_strServer, act_strObject, act_Port))
		{
			// this should not happen
			grabDLG->ShowInformation(IDC_CURRMSG, "Final URL check failed: Internal program error!");
			Sleep(2000);
			::ExitProcess(1);
			return FALSE;
		}
	}

	// if it's a picture -> check type
	if(m_pixTypes != 'A' && !isLink)
    {
        if(!CheckPictureType(strURLObject, m_pixTypes))
        {
            if(m_verboseMode)
                grabDLG->ShowSourceInfo(ExtractPicName(strURLObject),
                0, // we omit the size here since we'll skip it anyway
                0,
                "TYPE",
                FALSE);
            return FALSE;
		}
	}

	// do we already have this object?
	if(!isLink)
	{
        for(int i=0; i < m_Images.GetCount(); i++)
		{
            if(m_Images.GetAt(i) == servicePrefix + act_strServer + act_strObject)
                return FALSE;
		}
		// we don't want dynamic stuff here
		if(strURLObject.Find("?") != -1)
			return FALSE;
	}
	else
	{
        for(int i=0; i < m_Links.GetCount(); i++)
		{
            if(m_Links.GetAt(i) == servicePrefix + act_strServer + act_strObject)
                return FALSE;
		}
		// only accept common web pages
		strURLObject.MakeLower();
		if(strURLObject.Find(".html") == -1
			&& strURLObject.Find(".htm") == -1
			&& strURLObject.Find(".php") == -1
			&& strURLObject.Find(".php3") == -1
			&& strURLObject.Find(".php4") == -1
			&& strURLObject.Find(".asp") == -1)
			return FALSE;

	}

	// add it to the CStringArray
	if(isLink)
        m_Links.Add(servicePrefix + act_strServer + act_strObject);
	else
	{
		m_Images.Add(servicePrefix + act_strServer + act_strObject);
	}

	// free unused memory above the upper bound
	m_Images.FreeExtra();
	m_Links.FreeExtra();

	return TRUE;

}

//----------------------------------------------------------------------------------
//	public Function DownloadPix()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.3
//
// @task	:
//  This member function downloads the pix stored in the array m_Images.
//  If the picture does not exist, the function just skips
//  this picture and continues with the next one.
//
// @return TRUE if the download was successfull, FALSE if the m_Images
//         array was empty or it was not possible to create the directory for
//		   storage.
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::DownloadPix()
{
	CString filePath;
	CString fileBuffer;

	// we don't know if any pictures found are valid to save
	m_isPixFound = FALSE;
	// pictures found? -> create dir for storage
	if(m_Images.GetCount() > 0)
	{
        if(!PrepareDirs())
            return FALSE;
	}
	else
		return FALSE;

	// init progress bar
	grabDLG->m_ProgressPix.SetPos(0);
	grabDLG->m_ProgressPix.SetStep(1);
	grabDLG->m_ProgressPix.SetRange(0, (int)m_Images.GetCount());

	// process the images in the array-> delete the one processed
	while(m_Images.GetCount() != 0 && !m_isCanceled)
	{
		CString actImage = m_Images.GetAt(0);
		// remove this image from the array
		m_Images.RemoveAt(0);

		// display the actual pix cached
		grabDLG->ShowCachedPix((int)m_Images.GetCount());
		// get status code
		DWORD myStatus = GetWebFile(FALSE, actImage, fileBuffer, "PixGrabber", 80);

		CString ImageName;
		CFile saveFile;

		switch(myStatus)
        {
		case(HTTP_STATUS_OK):
			// successfully downloaded picture
			if(!m_isPixFound)
				m_isPixFound = TRUE;

            grabDLG->ShowSourceInfo(ExtractPicName(actImage),
                fileBuffer.GetLength(),
                m_transferRate,
                "OK",
                TRUE);
            grabDLG->ShowDownloadedPix(m_pixDownloaded++);

			m_downloadedData = m_downloadedData + fileBuffer.GetLength();
			grabDLG->ShowDownloadedData(m_downloadedData);


			// we need the name of the image file
			ImageName = ExtractPicName(actImage);

			filePath.Format("%s\\%s", m_finalDir, ImageName);

            try
            {
				// if this is zero -> error
				// possible reason: -strange metachars in the path
				//                  -2 threads accessing the same file
				//                     at the same time (very unlikely)
				// -> skip download of this pix and continue
				if(saveFile.Open(filePath,
                    CFile::modeCreate|CFile::modeWrite|CFile::typeBinary) == 0)
					continue;

                saveFile.Write(fileBuffer, fileBuffer.GetLength());

                saveFile.Close();
            }
            catch(CFileException *e)
            {
				// catch the exception and continue
                TCHAR   szCause[255];
				e->GetErrorMessage(szCause, 255);
				grabDLG->ShowInformation(IDC_CURRMSG, szCause);
				e->Delete();
				saveFile.Close();

			}
			// this pix has been downloaded successfully
			grabDLG->m_ProgressPix.StepIt();
			break;

		case(HTTP_STATUS_NOT_FOUND):

			// the pix could not be found on the server
			if(m_verboseMode)
				grabDLG->ShowSourceInfo(ExtractPicName(actImage),
                fileBuffer.GetLength(),
                0,
                "N_F",
				FALSE);
			grabDLG->m_ProgressPix.StepIt();
			break;

		case(HTTP_STATUS_SIZE):
			// the pix does not match needed size
			if(m_verboseMode)
				grabDLG->ShowSourceInfo(ExtractPicName(actImage),
                m_lastSize,
                0,
                "SIZE",
				FALSE);
			grabDLG->m_ProgressPix.StepIt();
			break;
		}
    }
	// if no pictures have been saved -> delete dir
	if(!m_isPixFound)
	{
		if(!RemoveDirectory(m_finalDir))
		{
			grabDLG->ShowInformation(IDC_CURRMSG,
				"Could not remove dir: %s", m_finalDir);
		}
	}

	return TRUE;
}

//--[check / util functions]-------------------------------------------------------

//----------------------------------------------------------------------------------
//	public Function SetTimeOut(DWORD timeOut)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This function is used to set the timeout for downloading operations with the
//  CInternetSession (m_pSession).
//
// @param DWORD timeOut				timeout to use. 5s is used if nothing else is
//									specified.
//
//----------------------------------------------------------------------------------
void CPixGrabThread::SetTimeOut(DWORD timeOut)
{
	m_timeOut = timeOut;
}

//----------------------------------------------------------------------------------
//	public Function PrepareDirs()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.2
//
// @task	:
//  Function creates the directory for storage. The directory created has the format:
//  $rootDir$\Actual_Date\m_strServer. The path is then stored in m_finalDir, which
//  is used by the member function DownloadPix().
//
// @return TRUE if directory was created successfully, FALSE if m_strServer is
//         empty or the directory could not be created (permission denied, ...)
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::PrepareDirs()
{
	// check if somebody pressed cancel
	if(m_isCanceled)
		return FALSE;

	// we don't know the server address
	if(m_strServer.IsEmpty())
		return FALSE;

	CString rootDir = CString(home) + CString("\\download\\");
	CString strDate;
	CString strFinalDir;

	// get current date and format it
	COleDateTime actDate = COleDateTime::GetCurrentTime();
	strDate = actDate.Format(_T("%d-%m-%Y"));

	strFinalDir = rootDir + strDate + "\\" + m_strServer;
	// this is actually not necessary, since it is created in MainFrame
	// ...but we never know :)
	if(SetCurrentDirectory( rootDir ) == 0)
	{
		CreateDirectory(rootDir, NULL);
	}
	if(SetCurrentDirectory( rootDir + strDate ) == 0)
	{
		CreateDirectory(rootDir + strDate, NULL);
	}
	if(SetCurrentDirectory( strFinalDir ) == 0)
	{
		if(CreateDirectory(strFinalDir, NULL) == 0)
		{
			LPVOID lpMsgBuf;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                FORMAT_MESSAGE_FROM_SYSTEM |
                FORMAT_MESSAGE_IGNORE_INSERTS,
                NULL,
                GetLastError(),
                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                (LPTSTR) &lpMsgBuf,
                0,
                NULL);

            grabDLG->ShowInformation(IDC_CURRMSG, (LPCTSTR)lpMsgBuf);
			return FALSE;
		}
	}
	// DownloadPix will use this
	m_finalDir = strFinalDir;
	if(m_verboseMode)
        grabDLG->ShowInformation(IDC_CURRMSG, "SaveDir: %s", strFinalDir);

	return TRUE;
}

//----------------------------------------------------------------------------------
//	public Function CheckPictureType(char Type, CStringArray pixArray)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task:
//  Function checks if the picture specified with pixURL meets the type the user
//  specified. At the moment the following types are known:
//			'J' = jpg only (extension .jpg, .jpeg)
//          'G' = gif only
//
// @param CString pixURL			URL of a picture
// @param char type					type to compare with picture
//
// @return TRUE if the picture is of the given type, FALSE if not or an unknown
//         picture type has been specified.
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::CheckPictureType(CString pixURL, char type)
{
	pixURL.MakeLower();
	if(type == 'J')
	{
		// is it a jpg picture?
		if(pixURL.Find(".jpg") != -1
			|| pixURL.Find(".jpeg") != -1)
			return TRUE;
	}
	else if(type == 'G')
	{
		// is it a gif picture
		if(pixURL.Find(".gif", 0) != -1)
			return TRUE;
	}

	return FALSE;
}

//----------------------------------------------------------------------------------
//	public Function CheckPixtureSize(int actSize)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function compares the actual size of a picture with the values the user
//  specified (stored in m_minSize, m_maxSize)
//
// @param int actSize				actual size in bytes
//
// @return TRUE if picture is in requested range, FALSE if not
//
//----------------------------------------------------------------------------------
BOOL CPixGrabThread::CheckPictureSize(int actSize)
{
	if((actSize >= (m_minSize * 1024.0)) && (actSize <= (m_maxSize* 1024.0)))
		return TRUE;

	return FALSE;
}

//----------------------------------------------------------------------------------
//	public Function ExtractPicName(CString picURL)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function takes an URL of a picture as an argument and extracts the pictures name
//  from it, e.g. http://localhost/img/test.jpg -> test.jpg
//
// @param CString picURL			URL of a picture
//
// @return CString ImageName		the name of the picture
//
//----------------------------------------------------------------------------------
CString CPixGrabThread::ExtractPicName(CString picURL)
{
	CString ImageName;

	int z = picURL.GetLength();
	int l = z;
	// walk through the URL till first slash
	while(picURL[z] != '/')
				z--;
	// extract the name
	ImageName = picURL.Right(l - (z+1));

	return ImageName;

}

//----------------------------------------------------------------------------------
//	public Function RemoveObject(CString &strURL)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function takes an URL as an argument and removes the object (if available).
//  For example: http://localhost/site1/images/bcd011.gif becomes
//               http://localhost/site1/images
//
// @param CString &strURL			URL to remove Object (URI) from
//
//----------------------------------------------------------------------------------
void CPixGrabThread::RemoveObject(CString &strURL)
{
	if(strURL.Find("/", 7) != -1)
	{
        BOOL foundPoint = FALSE;
        int counter = 0;
        int z = m_strURL.GetLength() -1;
        while(strURL[z] != '/' && counter != 1 && !foundPoint)
		{
            if(strURL[z] == '/')
                counter++;
            if(strURL[z] == '.')
                foundPoint = TRUE;
            z--;
         }
        // cut out the object
        if(foundPoint && counter == 0)
        {
            int z = m_strURL.GetLength();
            while(strURL[z] != '/')
                z--;
            strURL = m_strURL.Left(z);
        }
	}
}

//----------------------------------------------------------------------------------
//	public Function HandleRelativePath2P(CString &strObject)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function takes an object representing a relative path (unix style: ../)
//  and concatenates it with the actual m_strURL to get a valid
//  path for downloading
//  For example: m_strURL = "http://localhost/images/houses"
//               strObject= "../intro.gif"
//  return   ==> strObject= "http://localhost/images/intro.gif"
//
// @param CString &strObject		Object to concatenate with m_strURL
//
//----------------------------------------------------------------------------------
void CPixGrabThread::HandleRelativePath2P(CString &strObject)
{
	int counter = 0;
	int z = m_strURL.GetLength();
	int l = z;
	// may be there's a slash at the end
	if(m_strURL[m_strURL.GetLength()-1] == '/')
		z -= 2;
	// walk through the URL till first slash
	while(m_strURL[z] != '/')
		z--;
	// extract the name
	strObject = m_strURL.Left(z) +
		strObject.Right(strObject.GetLength() -2);
}

//----------------------------------------------------------------------------------
//	public Function HandleRelativePath1P(CString &strObject)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function takes an object representing a relative path (unix style: ./)
//  and concatenates it with the actual m_strURL to get a valid
//  path for downloading
//  For example: m_strURL = "http://localhost/images/houses"
//               strObject= "./intro.gif"
//  return   ==> strObject= "http://localhost/images/houses/intro.gif"
//
// @param CString &strObject		Object to concatenate with m_strURL
//
//----------------------------------------------------------------------------------
void CPixGrabThread::HandleRelativePath1P(CString &strObject)
{
	int pathPos = strObject.Find("./");
	if(pathPos != -1)
	{
		CString Object = strObject.Right(strObject.GetLength()
			- (pathPos + 2));
		if(m_strURL[m_strURL.GetLength()-1] == '/')
			strObject = m_strURL + Object;
		else
			strObject = m_strURL + "/" + Object;
	}
}

void CPixGrabThread::HandleRelativePath0P(CString &strObject)
{
	CString strTempURL = m_strURL;

	int objLength = strObject.GetLength();
	int pathPos = strObject.Find("./", 0);
	if(pathPos != -1)
		strObject = m_strURL + strObject.Right(objLength - (pathPos+1));
	else
	{
		if(m_strURL[m_strURL.GetLength()-1] == '/')
			strObject = m_strURL + strObject;
		else
			strObject = m_strURL + "/" + strObject;
	}
}

//--[status functions]-------------------------------------------------------------

//----------------------------------------------------------------------------------
//	public Function GetTransferRate()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This function returns the last measrued transfer rate to the caller.
//
// @return double m_transferRate	last measured transfer rate (Kb/s)
//
//----------------------------------------------------------------------------------
double CPixGrabThread::GetTransferRate()
{
	return m_transferRate;
}

//----------------------------------------------------------------------------------
//	public Function GetPageStatusCode()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This function returns the last status code to the caller.
//
// @return DWORD m_infoStatusCode	last status code
//
//----------------------------------------------------------------------------------
DWORD CPixGrabThread::GetPageStatusCode()
{
	return m_infoStatusCode;
}
