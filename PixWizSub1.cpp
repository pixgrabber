//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CPixWizSub1
// File		:	PixWizSub1.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class is to handle the first sub page in the wizard.
//			    The first sub page is used to welcome the user for entering the 
//			    values.
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixWizSub1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CPixWizSub1::CPixWizSub1(CWnd* pParent /*=NULL*/)
	: CNewWizPage(CPixWizSub1::IDD, pParent)
{
		// NOTE: the ClassWizard will add member initialization here
}


void CPixWizSub1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ST_CAPTION, m_CaptionCtrl);
}


BEGIN_MESSAGE_MAP(CPixWizSub1, CNewWizPage)

END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the font for the title.
//
// @return  : TRUE if successfull, ....
//
//---------------------------------------------------------------------------------- 

BOOL CPixWizSub1::OnInitDialog() 
{
	CNewWizPage::OnInitDialog();
	
// for this page, we will use the base classes' large font
	m_CaptionCtrl.SetFont(&m_LargeFont, TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
}



//----------------------------------------------------------------------------------
//  Function OnQueryCancel
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the cancel button. If the button is
//            is pressed, it shows a message box
// 
//
// @return  : TRUE if successfull, FALSE stop wizard ....
//
//----------------------------------------------------------------------------------

BOOL CPixWizSub1::OnQueryCancel()
{
	if (AfxMessageBox("Are you sure you want to cancel?", MB_YESNO | MB_ICONQUESTION) == IDNO) return FALSE;
	return TRUE;
}



//----------------------------------------------------------------------------------
//  Function OnWizardNext
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the "next" button.
// 
//
// @return  : 0 if successfull, ....
//
//----------------------------------------------------------------------------------

LRESULT CPixWizSub1::OnWizardNext()
{
	CNewWizPage::OnWizardNext();
	return 0;
}



//----------------------------------------------------------------------------------
//  Function GetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function has to be overwritten in this sub page. We do not get 
//            any values.
// 
// @return  : CString("")
//
//----------------------------------------------------------------------------------

CString CPixWizSub1::GetData()
{
	return CString("");
}


//----------------------------------------------------------------------------------
//  Function SetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function has to be overwritten in this sub page. We do not set 
//            new values
// 
//
//----------------------------------------------------------------------------------

void CPixWizSub1::SetData(CString data)
{
	return;
}
