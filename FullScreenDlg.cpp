//-----------------------------------------------------------------------
//	Project PixGrabber
//-----------------------------------------------------------------------
//
// CLASS	:	CPixGrabber
// File		:	PixGrabber.cpp
// Author	:	Thierry "real" Haven
// Date		:	09.12.2003
// Version	:	0.1
// Descr	:	This class provides a support for viewing pictures in full 
//				screen mode. So it is a dialog box. It is resized to fit
//				the screen. The picture is shown thanks to a CImgControl
//				custom control, which is also dynamically resized
//				
//-----------------------------------------------------------------------

#include "stdafx.h"
#include "PixGrabber.h"
#include "FullScreenDlg.h"

IMPLEMENT_DYNAMIC(CFullScreenDlg, CDialog)

//--[mfc message map]----------------------------------------------------
BEGIN_MESSAGE_MAP(CFullScreenDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_CHAR()
END_MESSAGE_MAP()

//--[constructor]--------------------------------------------------------
// Do not create this dialog with domodal().. use create() function instead..
// see messagepump() for more details.
CFullScreenDlg::CFullScreenDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFullScreenDlg::IDD, pParent)
{
	m_escape = FALSE;
	m_smartmode = SMART_DIM_ALWAYS;
	m_piclist.CreateObject();
}

CFullScreenDlg::~CFullScreenDlg()
{
}

//-----------------------------------------------------------------------
//	overriden Function void DoDataExchange(CDataExchange* pDX)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Etablishes links between controls and associated variables
//
//-----------------------------------------------------------------------
void CFullScreenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMGVIEW, m_image);	// the control we need to display
											// the picture
}

//-----------------------------------------------------------------------
//  Function void MessagePump()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		This is a custom message pump. It processes the messages currently
//		in the queue. Why ? because we can and we MUST create
//		the FullScreenDlg (ie. this) with the Create() function and we need
//		to process the WM_KEYDOWN (and so on..) messages coming from my
//		friend the user in order to swap pictures in fullscreen mode.
//		We can not create FullScreenDlg with DoModal function because we
//		have no buttons and we can not intercept messages this way, even
//		with a PreTranslateMessage function. More over, we'll have to quit
//		the fullscreen more ONLY when the user guy clicked or esc was pressed.
//
//-----------------------------------------------------------------------
void CFullScreenDlg::MessagePump()
{
	MSG *pMsg = NULL;
	do
	{
		// pump message, and quit on WM_QUIT
		if (!AfxPumpMessage())
		{
			AfxPostQuitMessage(0);
			return;
		}

		if (pMsg)
		{
			// show the window when certain special messages rec'd
			if ((pMsg->message == 0x118 || pMsg->message == WM_SYSKEYDOWN))
			{
				ShowWindow(SW_SHOWNORMAL);
				UpdateWindow();

			}
		}
	} while (::PeekMessage(pMsg, NULL, NULL, NULL, PM_NOREMOVE) || !this->m_escape);
}

//-----------------------------------------------------------------------
//	Function void ScanFolderForPics(char *dir)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		This is a function which tries, and actually does, fill
//		a dynamic list/array containing all files names in a given directory.
//		The file names must have extensions like ".jpg", ".bmp" or ".gif"
//		in order to be accepted in the list/array. We have to do a such list 
//		before going into fullscreen mode so we will have directly the pictures
//		names to load.
//
//	@param :		folder to scan
//
//-----------------------------------------------------------------------
void CFullScreenDlg::ScanFolderForPics(char *dir)
{
	HANDLE searchhandle;

	searchhandle = FindFirstFile("*", &fd);
	while(FindNextFile(searchhandle, &fd))
	{
		Sleep(0);
		// if it is a directory; we scan it.
		attrib = GetFileAttributes(fd.cFileName);
		if (attrib != 0xFFFFFFFF)
		{
			if (((attrib & FILE_ATTRIBUTE_DIRECTORY) >> 4 == 1) && (lstrcmp(fd.cFileName, "..")))
			{
				// handling folder options here ...
			}
			else // else we process with the file
			{
				if (lstrcmp(fd.cFileName, "..")) // we don't want the folder ".."
				{
					if (lstrcmp(fd.cFileName, ".")) // "." neither ...
					{
						char ext[8];
						lstrcpy(ext, fd.cFileName + lstrlen(fd.cFileName) - 5);
						for(int i =1; i < 5; i++)
						{
							ext[i] = (char) tolower(ext[i]);
						}
						if (!lstrcmp(ext+1, ".jpg") ||!lstrcmp(ext, ".jpeg") || !lstrcmp(ext+1, ".bmp") || !lstrcmp(ext+1, ".gif"))
						{
							// add object in the list
							m_piclist.AddTail((CObject *)new CString(fd.cFileName));
						}
					}
				}
			}
		}
	}
	FindClose(searchhandle);
	return;
}

//-----------------------------------------------------------------------
//	Function void BuildList()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Constructs the picture name list. First we free all datas in the
//		list by deleting the objects if something exists. Then destroy the
//		list and finally rebuild it, and fill it with new datas.
//
//-----------------------------------------------------------------------
void CFullScreenDlg::BuildList()
{
	POSITION curs;
	BOOL finished = FALSE;
	curs = m_piclist.GetHeadPosition();
	CString *val = new CString();
	while(!finished)
	{
		delete(val);								// free memory
		if (m_piclist.GetTailPosition() == curs)	// queue == tail !??
		{
			break;									// dirty coding... FIXME
		}
		val = (CString *)m_piclist.GetNext(curs);
	}
	m_piclist.RemoveAll();

	ScanFolderForPics(".");							// scan current folder
}

//-----------------------------------------------------------------------
//	Function void LoadPrev()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		This function load the previous picture, depending of the one
//		currently displayed on the screen. To do this, we search
//		in the picture list the position if the current picture.
//		Once found, we load the previous picture in the list by calling
//		m_piclist.GetPrev(curs)
//
//-----------------------------------------------------------------------
void CFullScreenDlg::LoadPrev()
{
	POSITION curs;				// position in the list, kind of "cursor"
	BOOL found = FALSE;			// found name in the list ?

	if (!lstrcmp(m_pic, "")) return;	// something displayed on screen ?

	curs = m_piclist.GetTailPosition();
	CString *val = new CString();
	CString *bak = val;
	while(!found)
	{
		if (curs == NULL)		// uh.. no pictures left ?
		{
			curs = m_piclist.GetTailPosition();
			found = TRUE;
			break;
		}
		if (*val == CString(m_pic))
		{
			found = TRUE;
			break;
		}
		val = (CString *)m_piclist.GetPrev(curs);
	}
	if (found)
	{
		CString *selected = (CString *) m_piclist.GetPrev(curs);
		//::MessageBox(0, selected->GetBuffer(), 0, 0);	// debug
		char ardf[256];			// intermediate variable holding the buffer found
		lstrcpy(ardf, selected->GetBuffer());
		this->Load(ardf);
	}
	delete(bak);
}

//-----------------------------------------------------------------------
//	Function void LoadNext()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Quite the same as LoadPrev() but here we are looking for the next
//		picture in the list instead of the previous...
//		(i.e. use of m_piclist.GetNext(curs) instead of m_piclist.GetPrev(curs))
//
//-----------------------------------------------------------------------
void CFullScreenDlg::LoadNext()
{
	
	POSITION curs;
	BOOL found = FALSE;

	if (!lstrcmp(m_pic, "")) return;

	curs = m_piclist.GetHeadPosition();
	CString *val = new CString();
	CString *bak = val;
	while(!found)
	{
		if (curs == NULL)	// uh.. no pictures left ?
		{
			curs = m_piclist.GetHeadPosition();
			found = TRUE;
			break;
		}
		if (*val == CString(m_pic))
		{
			found = TRUE;
			break;
		}
		val = (CString *)m_piclist.GetNext(curs);
	}
	if (found)
	{
		CString *selected = (CString *) m_piclist.GetNext(curs);
		//::MessageBox(0, selected->GetBuffer(), 0, 0);		// debug
		this->Load(selected->GetBuffer());
	}
	delete(bak);
}

//-----------------------------------------------------------------------
//	Function void Load(char *current)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Load a picture to display in full screen
//
//	@param :		name of the picture to load
//
//-----------------------------------------------------------------------
void CFullScreenDlg::Load(char *current)
{
	CPicture pic;
	lstrcpy(m_pic, current);
	this->BuildList();				// maybe not needed ? DELETE_ME?
	if (!pic.Load(m_pic))
	{
		// debug message if picture not found
		char arf[256];
		::GetCurrentDirectory(256, arf);
		lstrcat(arf, "\\");
		lstrcat(arf, m_pic);
		::MessageBox(0, arf, "Unknown picture format", 0);
		return;
	}
	m_image.LoadPic(&pic);			// give a copy of the pic to display
}

//-----------------------------------------------------------------------
//	Function void MakeFullScreen()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Resizes current dialog control (this pointer) and CImgControl
//		to fit the entire screen. We also affect a "smart resize" to the
//		picture because we don't want it to be too small or too big...
//		Enjoy fullscreen !
//
//-----------------------------------------------------------------------
void CFullScreenDlg::MakeFullScreen()
{
	if (this->m_image)
	{
		CRect client;
		CRect desktop;
		this->GetWindowRect(client);
		this->ScreenToClient(client);
		m_image.MoveWindow(client);
		this->m_image.SetFit(false);				// fit to client area !
		this->m_image.UpdateZoom(0);
		this->m_image.m_sx = 0;
		this->m_image.m_sy = 0;
		//this->m_image.SmartResize();

		int x = ::GetSystemMetrics(SM_CXSCREEN);
		int y = ::GetSystemMetrics(SM_CYSCREEN);
		this->MoveWindow(0,0,x, y);
		this->m_image.MoveWindow(0,0,x,y);
		this->m_image.SmartResize(SMART_DIM_ALWAYS);
	}
}

//-----------------------------------------------------------------------
//	overriden Function void OnPaint()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Future use: display infos of the picture in full screen when user
//		asks for it... draw info with dc drawtext or something.
//
//-----------------------------------------------------------------------
void CFullScreenDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
}

//-----------------------------------------------------------------------
//	overriden Function BOOL PreTranslateMessage(LPMSG lpmsg)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Intercepts keyboard and mouse events, so the app knows when to
//		switch the currently displayed picture to another one.
//		Ow yeah.. F key also switches the zoom mode if picture real size
//		is smaller than screen width & height.
//
//	@param LPMSG lpmsg		see msdn
//	@return BOOL			see msdn
//
//-----------------------------------------------------------------------
BOOL CFullScreenDlg::PreTranslateMessage(LPMSG lpmsg)
{
    BOOL bHandleNow = FALSE;

    switch (lpmsg->message)
    {
		case WM_KEYDOWN:
        switch (lpmsg->wParam)
        {
			case VK_RIGHT:
				// the "normal" way to show a picture : load, zoom, resize,  invalidate
				this->LoadNext();
				this->m_image.UpdateZoom(0);
				this->m_image.SmartResize(m_smartmode);
				this->m_image.Invalidate();
			break;
			case VK_LEFT:
				this->LoadPrev();
				this->m_image.UpdateZoom(0);
				this->m_image.SmartResize(m_smartmode);
				this->m_image.Invalidate();
			break;
			case VK_ESCAPE:
				this->m_escape = TRUE;
			break;
			case VK_F5:		// kind of "refresh window"... not useful btw
				this->m_image.UpdateZoom(0);
				this->m_image.SmartResize(m_smartmode);
				this->m_image.Invalidate();
			break;
			case 70:	// 'F'
				if (m_smartmode == SMART_DIM_ALWAYS)
				{
					m_smartmode = SMART_NO_DIM_IF_SMALLER;
				}
				else
				{
					m_smartmode = SMART_DIM_ALWAYS;
				}
				this->m_image.UpdateZoom(0);			// no zoom please ..
				this->m_image.SmartResize(m_smartmode);
				this->m_image.Invalidate();
			break;
			/*
			default:			// debug: print out vk codes
				char af[256];
				wsprintf(af, "%d", lpmsg->wParam);
				::MessageBox(0, af, af, 0);
			break;*/
        }
		break;
		case WM_LBUTTONDOWN:	// leaves fullscreen mode
		case WM_RBUTTONDOWN:
			this->m_escape = TRUE;
		break;

		case WM_MOUSEWHEEL:		// mouse wheel also swaps pictures.. yey. we should party
			if ((short)HIWORD(lpmsg->wParam) < 0)
			{
				this->LoadNext();
				this->m_image.UpdateZoom(0);
				this->m_image.SmartResize(SMART_DIM_ALWAYS);
				this->m_image.Invalidate();
			}
			else if ((short)HIWORD(lpmsg->wParam) > 0)
			{
				this->LoadPrev();
				this->m_image.UpdateZoom(0);
				this->m_image.SmartResize(SMART_DIM_ALWAYS);
				this->m_image.Invalidate();
			}
		break;

        if (bHandleNow)				// DELETE_ME?
            OnKeyDown((UINT)lpmsg->wParam, LOWORD(lpmsg ->lParam), HIWORD(lpmsg->lParam));
        break;

		case WM_WINDOWPOSCHANGED :	// make it fullscreen here for example
			this->MakeFullScreen();	// but not very clean programming ... FIXME
		break;
	}
    return bHandleNow;
}
