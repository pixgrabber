//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CNewWizDialog
// File		:	NewWizDialog.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class handles all actions of the wizard
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "NewWizDialog.h"
#include "NewWizPage.h"
#include "MainFrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CNewWizDialog::CNewWizDialog(LPCTSTR lpszTemplateName, 
   CWnd* pParent):CDialog(lpszTemplateName,pParent)
{
  Init();
  m_curpage = 0;
}

CNewWizDialog::CNewWizDialog(UINT nIDTemplate, 
	CWnd* pParent):CDialog(nIDTemplate,pParent)
{
  Init();
}

CNewWizDialog::~CNewWizDialog()
{

}

void CNewWizDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);		
}

BEGIN_MESSAGE_MAP(CNewWizDialog, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(ID_WIZFINISH, OnWizardFinish)
	ON_BN_CLICKED(ID_WIZBACK, OnWizardBack)
	ON_BN_CLICKED(ID_WIZNEXT, OnWizardNext)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function Init
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used for the initialization.
//
//----------------------------------------------------------------------------------

void CNewWizDialog::Init()
{
  m_nPlaceholderID = 0;
}



//----------------------------------------------------------------------------------
//  Function DestroyPage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to destroy the page.
//
// @param   : pPage
//
// @return  : TRUE 
//----------------------------------------------------------------------------------

BOOL CNewWizDialog::DestroyPage(CNewWizPage* pPage)
{
	if (pPage->m_bCreated)
	{
		if (pPage->OnKillActive() == FALSE) return FALSE;
		pPage->OnDestroyPage();
		pPage->DestroyWindow();
		pPage->m_bCreated = FALSE;
	}
	return TRUE;
}



//----------------------------------------------------------------------------------
//  Function GetFirstPage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the first page
//
// @return  : CNewWizPage*
//----------------------------------------------------------------------------------

CNewWizPage* CNewWizDialog::GetFirstPage()
{
	return (CNewWizPage*)m_PageList.GetHead();	
}



//----------------------------------------------------------------------------------
//  Function GetLastPage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the last page
//
// @return  : CNewWizPage*
//----------------------------------------------------------------------------------

CNewWizPage* CNewWizDialog::GetLastPage()
{
	return (CNewWizPage*)m_PageList.GetTail();	
}



//----------------------------------------------------------------------------------
//  Function GetActivePage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the active page
//
// @return  : pPage
//----------------------------------------------------------------------------------

CNewWizPage* CNewWizDialog::GetActivePage() const
{
	CNewWizPage* pPage;
	POSITION Pos = m_PageList.GetHeadPosition();
	while (Pos)
	{
		pPage = (CNewWizPage*)m_PageList.GetNext(Pos);
		if (pPage->m_bActive)
		{
			return pPage;
		}
	}
	return NULL;
}



//----------------------------------------------------------------------------------
//  Function GetNextPage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the next page
//
// @return  : CNewWizPage* if successfull...
//----------------------------------------------------------------------------------

CNewWizPage* CNewWizDialog::GetNextPage()
{
	CNewWizPage* pPage;
	POSITION Pos = m_PageList.GetHeadPosition();
	while (Pos)
	{
		pPage = (CNewWizPage*)m_PageList.GetNext(Pos);
		if (pPage->m_bActive)
		{
			if (Pos == NULL) return NULL;
			return (CNewWizPage*)m_PageList.GetAt(Pos);
		}
	}
	return NULL;
}






//----------------------------------------------------------------------------------
//  Function OnWizardFinish
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the action by pressing the finish button.
//
//----------------------------------------------------------------------------------

void CNewWizDialog::OnWizardFinish() 
{
	CNewWizPage* pPage;

	pPage = GetActivePage();

	// can we kill the active page?
	if (!pPage->OnKillActive()) return;

	// notify all pages that we are finishing
	POSITION Pos = m_PageList.GetHeadPosition();
	while (Pos)
	{
		pPage = (CNewWizPage*)m_PageList.GetNext(Pos);
		if (pPage->m_bCreated)
		{
			if (!pPage->OnWizardFinish())
			{
				// data validation failed for one of the pages so we can't close
				return;
			}
		}
	}

	UpdateData(TRUE);

	// Fill in the m_summary in CMainFrame
	CMainFrame* m_pFrame = (CMainFrame *)AfxGetMainWnd();
	m_pFrame->m_summary = summary;

	// close the dialog and return ID_WIZFINISH
	CDialog::EndDialog(ID_WIZFINISH);
}


//----------------------------------------------------------------------------------
//  Function OnWizardBack
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the action by pressing the back button.
//
//----------------------------------------------------------------------------------

void CNewWizDialog::OnWizardBack() 
{
	CNewWizPage* pPage = GetActivePage();
	ASSERT(pPage);

	LRESULT lResult = pPage->OnWizardBack();
	if (lResult == -1) return; // don't change pages

	m_curpage--;

	if (lResult == 0)
	{
		POSITION Pos = m_PageList.Find(pPage);
		ASSERT(Pos);
		m_PageList.GetPrev(Pos);
		EnableFinish(FALSE);
		if (Pos == NULL) return; // the current page was the first page
		pPage = (CNewWizPage*)m_PageList.GetAt(Pos);
	}
	else
	{
		pPage = GetPageByResourceID((UINT)lResult);
		if (pPage == NULL) return;
	}

	if (!ActivatePage(pPage)) return;	

	// if we are on the first page, then disable the back button
	if (pPage == GetFirstPage())
	{
		EnableBack(FALSE);
		EnableFinish(FALSE);
	}

	// enable the next button
	EnableNext(TRUE);
}



//----------------------------------------------------------------------------------
//  Function OnWizardNext
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the action by pressing the next button.
//
//----------------------------------------------------------------------------------

void CNewWizDialog::OnWizardNext() 
{
	CNewWizPage* pPage = GetActivePage();

	ASSERT(pPage);

	LRESULT lResult = pPage->OnWizardNext();
	if (lResult == -1) return; // don't change pages

	switch(m_curpage)
	{
		case 0 : // no data on first page
		break;
		default:
			summary[m_curpage - 1] = pPage->GetData();
		break;
	}
	m_curpage++;


	if (lResult == 0)
	{
		POSITION Pos = m_PageList.Find(pPage);
		ASSERT(Pos);
		m_PageList.GetNext(Pos);
		if (Pos == NULL) return; // the current page was the last page
		pPage = (CNewWizPage*)m_PageList.GetAt(Pos);
	}
	else
	{
	    pPage = GetPageByResourceID((UINT)lResult);
		if (pPage == NULL) return;
	}

	if (!ActivatePage(pPage)) return;	

	// if we are on the last page, then disable the next button
	if (pPage == GetLastPage())
	{
		EnableNext(FALSE);
		EnableFinish(TRUE);
	}

	if (m_curpage == 5)
	{
		CNewWizPage* pPage = GetPage(5);
		pPage->SetData(summary);
	}

	EnableBack(TRUE);
}



//----------------------------------------------------------------------------------
//  Function SetPlaceholderID
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the placeholder ID
//
// @param   : nPlaceholderID -> int
//----------------------------------------------------------------------------------

void CNewWizDialog::SetPlaceholderID(int nPlaceholderID)
{
	m_nPlaceholderID = nPlaceholderID;
}



//----------------------------------------------------------------------------------
//  Function ActivatePage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to activate the new page
//
// @param   : nPlaceholderID -> int
//
// @return  : TRUE if successfull...
//----------------------------------------------------------------------------------

BOOL CNewWizDialog::ActivatePage(CNewWizPage* pPage)
{
	ASSERT(m_nPlaceholderID != 0);
	ASSERT(pPage != NULL);
	ASSERT(::IsWindow(m_hWnd));

	// if the page is not created yet, create the page now
	if (pPage->m_bCreated == FALSE)
	{
		UINT a = pPage->m_nDialogID;
		if (pPage->Create(a, this) == FALSE) return FALSE;
		pPage->m_bCreated = TRUE;
		pPage->m_pParent = this;

	  if (pPage->OnCreatePage() == FALSE) return FALSE;
	}

	// deactivate the current page
	if (!DeactivatePage()) return FALSE;

    CRect rect;
    CWnd *pWnd = GetDlgItem(m_nPlaceholderID);
    ASSERT(pWnd != NULL);
    ASSERT(IsWindow(pWnd->m_hWnd) != FALSE);
    pWnd->GetWindowRect(&rect);
    ScreenToClient(&rect);
    pPage->SetWindowPos(NULL, rect.left, rect.top, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_NOACTIVATE );
    pPage->EnableWindow(TRUE);
    pPage->ShowWindow(SW_SHOW);
    pPage->InvalidateRect(NULL);
    pPage->UpdateWindow();
    pPage->OnSetActive();
    pPage->m_bActive = TRUE;
    return TRUE;
}



//----------------------------------------------------------------------------------
//  Function DeactivatePage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to check if the current page is active or not.
//
// @return  : TRUE if successfull...
//----------------------------------------------------------------------------------

BOOL CNewWizDialog::DeactivatePage()
{
	CNewWizPage* pPage = GetActivePage();
	if (pPage == NULL) return TRUE;
    ASSERT(pPage->m_bCreated != FALSE);
    if (!pPage->OnKillActive()) return FALSE;
    pPage->ShowWindow(SW_HIDE);
    pPage->m_bActive = FALSE;
	return TRUE;
}



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the first page as an active page when 
//			  we start the wizard.
//
// @return  : TRUE if successfull...
//----------------------------------------------------------------------------------

BOOL CNewWizDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	ModifyStyleEx (0, WS_EX_CONTROLPARENT);	

	ASSERT(m_nPlaceholderID != 0);
	SetFirstPage();
	m_curpage = 0;

	return TRUE;  
}



//----------------------------------------------------------------------------------
//  Function OnDestroy
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to destroy the sub page of the wizard
//
//----------------------------------------------------------------------------------

void CNewWizDialog::OnDestroy() 
{
	CNewWizPage* pPage;
	POSITION Pos = m_PageList.GetHeadPosition();
	while (Pos)
	{
		pPage = (CNewWizPage*)m_PageList.GetNext(Pos);
		VERIFY(DestroyPage(pPage));
	}

  CDialog::OnDestroy();
}



//----------------------------------------------------------------------------------
//  Function OnWizardNext
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to add a page to the list of pages. nID is the 
//            resource ID of the page we are adding
//
// @param   : pPage
//			  nID -> UINT
//----------------------------------------------------------------------------------

void CNewWizDialog::AddPage(CNewWizPage* pPage, UINT nID)
{
	m_PageList.AddTail(pPage);
	pPage->m_nDialogID = nID;
}



//----------------------------------------------------------------------------------
//  Function SetActivePageByResource
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to activate the page with the specified dialog 
//			  resource.
//
// @param   : nResourceID -> UINT
//----------------------------------------------------------------------------------

void CNewWizDialog::SetActivePageByResource(UINT nResourceID)
{
	CNewWizPage* pPage = GetPageByResourceID(nResourceID);
	if (pPage == NULL) return;

	if (!DeactivatePage()) return;

	ActivatePage(pPage);
}



//----------------------------------------------------------------------------------
//  Function GetPageByResourceID
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to return a page object based on its dialog 
//            resource ID
//
// @param   : nResourceID -> UINT
//
// @return  : pPage if successfull...
//----------------------------------------------------------------------------------

CNewWizPage* CNewWizDialog::GetPageByResourceID(UINT nResourceID)
{
	CNewWizPage* pPage;
	POSITION Pos = m_PageList.GetHeadPosition();
	while (Pos)
	{
		pPage = (CNewWizPage*)m_PageList.GetNext(Pos);
		if (pPage->m_nDialogID == nResourceID)
		{
			return pPage;
		}
	}
	return NULL;
}



//----------------------------------------------------------------------------------
//  Function SetFirstPage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the first page as an active page
//
// @return  : TRUE if successfull...
//
//----------------------------------------------------------------------------------

BOOL CNewWizDialog::SetFirstPage()
{
	CNewWizPage* pPage = GetFirstPage();

	if (!DeactivatePage()) return FALSE;

	EnableBack(FALSE);
	
	if (m_PageList.GetCount() > 1)
	{
		EnableFinish(FALSE);
		EnableNext(TRUE);
	}
	else 
	{
		EnableFinish(TRUE);
		EnableNext(FALSE);
	}

	if (ActivatePage(pPage)) return TRUE;
	return FALSE;
}



//----------------------------------------------------------------------------------
//  Function SetNextPage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the next page as an active page
//
//----------------------------------------------------------------------------------

void CNewWizDialog::SetNextPage()
{
  CNewWizPage* pPage = GetNextPage();
	if (ActivatePage(pPage))
	{
		EnableBack(TRUE);
	}
}



//----------------------------------------------------------------------------------
//  Function EnableFinnish
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to enable the text on the back button
//
// @param   : bEnable -> BOOL
//----------------------------------------------------------------------------------

void CNewWizDialog::EnableFinish(BOOL bEnable)
{
	ASSERT(::IsWindow(m_hWnd));
	CWnd* pWnd = GetDlgItem(ID_WIZFINISH);
	ASSERT(pWnd); 
	if (pWnd)
	{
		pWnd->EnableWindow(bEnable);		
	}
}



//----------------------------------------------------------------------------------
//  Function EnableBack
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to enable the text on the back button
//
// @param   : bEnable -> BOOL
//----------------------------------------------------------------------------------

void CNewWizDialog::EnableBack(BOOL bEnable)
{
	ASSERT(::IsWindow(m_hWnd));
	CWnd* pWnd = GetDlgItem(ID_WIZBACK);
	ASSERT(pWnd);
	if (pWnd)
	{
		pWnd->EnableWindow(bEnable);		
	}
}



//----------------------------------------------------------------------------------
//  Function EnableText
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to enable the text on the next button
//
// @param   : bEnable -> BOOL
//
//----------------------------------------------------------------------------------

void CNewWizDialog::EnableNext(BOOL bEnable)
{
	ASSERT(::IsWindow(m_hWnd));
	CWnd* pWnd = GetDlgItem(ID_WIZNEXT);
	ASSERT(pWnd); 
	if (pWnd)
	{
		pWnd->EnableWindow(bEnable);		
	}
}



//----------------------------------------------------------------------------------
//  Function GetActiveIndex
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the index of the current active page
//
// @return  : nIndex -> int if successfull, otherwise -> -1
//----------------------------------------------------------------------------------

int CNewWizDialog::GetActiveIndex() const
{
	CNewWizPage* pPage;
	POSITION Pos = m_PageList.GetHeadPosition();
	int nIndex = 0;
	while (Pos)
	{
		pPage = (CNewWizPage*)m_PageList.GetNext(Pos);
		if (pPage->m_bActive)
		{
			return nIndex;
		}
		++nIndex;
	}
	return -1;
}



//----------------------------------------------------------------------------------
//  Function GetPageIndex
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the page index
//
// @param   : pPage -> CNewWizPage
//
// @return  : -1
//----------------------------------------------------------------------------------

int CNewWizDialog::GetPageIndex(CNewWizPage* pPage) const
{
	CNewWizPage* pTestPage;
	POSITION Pos = m_PageList.GetHeadPosition();
	int nIndex = 0;
	while (Pos)
	{
		pTestPage = (CNewWizPage*)m_PageList.GetNext(Pos);
		if (pTestPage == pPage)
		{
			return nIndex;
		}
		++nIndex;
	}
	return -1;
}



//----------------------------------------------------------------------------------
//  Function GetPageCount
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the count of the actual page.
//
// @return  : int
//----------------------------------------------------------------------------------

int CNewWizDialog::GetPageCount()
{
	return (int)m_PageList.GetCount();
}



//----------------------------------------------------------------------------------
//  Function GetPage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get a page based on it�s placed index in
//            the list.
//
// @param   : nPage -> int
//
// @return  : page
//----------------------------------------------------------------------------------

CNewWizPage* CNewWizDialog::GetPage(int nPage) const
{
	POSITION Pos = m_PageList.FindIndex(nPage);
	if (Pos == NULL) return NULL;
	return (CNewWizPage*)m_PageList.GetAt(Pos);
}



//----------------------------------------------------------------------------------
//  Function SetActivePage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to activate a page based on its place in the list
//
// @param   : nPage -> int
//
// @return  : TRUE if successfull...
//----------------------------------------------------------------------------------

BOOL CNewWizDialog::SetActivePage(int nPage)
{
	CNewWizPage* pPage = GetPage(nPage);
	if (pPage == NULL) return FALSE;
	ActivatePage(pPage);
	return TRUE;
}



//----------------------------------------------------------------------------------
//  Function SetActivePage
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the enabling of the edit field by 
//			  using the checkbox.
//
// @param   : pPage -> CNewWizPage
//
// @return  : TRUE if successfull...
//----------------------------------------------------------------------------------

BOOL CNewWizDialog::SetActivePage(CNewWizPage* pPage)
{
	ActivatePage(pPage);
	return TRUE;
}



//----------------------------------------------------------------------------------
//  Function OnCheckLink
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the title of the main wizard window
//
// @param   : lpszText -> LPCTSTR
//----------------------------------------------------------------------------------

void CNewWizDialog::SetTitle(LPCTSTR lpszText)
{
	ASSERT(::IsWindow(m_hWnd));
	SetWindowText(lpszText);
}



//----------------------------------------------------------------------------------
//  Function SetTitle
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the title in the wizard
//
// @param   : nIDText -> UINT
//----------------------------------------------------------------------------------

void CNewWizDialog::SetTitle(UINT nIDText)
{
	CString s;
	s.LoadString(nIDText);
	SetTitle(s);
}



//----------------------------------------------------------------------------------
//  Function SetFinishText
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : Set the text on the finish button
//
// @param   : lpszText -> LPCTSTR
//----------------------------------------------------------------------------------

void CNewWizDialog::SetFinishText(LPCTSTR lpszText)
{
	ASSERT(::IsWindow(m_hWnd));
	CWnd* pWnd = GetDlgItem(ID_WIZFINISH);
	ASSERT(pWnd); // ID_WIZFINISH
	if (pWnd)
	{
		pWnd->SetWindowText(lpszText);		
	}
}



//----------------------------------------------------------------------------------
//  Function SetFinishText
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set the "Finish" Text
//
// @param   : nIDText -> UINT
//----------------------------------------------------------------------------------

void CNewWizDialog::SetFinishText(UINT nIDText)
{
	CString s;
	s.LoadString(nIDText);
	SetFinishText(s);	
}



//----------------------------------------------------------------------------------
//  Function OnCancel
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is called if the user pressed the cancel button.
//			  
//
//----------------------------------------------------------------------------------

void CNewWizDialog::OnCancel()
{
	CNewWizPage* pPage;
	POSITION Pos = m_PageList.GetHeadPosition();
	while (Pos)
	{
		pPage = (CNewWizPage*)m_PageList.GetNext(Pos);
		if (pPage->m_bCreated)
		{
			// is it possible to cancel?
			if (pPage->OnQueryCancel() == FALSE) return;
		}
	}
	CDialog::OnCancel();
}
