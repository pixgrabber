//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CMasterDlg
// File     :   MasterDlg.h
// Author   :   isc03meal
// Date     :   02.12.2003
// Version  :   1.0
//
//----------------------------------------------------------------------------------
#pragma once

#include "NewWizDialog.h"
#include "resource.h"

class CMasterDlg : public CNewWizDialog
{
// Construction
public:
    CMasterDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_WIZARD };

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX); 

// Implementation
protected:
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};