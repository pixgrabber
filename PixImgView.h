//-----------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixImgView
// File		:	PixImgView.h
// Author	:	Thierry "real" Haven
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once
#include "PixGrabberDoc.h"
#include "ImgControl.h"
#include "resource.h"
#include "afxwin.h"

//--[class]--------------------------------------------------------------
class CPixImgView : public CFormView
{
	DECLARE_DYNCREATE(CPixImgView)

//--[variables]----------------------------------------------------------
public:
	int m_imgx;	// picture dimension x
	int m_imgy;	// picture dimension y 

	int m_sx;	// actual x scroll for the picture
	int m_sy;	// actual y scroll 

	int max_x;	// thorical maximal value for horizontal scrolling
	int max_y;	// idem for vertical scrolling

	int mode;	// fit / full screen / resize ... just to keep track of what user
				// is doing :)

	CString m_previous;		// previous shown picture (not to display twice the same)
	char m_hselected[256];	// keep track of previous selected pic.
	char m_hproject[256];	// keep track of previous document project

	enum { IDD = IDD_IMG };

	CImgControl m_image;	// custom control CImgControl handling picture display
	CButton m_b_plus;		// zoom + button
	CButton m_b_minus;		// zoom - button
	CButton m_b_fit;		// zoom fit button
	CButton m_b_full;		// full screen button
	CButton m_b_real;		// X:1 smart size button
	CButton m_b_previous;	// not used atm
	CButton m_b_next;		// not used atm
	CButton m_b_zinit;		// 1:1 scale button

	CScrollBar m_shoriz;	// scroll bars
	CScrollBar m_sverti;

	// button icons
	HICON m_i_plus;
	HICON m_i_minus;
	HICON m_i_equal;
	HICON m_i_fit;
	HICON m_i_smart;

//--[construction]-------------------------------------------------------
protected: // create from serialization only
	CPixImgView();
public:
	virtual ~CPixImgView();

//--[functions]----------------------------------------------------------
public:
	void OnDataChange(void);
	CPixGrabberDoc* GetDocument() const;

protected:
	void UpdateScrollInfo();

//--[overrides]----------------------------------------------------------
public:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual void OnDraw(CDC* pDC);		// overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnSize(UINT nType, int cx, int cy);
	virtual void OnUpdate(CView *pSender, LPARAM lHint, CObject *pHint);

//--[event handling]-----------------------------------------------------
public:
	// event handlers for buttons
	afx_msg void OnBnClickedButtonZoomplus();
	afx_msg void OnBnClickedButtonZoomminus();
	afx_msg void OnBnClickedButtonZinit();
	afx_msg void OnBnClickedButtonFit();
	afx_msg void OnBnClickedButtonReal();
	afx_msg void OnBnClickedButtonFull();

protected:
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	DECLARE_MESSAGE_MAP()

//--[VC++ generated]-----------------------------------------------------
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//--[mfc stuff]----------------------------------------------------------
#ifndef _DEBUG  // debug version in PixGrabberView.cpp
inline CPixGrabberDoc* CPixImgView::GetDocument() const
   { return reinterpret_cast<CPixGrabberDoc*>(m_pDocument); }
#endif

