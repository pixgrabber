//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixWizSub2
// File		:	PixWizSub2.h
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
//
//----------------------------------------------------------------------------------
#pragma once

#include "NewWizPage.h"
#include "resource.h"

class CPixWizSub2 : public CNewWizPage
{
// Construction
public:
	CPixWizSub2(CWnd* pParent = NULL);   // standard constructor
	void SetData(CString data);
	CString GetData();

// Dialog Data
	enum { IDD = IDD_WIZSUB2 };
	CStatic	m_CaptionCtrl;
	CFont m_Font;
	
// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	LRESULT OnWizardNext();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_PixUrl;
};
