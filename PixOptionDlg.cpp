//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixOptionDlg
// File     :   PixOptionDlg.cpp
// Author   :   isc03meal
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class is to handle all activities of the 'options' dialog.
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixOptionDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CPixOptionDlg::CPixOptionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPixOptionDlg::IDD, pParent)
{
		// NOTE: the ClassWizard will add member initialization here
}

void CPixOptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
		// NOTE: the ClassWizard will add DDX and DDV calls here
}

BEGIN_MESSAGE_MAP(CPixOptionDlg, CDialog)
	ON_BN_CLICKED(IDC_ALL, OnClickAll)
	ON_BN_CLICKED(IDC_CHECKLINK, OnChecklink)
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnClickAll
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to make the edit field (all) in the option dialog
//            enable or disable.
//
//---------------------------------------------------------------------------------- 

void CPixOptionDlg::OnClickAll() 
{
	if (IsDlgButtonChecked (IDC_ALL) == TRUE) 
	{
		GetDlgItem( IDC_JPG )->EnableWindow(FALSE);
		GetDlgItem( IDC_GIF )->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem( IDC_JPG )->EnableWindow(TRUE);
		GetDlgItem( IDC_GIF )->EnableWindow(TRUE);
	}	
}



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : Copy the values from the dialog to the controls
//
// @return  : TRUE if successfull, ....
//
//---------------------------------------------------------------------------------- 

BOOL CPixOptionDlg::OnInitDialog()
{

	CMainFrame *pmFrame = (CMainFrame *)AfxGetMainWnd();
	  	
	SetDlgItemInt (IDC_MIN, min);
    SetDlgItemInt (IDC_MAX, max);
    SetDlgItemInt (IDC_LINK, link);

	if(pmFrame->pixoptions.m_PixFollowLinks)
	{
		GetDlgItem(IDC_LINK)->EnableWindow(TRUE);
		CheckDlgButton(IDC_CHECKLINK,1);
	}
	else
	{
        GetDlgItem(IDC_LINK)->EnableWindow(FALSE);
		CheckDlgButton(IDC_CHECKLINK,0);
	}

	if(pmFrame->pixoptions.m_PixTypes == 'A')
	{
        GetDlgItem(IDC_JPG)->EnableWindow(FALSE);
        GetDlgItem(IDC_GIF)->EnableWindow(FALSE);
		CheckRadioButton(IDC_JPG, IDC_GIF, IDC_JPG);        
		CheckDlgButton (IDC_ALL,1);
	}
	else
	{
		if(pmFrame->pixoptions.m_PixTypes == 'J')
            CheckRadioButton(IDC_JPG, IDC_GIF, IDC_JPG);
		else
			CheckRadioButton(IDC_JPG, IDC_GIF, IDC_GIF);
	}	
	return TRUE;
}



//----------------------------------------------------------------------------------
//  Function OnOK
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to check the entered data from the user at the 
//            moment he click the "OK" button. If there are wrong datas, the user 
//            will receive a mesage with a statement.
//
//---------------------------------------------------------------------------------- 

void CPixOptionDlg::OnOK()
{

	CString str;
	int n;

// Message
	char msg1[] = "Please enter a minimum Size for the selected file.\n";
    char msg2[] = "Please enter a maximum Size for the selected file.\n";
	char msg3[] = "Please enter a generation.\n";
	char msg4[] = "The minimum of a file can not be bigger then the maximum.\n";
	char msg5[] = "Negativ values are not allowed here.\n";
	char msg6[] = "It is not possible to grab more then 5000 links.\n";	

// Get the entered values
	min  = GetDlgItemInt (IDC_MIN);
	max  = GetDlgItemInt (IDC_MAX);
	link = GetDlgItemInt (IDC_LINK);
	all  = IsDlgButtonChecked (IDC_ALL);
	checklink = IsDlgButtonChecked(IDC_CHECKLINK);

// Check which kind of pix are chosen
	if (all == TRUE)
	{
		pix = 'A';
	}
	else
	{
		switch(GetCheckedRadioButton(IDC_JPG,IDC_GIF))
		{
		case IDC_JPG:	pix = 'J';
			break;
		case  IDC_GIF:	pix = 'G';
			break;
		default:
			break;
		}
	}

// Check value to follow links
	if (checklink == FALSE) 
	{
		link = NULL;
	}

// Check values and leave a mesage if it's important
	if (min == NULL) 
	{
		n = str.GetLength();
		str.Insert(n, msg1);
	}
	if (max == NULL) 
	{
		n = str.GetLength();
		str.Insert(n, msg2);
	}
	if (link == NULL && checklink == TRUE)
	{
		n = str.GetLength();
        str.Insert(n, msg3); 
	}
	if (min > max) 
	{
		n = str.GetLength();
		str.Insert(n, msg4);
	}
	if (min < 0 || max < 0)
	{
		n = str.GetLength();
		str.Insert(n, msg5);
	}
	if (link > 5000) 
	{
		n = str.GetLength();
		str.Insert(n, msg6);			 
	}

	if (str.IsEmpty() == 0) 
	{
		MessageBox(str, "PixGrabber", MB_APPLMODAL);
	}
	else
	{
		CDialog::OnOK();
	}
}



//----------------------------------------------------------------------------------
//  Function OnChecklink
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to make the edit field (checklink) in the option 
//            dialog enable or disable.
//
//---------------------------------------------------------------------------------- 

void CPixOptionDlg::OnChecklink() 
{
	if (IsDlgButtonChecked (IDC_CHECKLINK) == FALSE) 
	{
		GetDlgItem( IDC_LINK )->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem( IDC_LINK )->EnableWindow(TRUE);
	}
}
