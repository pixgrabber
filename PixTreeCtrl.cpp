//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// File     :   PixTreeCtrl.cpp
// Author   :   MaEi
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "resource.h"
#include "PixTreeCtrl.h"
#include "PixTreeView.h"
#include "SortedArray.h"


#ifdef _DEBUG
	#define new DEBUG_NEW
#endif

int   CPixTreeCtrl::m_nImListCount=0;
int   CSystemImageList::m_nRefCount=0;
const UINT WM_POPULATE_TREE = WM_APP + 1;	


//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeCtrl
// File     :   PixTreeCtrl.cpp
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   Access to system icons to display in the tree.
//
//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
//  Function CSystemImageList
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Constructor
// 
//----------------------------------------------------------------------------------
CSystemImageList::CSystemImageList()
{
	if (m_nRefCount <= 0)
	{
		//Attach to the system image list
		SHFILEINFO sfi;
		hSystemImageList = (HIMAGELIST) SHGetFileInfo(_T("C:\\"),
			0, &sfi, sizeof(SHFILEINFO), SHGFI_SYSICONINDEX | SHGFI_SMALLICON);
		VERIFY(m_ImageList.Attach(hSystemImageList));
	}  
	//Increment the reference count
	m_nRefCount++;
}

//----------------------------------------------------------------------------------
//  Function ~CSystemImageList
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Destructor
// 
//----------------------------------------------------------------------------------
CSystemImageList::~CSystemImageList()
{
	//Decrement the reference count
	m_nRefCount--;  
}

//----------------------------------------------------------------------------------
//  Function GetImageList
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Returns the image list.
// 
// @return	: CImageList&
//
//----------------------------------------------------------------------------------
CImageList& CSystemImageList::GetImageList()
{
	return m_ImageList;
}

//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeCtrl
// File     :   PixTreeCtrl.h
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class implements a tree control for the entire file system.
//
//----------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CPixTreeCtrl, CTreeCtrl)
	ON_COMMAND(ID_FILE_PROPERTIES, FileProperties)
	ON_COMMAND(ID_FILE_RENAME, FileRename)
	ON_COMMAND(ID_FILE_DELETE, FileDelete)
	ON_COMMAND(ID_REFRESH, OnViewRefresh)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDING, OnItemexpanding)
	ON_NOTIFY_REFLECT(TVN_SELCHANGING, OnItemselected)
	ON_WM_CONTEXTMENU()
	ON_NOTIFY_REFLECT(TVN_ENDLABELEDIT, OnEndlabeledit)
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	ON_NOTIFY_REFLECT(NM_CLICK, OnLclick)
	ON_WM_INITMENUPOPUP()
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBegindrag)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
    ON_MESSAGE(WM_POPULATE_TREE, OnPopulateTree)
END_MESSAGE_MAP()

//----------------------------------------------------------------------------------
//  Function CPixTreeCtrl
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Constructor
//			  Initialises some default values.
//
//----------------------------------------------------------------------------------
CPixTreeCtrl::CPixTreeCtrl() : CTreeCtrl()
{
	m_bShowFiles = FALSE;	//No Files are displayed
//	m_bRootFolderSet = TRUE;
	// Drag and Drop
	m_pilDrag = NULL;
	m_hItemDrag = NULL;
	m_hItemDrop = NULL;
	m_TimerTicks = 0;
}

//----------------------------------------------------------------------------------
//  Function ~CPixTreeCtrl
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Destructor
//
//----------------------------------------------------------------------------------
CPixTreeCtrl::~CPixTreeCtrl()
{
}

//----------------------------------------------------------------------------------
//  Function CompareByFilenameNoCase
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Compares two files. Without taking the case in account
//
// @param	: CString& element1, First File Name
// @param	: CString& element2, Second File Name
//
// @return	: int, 0 if the file names are indentical.
//
//----------------------------------------------------------------------------------
int CPixTreeCtrl::CompareByFilenameNoCase(CString& element1, CString& element2) 
{
	return element1.CompareNoCase(element2);
}

#ifdef _DEBUG
/*void CPixTreeCtrl::AssertValid() const
{
	CTreeCtrl::AssertValid();
}*/

void CPixTreeCtrl::Dump(CDumpContext& dc) const
{
	CTreeCtrl::Dump(dc);
}
#endif //_DEBUG

//----------------------------------------------------------------------------------
//  Function GetIconIndex
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Retreive the default icon index for a file/folder.
//
// @param	: const CString& sFilename, Name of the file/folder.
//
// @return	: int, index number of the icon.
//
//----------------------------------------------------------------------------------
int CPixTreeCtrl::GetIconIndex(const CString& sFilename)
{
	SHFILEINFO sfi;
	if (SHGetFileInfo(sFilename, 0, &sfi, sizeof(SHFILEINFO), SHGFI_ICON | SHGFI_SMALLICON) == 0)
		return -1;
	return sfi.iIcon;
}

//----------------------------------------------------------------------------------
//  Function GetSelIconIndex
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Retreive the icon index for a selected file/folder.
//
// @param	: const CString& sFilename, Name of the file/folder.
//
// @return	: int, index number of the icon.
//
//----------------------------------------------------------------------------------
int CPixTreeCtrl::GetSelIconIndex(const CString& sFilename)
{
	SHFILEINFO sfi;
	if (SHGetFileInfo(sFilename, 0, &sfi, sizeof(SHFILEINFO), SHGFI_ICON | SHGFI_OPENICON | SHGFI_SMALLICON) == 0)
		return -1;
	return sfi.iIcon;
}

//----------------------------------------------------------------------------------
//  Function FindSibling
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Searches whether a file/folder name already exists in the directory.  
//
// @param	: HTREEITEM hParent, Handle to the item's parent.
// @param	: const CString& sItem, Name of the file/folder
//
// @return	: HTREEITEM, If item exists, return handle to the tree item. 
//
//----------------------------------------------------------------------------------
HTREEITEM CPixTreeCtrl::FindSibling(HTREEITEM hParent, const CString& sItem)
{
	HTREEITEM hChild = GetChildItem(hParent);
	while (hChild)
	{
		CString sFound = GetItemText(hChild);
		if (sFound.CompareNoCase(sItem) == 0)
			return hChild;
		hChild = GetNextItem(hChild, TVGN_NEXT);
	}
	return NULL;
}

//----------------------------------------------------------------------------------
//  Function GetSelectedPath
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Returns the file/folder name currently selected in the tree control. 
//
// @return	: CString, Folder-/file path as selected in the tree control.
//
//----------------------------------------------------------------------------------
CString CPixTreeCtrl::GetSelectedPath()
{
	return ItemToPath(GetSelectedItem());
}

//----------------------------------------------------------------------------------
//  Function SetSelectedPath
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Sets the currently selected item in the tree control. 
//
// @param	: const CString& sPath, Folder/file name path to select in the tree control. 
// @param	: BOOL bExpanded, TRUE if the new item should be shown expanded otherwise FALSE.
//
// @return	: HTREEITEM, Folder/file path as represented by hItem.
//
//----------------------------------------------------------------------------------
HTREEITEM CPixTreeCtrl::SetSelectedPath(const CString& sPath, BOOL bExpanded)
{
	CString sSearch(sPath);
	int nSearchLength = sSearch.GetLength();
	if (nSearchLength == 0)
	{
		TRACE(_T("Cannot select a empty path\n"));
		return NULL;
	}

	//Remove trailing "\" from the path
	if (nSearchLength > 3 && sSearch.GetAt(nSearchLength-1) == _T('\\'))
		sSearch = sSearch.Left(nSearchLength-1);
	
	//Remove initial part of path if the root folder is setup
	int nRootLength = m_sRootFolder.GetLength();
		if (nRootLength)
		{
			if (sSearch.Find(m_sRootFolder) != 0)
			{
				TRACE(_T("Could not select the path %s as the root has been configued as %s\n"), sPath, m_sRootFolder);
				return NULL;
			}
			sSearch = sSearch.Right(sSearch.GetLength() - 1 - nRootLength);
		}

	if (sSearch.IsEmpty())
	return NULL;


	SetRedraw(FALSE);

	HTREEITEM hItemFound = TVI_ROOT;
	int nFound = sSearch.Find(_T('\\'));
BOOL bDriveMatch = m_sRootFolder.IsEmpty();
	while (nFound != -1)
	{
		CString sMatch;
		if (bDriveMatch)
		{
			sMatch = sSearch.Left(nFound + 1);
			bDriveMatch = FALSE;
		}
		else
			sMatch = sSearch.Left(nFound);

		hItemFound = FindSibling(hItemFound, sMatch);
		if (hItemFound == NULL)
			break;
		else
			Expand(hItemFound, TVE_EXPAND);

		sSearch = sSearch.Right(sSearch.GetLength() - nFound - 1);
		nFound = sSearch.Find(_T('\\'));
	};

	//The last item 
	if (hItemFound)
	{
		if (sSearch.GetLength())
			hItemFound = FindSibling(hItemFound, sSearch);
			SelectItem(hItemFound);

		if (bExpanded)
			Expand(hItemFound, TVE_EXPAND);
	}

	SetRedraw(TRUE);

	return hItemFound;
}

//----------------------------------------------------------------------------------
//  Function SetRootFolder
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Sets the tree root to a specific folder
//
// @param	: const CString& sPath, Name of the root folder
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::SetRootFolder(const CString& sPath)
{
	int nLength = sPath.GetLength();
	if (sPath.GetLength() > 0)
			m_sRootFolder = sPath;
	
	if (IsWindow(GetSafeHwnd()))
		OnViewRefresh();
}

//----------------------------------------------------------------------------------
//  Function FileRename
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Renaming of a file/folder.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::FileRename() 
{
	HTREEITEM hItem = GetSelectedItem();

	if (!IsDrive(hItem))
		EditLabel(hItem);
}

//----------------------------------------------------------------------------------
//  Function FileProperties
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Show Windows properties dialog of the selected item.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::FileProperties() 
{
	HTREEITEM hItem = GetSelectedItem();
	if (hItem)
	{
		//Show the "properties" for the selected file
		CString sFile = ItemToPath(hItem);
		SHELLEXECUTEINFO sei;
		ZeroMemory(&sei,sizeof(sei));
		sei.cbSize = sizeof(sei);
		sei.hwnd = AfxGetMainWnd()->GetSafeHwnd();
		sei.nShow = SW_SHOW;
		sei.lpFile = sFile.GetBuffer(sFile.GetLength());
		sei.lpVerb = _T("properties");
		sei.fMask  = SEE_MASK_INVOKEIDLIST;
		ShellExecuteEx(&sei);
		sFile.ReleaseBuffer();
	}
}

//----------------------------------------------------------------------------------
//  Function FileDelete
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Delete the selected item. Uses the standard Windows dianlog to confirm.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::FileDelete() 
{
	HTREEITEM hItem = GetSelectedItem();
	if (!IsDrive(hItem))
	{
		//Create a Multi SZ string with the filename to delete
		CString sFileToDelete = ItemToPath(hItem);
		int nChars = sFileToDelete.GetLength() + 1;
		nChars++;
		SHFILEOPSTRUCT shfo;
		ZeroMemory(&shfo, sizeof(SHFILEOPSTRUCT));
		shfo.hwnd = AfxGetMainWnd()->GetSafeHwnd();
		shfo.wFunc = FO_DELETE;

		//Undo is not allowed if the SHIFT key is held down
		if (!(GetKeyState(VK_SHIFT) & 0x8000))
		shfo.fFlags = FOF_ALLOWUNDO;

		TCHAR* pszFrom = new TCHAR[nChars];
		TCHAR* pszCur = pszFrom;
		_tcscpy(pszCur, sFileToDelete);
		pszCur[nChars-1] = _T('\0');
		shfo.pFrom = pszFrom;

		//Let the shell perform the actual deletion
		if (SHFileOperation(&shfo) == 0 && shfo.fAnyOperationsAborted == FALSE)
		{
			//Delete the item from the view
			DeleteItem(hItem);
		}

		//Free up the memory we had allocated
		delete [] pszFrom;
	}
}

//----------------------------------------------------------------------------------
//  Function OnContextMenu
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Display the context menue (popup) an given coordinates
//
// @param	: CPoint point, Specifies the coordinates of the cursor.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnContextMenu(CWnd* /*nix*/, CPoint point)
{
	CMenu menu;
    VERIFY(menu.LoadMenu(IDR_MENU1));
	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y,	this);
}

//----------------------------------------------------------------------------------
//  Function InsertFileItem
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Inserts a new tree item (drive/folder/file) into the tree.
//
// @param	: const CString& sFile, Name of the file to be inserted.
// @param	: const CString& sPath, Name of the path of the file to be inserted.
// @param	: HTREEITEM hParent, Handle to the item's parent.
//
// @return	: HTREEITEM, Handle to the inserted item.
//
//----------------------------------------------------------------------------------
HTREEITEM CPixTreeCtrl::InsertFileItem(const CString& sFile, const CString& sPath, HTREEITEM hParent)
{
	//Retreive the icon indexes for the specified file/folder
	int nIconIndex = GetIconIndex(sPath);
	int nSelIconIndex = GetSelIconIndex(sPath);
	if (nIconIndex == -1 || nSelIconIndex == -1)
	{
		TRACE(_T("Failed in call to SHGetFileInfo for %s, GetLastError:%d\n"), sPath, ::GetLastError());
		return NULL;
	}

	//Add the actual item
	CString sTemp(sFile);
	TV_INSERTSTRUCT tvis;
	ZeroMemory(&tvis, sizeof(TV_INSERTSTRUCT));
		tvis.hParent = hParent;
		tvis.hInsertAfter = TVI_LAST;
		tvis.item.mask = TVIF_CHILDREN | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
		tvis.item.pszText = sTemp.GetBuffer(sTemp.GetLength());
		tvis.item.iImage = nIconIndex;
		tvis.item.iSelectedImage = nSelIconIndex;
		tvis.item.cChildren = HasGotSubEntries(sPath);
	HTREEITEM hItem = InsertItem(&tvis);
	sTemp.ReleaseBuffer();

	return hItem;
}

//----------------------------------------------------------------------------------
//  Function OnViewRefresh
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Sets the redraw flag to TRUE, so that the tree is redrawn
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnViewRefresh() 
{
	SetRedraw(FALSE);

	//Get the item which is currently selected
	HTREEITEM hSelItem = GetSelectedItem();
	CString sItem  = ItemToPath(hSelItem);
	BOOL bExpanded = (GetChildItem(hSelItem) != NULL); 

	//DisplayDrives(TVI_ROOT, FALSE);
	//Display the folder items in the tree
	if (m_sRootFolder.IsEmpty())
		DisplayDrives(TVI_ROOT, FALSE);
	else
		DisplayPath(m_sRootFolder, TVI_ROOT, FALSE);	

	//Reselect the initially selected item
	if (sItem.GetLength())
		hSelItem = SetSelectedPath(sItem, bExpanded);

	SetRedraw(TRUE);
}


//----------------------------------------------------------------------------------
//  Function OnEndlabeledit
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Renaming of a file/folder.
//
// @param	: NMHDR* pNMHDR, Contains information about a notification message. 
// @param	: LRESULT* pResult, Contains information about a notification message.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_DISPINFO* pDispInfo = (TV_DISPINFO*)pNMHDR;
	if (pDispInfo->item.pszText)
	{
		SHFILEOPSTRUCT shfo;
		ZeroMemory(&shfo, sizeof(SHFILEOPSTRUCT));
		shfo.hwnd = AfxGetMainWnd()->GetSafeHwnd();
		shfo.wFunc = FO_RENAME;
		shfo.fFlags = FOF_ALLOWUNDO;

		CString sFrom = ItemToPath(pDispInfo->item.hItem);
		int nFromLength = sFrom.GetLength();
		TCHAR* pszFrom = new TCHAR[nFromLength + 2];
		_tcscpy(pszFrom, sFrom);
		pszFrom[nFromLength+1] = _T('\0');
		shfo.pFrom = pszFrom;
		HTREEITEM hParent = GetParentItem(pDispInfo->item.hItem);
		CString sParent = ItemToPath(hParent);
		CString sTo;
		if (IsDrive(sParent))
			sTo = sParent + pDispInfo->item.pszText;
		else
			sTo = sParent + _T("\\") + pDispInfo->item.pszText;

		int nToLength = int(_tcslen(sTo));
		TCHAR* pszTo = new TCHAR[nToLength + 2];
		_tcscpy(pszTo, sTo);
		pszTo[nToLength+1] = _T('\0');
		shfo.pTo = pszTo;

		//Let the shell perform the actual rename
		if (SHFileOperation(&shfo) == 0 && shfo.fAnyOperationsAborted == FALSE)
		{
			*pResult = TRUE;

			//Update its text  
			SetItemText(pDispInfo->item.hItem, pDispInfo->item.pszText);

			//Also update the icons for it
			CString sPath = ItemToPath(pDispInfo->item.hItem);
			SetItemImage(pDispInfo->item.hItem, GetIconIndex(sPath), GetSelIconIndex(sPath));
		}

		//Don't forget to free up the memory we allocated
		delete [] pszFrom;
		delete [] pszTo;
	}
		
	*pResult = 0;
}

//----------------------------------------------------------------------------------
//  Function PreTranslateMessage
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Filtering out messages before the default Windows messages are called.
//
// @param	: MSG* pMsg, Pointer to a MSG structure that contains the message to process. 
//
// @return	: BOOL, TRUE if meassage was interpreted correct.
//
//----------------------------------------------------------------------------------
BOOL CPixTreeCtrl::PreTranslateMessage(MSG* pMsg) 
{
	// When an item is being edited make sure the edit control
	// receives certain important key strokes
	if (GetEditControl())
	{
		::TranslateMessage(pMsg);
		::DispatchMessage(pMsg);
		return TRUE; // DO NOT process further
	}

	//Hitting the Escape key, Cancelling drag & drop
		else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE && (m_pilDrag != NULL))
	{
		EndDragging(TRUE);
		return TRUE;
	}
	//Hitting the Alt-Enter key combination, show the properties sheet 
		else if (pMsg->message == WM_SYSKEYDOWN && pMsg->wParam == VK_RETURN)
	{
		FileProperties();
		return TRUE;
	} 
	//Hitting the delete key, delete the item
		else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_DELETE)
	{
		FileDelete();
		return TRUE;
	}
	//hitting the backspace key, go to the parent folder
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_BACK)
	{
		UpOneLevel();
		return TRUE;
	}

	//hitting the F2 key, being in-place editing of an item
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F2)
	{
		FileRename();
		return TRUE;
	}
	//hitting the F5 key, force a refresh of the whole tree
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F5)
	{
		OnViewRefresh();
		return TRUE;
	}

	//Let the parent class do its thing
		return CTreeCtrl::PreTranslateMessage(pMsg);
}

//----------------------------------------------------------------------------------
//  Function UpOneLevel
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Selects the parent directory as actual directory
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::UpOneLevel()
{
	HTREEITEM hItem = GetSelectedItem();
	if (hItem)
	{
		HTREEITEM hParent = GetParentItem(hItem);
		if (hParent)
		Select(hParent, TVGN_CARET);
	}
}

//----------------------------------------------------------------------------------
//  Function IsFile
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Determines if an item is a file.
//
// @param	: HTREEITEM hItem, The tree item to be determined if it is a file. 
//
// @return	: BOOL, TRUE if the tree control item is a file. 
//
//----------------------------------------------------------------------------------
BOOL CPixTreeCtrl::IsFile(HTREEITEM hItem)
{
	return ((GetFileAttributes(ItemToPath(hItem)) & FILE_ATTRIBUTE_DIRECTORY) == 0);
}

//----------------------------------------------------------------------------------
//  Function IsFolder
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Determines if an item is a folder/directory. 
//
// @param	: HTREEITEM hItem, The tree item to be determined if it is a folder. 
//
// @return	: BOOL, TRUE if the tree control item or path is a folder.
//
//----------------------------------------------------------------------------------
BOOL CPixTreeCtrl::IsFolder(HTREEITEM hItem)
{
	return ((GetFileAttributes((ItemToPath(hItem))) & FILE_ATTRIBUTE_DIRECTORY) != 0);
}

//----------------------------------------------------------------------------------
//  Function IsDrive
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Determine if an item is a drive letter e.g. "C:\". 
//
// @param	: HTREEITEM hItem, The tree item to be determined if it is a drive letter. 
//
// @return	: BOOL, TRUE if the tree control item or path is a drive.
//
//----------------------------------------------------------------------------------
BOOL CPixTreeCtrl::IsDrive(HTREEITEM hItem)
{
	return IsDrive(ItemToPath(hItem));
}

//----------------------------------------------------------------------------------
//  Function IsDrive
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Determine if an item is a drive letter e.g. "C:\". 
//
// @param	: const CString& sPath, The path of the object to be determined 
//			  if it is a drive letter.
//
// @return	: BOOL, TRUE if the tree control item or path is a drive.
//
//----------------------------------------------------------------------------------
BOOL CPixTreeCtrl::IsDrive(const CString& sPath)
{
	return (sPath.GetLength() == 3 && sPath.GetAt(1) == _T(':') && sPath.GetAt(2) == _T('\\'));
}

//----------------------------------------------------------------------------------
//  Function HasGotSubEntries
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Searches through folder for subdirectories. 
//
// @param	: const CString& sDirectory, Name of the folder to search for subentries.
//
// @return	: BOOL, TRUE if a subdirectory has been found.
//
//----------------------------------------------------------------------------------
BOOL CPixTreeCtrl::HasGotSubEntries(const CString& sDirectory)
{
	ASSERT(sDirectory.GetLength());
	CFileFind find;
	CString sFile;
	if (sDirectory.GetAt(sDirectory.GetLength()-1) == _T('\\'))
		sFile = sDirectory + _T("*.*");
	else
		sFile = sDirectory + _T("\\*.*");
	BOOL bFind = find.FindFile(sFile);  
	while (bFind)
	{
		bFind = find.FindNextFile();
		if (find.IsDirectory() && !find.IsDots())
			return TRUE;
		else if (!find.IsDirectory() && !find.IsHidden() && m_bShowFiles)
			return TRUE;
	}

	return FALSE;
}

//----------------------------------------------------------------------------------
//  Function PreSubclassWindow
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Dynamic subclassing of window controls.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::PreSubclassWindow() 
{
	//Let the parent class do its thing	
		CTreeCtrl::PreSubclassWindow();

	//Post a message to populate the tree view with the drive letters
	PostMessage(WM_POPULATE_TREE);
}

//----------------------------------------------------------------------------------
//  Function OnPopulateTree
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Attach the image list to the tree control.
//
// @return	: LRESULT
//
//----------------------------------------------------------------------------------
LRESULT CPixTreeCtrl::OnPopulateTree(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	//Using normal Windows images
	SetImageList(&m_SysImageList.GetImageList(), TVSIL_NORMAL);

	//Force a refresh
	OnViewRefresh();

	return 0L;
}

//----------------------------------------------------------------------------------
//  Function DisplayDrives
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Inserts drives into the tree. 
//
// @param	: HTREEITEM hParent, Handle to parent (root) of the tree.
// @param	: BOOL bUseSetRedraw, Decide if redrawing of tree is necessary.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::DisplayDrives(HTREEITEM hParent, BOOL bUseSetRedraw)
{
	CWaitCursor c;

	//Speed up the job by turning off redraw
	if (bUseSetRedraw)
		SetRedraw(FALSE);

	//Remove any items currently in the tree
	DeleteAllItems();

	//Enumerate the drive letters and add them to the tree control
	DWORD dwDrives = GetLogicalDrives();
	DWORD dwMask = 1;
	for (int i=0; i<32; i++)
	{
		if (dwDrives & dwMask)
		{
			CString sDrive;
			sDrive.Format(_T("%c:\\"), i + _T('A'));
			InsertFileItem(sDrive, sDrive, hParent);
		}
		dwMask <<= 1;
	}

	if (bUseSetRedraw)
		SetRedraw(TRUE);
}

//----------------------------------------------------------------------------------
//  Function DisplayPath
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Insert directories/files to tree. 
//
// @param	: const CString& sPath, Name of the folder that should be added to the tree.
// @param	: HTREEITEM hParent, Handle to parent (root) of the tree.
// @param	: BOOL bUseSetRedraw, Decide if redrawing of tree is necessary.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::DisplayPath(const CString& sPath, HTREEITEM hParent, BOOL bUseSetRedraw)
{
	CWaitCursor c;

	//Speed up the job by turning off redraw
	if (bUseSetRedraw)
		SetRedraw(FALSE);

	//Remove all the items currently under hParent
	HTREEITEM hChild = GetChildItem(hParent);
	while (hChild)
	{
		DeleteItem(hChild);
		hChild = GetChildItem(hParent);
	}

	//Find all the directories and files underneath sPath
	CSortedArray<CString, CString&> DirectoryPaths;
	CSortedArray<CString, CString&> FilePaths;
	CFileFind find;
	CString sFile;
	if (sPath.GetAt(sPath.GetLength()-1) == _T('\\'))
		sFile = sPath + _T("*.*");
	else
		sFile = sPath + _T("\\*.*");
	  
	BOOL bFind = find.FindFile(sFile);  
	while (bFind)
	{
		bFind = find.FindNextFile();
		if (find.IsDirectory())
		{
			if (!find.IsDots())
			{
				CString sPath = find.GetFilePath();
				DirectoryPaths.Add(sPath);
			}
		}
		else 
		{
			if (!find.IsHidden() && m_bShowFiles)
			{
				CString sPath = find.GetFilePath();
				FilePaths.Add(sPath);
			}  
		}
	}

	//Now sort the 2 arrays prior to added to the tree control
	DirectoryPaths.SetCompareFunction(CompareByFilenameNoCase);
	FilePaths.SetCompareFunction(CompareByFilenameNoCase);
	DirectoryPaths.Sort();
	FilePaths.Sort();

	//Now add all the directories to the tree control
	for (int i=0; i<DirectoryPaths.GetSize(); i++)
	{
		CString& sPath = DirectoryPaths.ElementAt(i);
		TCHAR path_buffer[_MAX_PATH];
		TCHAR fname[_MAX_FNAME];
		TCHAR ext[_MAX_EXT];
		_tsplitpath(sPath, NULL, NULL, fname, ext);
		_tmakepath(path_buffer, NULL, NULL, fname, ext);
		InsertFileItem(path_buffer, sPath, hParent);
	}

	//And the files to the tree control (if required)
	for (i=0; i<FilePaths.GetSize(); i++)
	{
		CString& sPath = FilePaths.ElementAt(i);
		TCHAR path_buffer[_MAX_PATH];
		TCHAR fname[_MAX_FNAME];
		TCHAR ext[_MAX_EXT];
		_tsplitpath(sPath, NULL, NULL, fname, ext);
		_tmakepath(path_buffer, NULL, NULL, fname, ext);
		InsertFileItem(path_buffer, sPath, hParent);
	}

	//Turn back on the redraw flag
	if (bUseSetRedraw)
		SetRedraw(TRUE);
}

//----------------------------------------------------------------------------------
//  Function OnItemexpanding
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Handels expantion and collaps off tree nodes. 
//
// @param	: NMHDR* pNMHDR, Contains information about a notification message.
// @param	: LRESULT* pResult, Contains information about a notification message.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	if (pNMTreeView->action == TVE_EXPAND)
	{
		//Add the new items to the tree if it does not have any child items
		//already
		if (!GetChildItem(pNMTreeView->itemNew.hItem))
		{
			CString sPath = ItemToPath(pNMTreeView->itemNew.hItem);
			DisplayPath(sPath, pNMTreeView->itemNew.hItem);
		}
	}

	*pResult = 0;
}

//----------------------------------------------------------------------------------
//  Function ItemToPath
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Retrieves the file-/folder path of a given HTREEITEM.
//
// @param	: HTREEITEM hItem, The tree item to get the path for.
//
// @return	: CString, Folder-/file path. 
//
//----------------------------------------------------------------------------------
CString CPixTreeCtrl::ItemToPath(HTREEITEM hItem)
{
	CString sPath;
	  
	//Create the full string of the tree item
	HTREEITEM hParent = hItem;
	while (hParent)
	{
		CString sItem = GetItemText(hParent);
		int nLength = sItem.GetLength();
		ASSERT(nLength);
		hParent = GetParentItem(hParent);

		if (sItem.GetAt(nLength-1) == _T('\\'))
			sPath = sItem + sPath;
		else
		{
			if (sPath.GetLength())
				sPath = sItem + _T('\\') + sPath;
			else
				sPath = sItem;
		}
	}

//Add the root folder if there is one
if (m_sRootFolder.GetLength())
sPath = m_sRootFolder + _T('\\') + sPath;

	return sPath;
}

//----------------------------------------------------------------------------------
//  Function OnRclick
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Handles the right click of the mouse. And calls the context menu.
//
// @param	: LRESULT* pResult, Contains information about the notification message. 
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnRclick(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	//Work out the position of where the context menu should be
	CPoint p(GetCurrentMessage()->pt);
	CPoint pt(p);
	ScreenToClient(&pt);
	Select(HitTest(pt), TVGN_CARET);
	OnContextMenu(NULL, p);
	*pResult = 0;
}

//----------------------------------------------------------------------------------
//  Function OnRclick
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Grabs the selected item in the tree.
//
// @param	: LRESULT* pResult, Contains information about the notification message. 
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnItemselected(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
//	::SendMessage(this->m_parentwnd, WM_USER, 0, 0);
	// m_selected = CString(this->GetItemText(pNMTreeView->itemNew.hItem));
	//((CPixTreeView *)this->m_parentwnd)->LoadSelected(m_selected.GetBuffer());
	//::MessageBox(0, CString(this->GetItemText(pNMTreeView->itemNew.hItem).GetBuffer()),0,0);
	//pNMTreeView->itemNew.pszText
	CString path = CString("");
	HTREEITEM item = pNMTreeView->itemNew.hItem;
	do
	{
		path += this->GetItemText(item);
		item = this->GetParentItem(item);
		if (item != NULL)
		{
			path += CString("\\");
		}
	//	CString(this->GetItemText(pNMTreeView->itemNew.pszText).GetBuffer())
	}while(item != NULL);
	//::MessageBox(0, CString(this->GetItemText(pNMTreeView->itemNew.pszText).GetBuffer()),0,0);


	if (path.GetLength() <= 3)
	{
		CString inter = CString("\\");
		inter += path;
		path = inter;
	}

	CString npath = CString("");
	char cword[256];
	char cpath[512];
	int i;
	lstrcpy(cpath,path.GetBuffer());
	for(i=lstrlen(cpath) - 1; i>=0; i--)
	{
		while (((cpath[i] != '\\') || (i==lstrlen(cpath) - 1)) && i >= 0)
		{
			//cword[lstrlen(cpath) - i] = cpath[i];
			i--;
		}
		lstrcpy(cword, cpath + i + 1);
		if (i >= 0) cpath[i]= '\0';
		if (npath.GetLength() > 3) npath += CString("\\");
		npath += CString(cword);
	}
	//::MessageBox(0, npath.GetBuffer(), 0, 0);
	m_selected = npath;

	DWORD dwResult = NULL;
	SendMessageTimeout(this->m_parentwnd->m_hWnd, UWM_ON_TREE_LCLICKED, (WPARAM)this, (LPARAM)0, SMTO_ABORTIFHUNG|SMTO_NORMAL, 2000, &dwResult );

	*pResult = 0;
}

//----------------------------------------------------------------------------------
//  Function OnLclick
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Handles the left click of the mouse.
//
// @param	: LRESULT* pResult, Contains information about the notification message. 
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnLclick(NMHDR* pNMHDR , LRESULT* pResult) 
{
	/*
	HTREEITEM hItem = GetSelectedItem();
	CPoint pt = GetCurrentMessage()->pt;
	ScreenToClient(&pt);

//	CPixGrabberDoc* pDoc = GetDocument();

	//XXTree.TreeDataChange(pNMHDR, pResult);

	if (hItem && (hItem == HitTest(pt)))
	{
		TV_ITEM tvi;
		ZeroMemory(&tvi, sizeof(TVITEM));
		tvi.mask = TVIF_CHILDREN;
		tvi.hItem = hItem;
		CString S;
		S = GetSelectedPath();
		AfxMessageBox(S);

	}*/
	*pResult = 0;
}
/*
void CPixTreeView::OnTvnSelchangingTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	CPixGrabberDoc* pDoc = GetDocument();
	if (pDoc)
	{
		//::MessageBox(0, zetree.GetItemText(pNMTreeView->itemNew.hItem), zetree.GetItemText(zetree.GetParentItem(pNMTreeView->itemNew.hItem)), 0);
		if (!lstrcmp(zetree.GetItemText(zetree.GetParentItem(pNMTreeView->itemNew.hItem)), "Grabbed Websites"))
		{
			pDoc->SetProject(CString(zetree.GetItemText(pNMTreeView->itemNew.hItem)));
		}
		pDoc->UpdateAllViews(this);
	}
	*pResult = 0;
}*/

//----------------------------------------------------------------------------------
//  Function OnInitMenuPopup
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : The framework calls this member function when a pop-up menu is 
//			  about to become active.
//
// @param	: CMenu* pMenu, Specifies the menu object of the pop-up menu.
// @param	: BOOL bSysMenu, TRUE if the pop-up menu is the Control menu. 
//
//Copied from CFrameWnd::OnInitMenuPopup to provide OnUpdateCmdUI functionality
//in the tree control
//
//----------------------------------------------------------------------------------

void CPixTreeCtrl::OnInitMenuPopup(CMenu* pMenu, UINT /*nIndex*/, BOOL bSysMenu) 
{
	//AfxCancelModes(m_hWnd);

	if (bSysMenu)
		return;     // don't support system menu

	ASSERT(pMenu != NULL);
	// check the enabled state of various menu items

	CCmdUI state;
	state.m_pMenu = pMenu;
	ASSERT(state.m_pOther == NULL);
	ASSERT(state.m_pParentMenu == NULL);

	// determine if menu is popup in top-level menu and set m_pOther to
	//  it if so (m_pParentMenu == NULL indicates that it is secondary popup)
	HMENU hParentMenu;
	if (AfxGetThreadState()->m_hTrackingMenu == pMenu->m_hMenu)
		state.m_pParentMenu = pMenu;    // parent == child for tracking popup
	else if ((hParentMenu = ::GetMenu(m_hWnd)) != NULL)
	{
		CWnd* pParent = GetTopLevelParent();
			// child windows don't have menus -- need to go to the top!
		if (pParent != NULL &&
			(hParentMenu = ::GetMenu(pParent->m_hWnd)) != NULL)
		{
			int nIndexMax = ::GetMenuItemCount(hParentMenu);
			for (int nIndex = 0; nIndex < nIndexMax; nIndex++)
			{
				if (::GetSubMenu(hParentMenu, nIndex) == pMenu->m_hMenu)
				{
					// when popup is found, m_pParentMenu is containing menu
					state.m_pParentMenu = CMenu::FromHandle(hParentMenu);
					break;
				}
			}
		}
	}

	state.m_nIndexMax = pMenu->GetMenuItemCount();
	for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax;
	  state.m_nIndex++)
	{
		state.m_nID = pMenu->GetMenuItemID(state.m_nIndex);
		if (state.m_nID == 0)
			continue; // menu separator or invalid cmd - ignore it

		ASSERT(state.m_pOther == NULL);
		ASSERT(state.m_pMenu != NULL);
		if (state.m_nID == (UINT)-1)
		{
			// possibly a popup menu, route to first item of that popup
			state.m_pSubMenu = pMenu->GetSubMenu(state.m_nIndex);
			if (state.m_pSubMenu == NULL ||
				(state.m_nID = state.m_pSubMenu->GetMenuItemID(0)) == 0 ||
				state.m_nID == (UINT)-1)
			{
				continue;       // first item of popup can't be routed to
			}
			state.DoUpdate(this, FALSE);    // popups are never auto disabled
		}
		else
		{
			// normal menu item
			// Auto enable/disable if frame window has 'm_bAutoMenuEnable'
			//    set and command is _not_ a system command.
			state.m_pSubMenu = NULL;
			//state.DoUpdate(this, m_bAutoMenuEnable && state.m_nID < 0xF000);
			state.DoUpdate(this, TRUE && state.m_nID < 0xF000);
		}

		// adjust for menu deletions and additions
		UINT nCount = pMenu->GetMenuItemCount();
		if (nCount < state.m_nIndexMax)
		{
			state.m_nIndex -= (state.m_nIndexMax - nCount);
			while (state.m_nIndex < nCount &&
				pMenu->GetMenuItemID(state.m_nIndex) == state.m_nID)
			{
				state.m_nIndex++;
			}
		}
		state.m_nIndexMax = nCount;
	}
}

//----------------------------------------------------------------------------------
//  Function OnBegindrag
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Used to initiate Drag & Drop. Gets information which item will be 
//			  dragged.
//
// @param	: NMHDR* pNMHDR, Contains information about a notification message.
// @param	: LRESULT* pResult, Contains information about the notification message. 
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	*pResult = 0;

	SelectItem(pNMTreeView->itemNew.hItem);
	if (!IsDropSource(pNMTreeView->itemNew.hItem)) //!IsDrive(hItem)
		return;

	m_pilDrag = CreateDragImage(pNMTreeView->itemNew.hItem);
	if (!m_pilDrag)
		return;

	m_hItemDrag = pNMTreeView->itemNew.hItem;
	m_hItemDrop = NULL;

	// Calculate the offset to the hotspot
	CPoint offsetPt(8,8);   // Initialize a default offset

	CPoint dragPt = pNMTreeView->ptDrag;    // Get the Drag point
	UINT nHitFlags = 0;
	HTREEITEM htiHit = HitTest(dragPt, &nHitFlags);
	if (htiHit != NULL)
	{
		// The drag point has Hit an item in the tree
		CRect itemRect;
		if (GetItemRect(htiHit, &itemRect, FALSE))
		{
			// Count indent levels
			HTREEITEM htiParent = htiHit;
			int nIndentCnt = 0;
			while (htiParent != NULL)
			{
				htiParent = GetParentItem(htiParent);
				nIndentCnt++;
			}

			if (!(GetStyle() & TVS_LINESATROOT)) 
				nIndentCnt--;

			// Calculate the new offset
			offsetPt.y = dragPt.y - itemRect.top;
			offsetPt.x = dragPt.x - (nIndentCnt * GetIndent()) + GetScrollPos(SB_HORZ);
		}
	}

	//Begin the dragging  
	m_pilDrag->BeginDrag(0, offsetPt);
	POINT pt = pNMTreeView->ptDrag;
	ClientToScreen(&pt);
	m_pilDrag->DragEnter(NULL, pt);
	SetCapture();

	m_nTimerID = UINT(SetTimer(1, 300, NULL));
}

//----------------------------------------------------------------------------------
//  Function OnMouseMove
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Used during Drag & Drop. Selects the drop item if cursor is in the 
//			  client area.
//
// @param	: UINT nFlags, Indicates whether various virtual keys are down. 
// @param	: CPoint point, Specifies the coordinates of the cursor.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (m_pilDrag != NULL)
	{
		CRect clientRect;
		GetClientRect(&clientRect);

		POINT pt = point;
		ClientToScreen(&pt);
		CImageList::DragMove(pt);

		HTREEITEM hItem = NULL;
		if (clientRect.PtInRect(point))
		{
			UINT flags;
			hItem = HitTest(point, &flags);
			if (m_hItemDrop != hItem)
			{
				CImageList::DragShowNolock(FALSE);
				SelectDropTarget(hItem);
				m_hItemDrop = hItem;
				CImageList::DragShowNolock(TRUE);
			}
		}
    }

	//Let the parent class do its thing	
	CTreeCtrl::OnMouseMove(nFlags, point);
}

//----------------------------------------------------------------------------------
//  Function GetDropTarget
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Use during Drag & Drop. Indetifies drop destination.
//
// @param	: HTREEITEM hItem, 
//
// @return	: HTREEITEM, returns drop destination
//
//----------------------------------------------------------------------------------
HTREEITEM CPixTreeCtrl::GetDropTarget(HTREEITEM hItem)
{
	if (!IsFile(hItem) && hItem != m_hItemDrag && hItem != GetParentItem(m_hItemDrag))
	{
		HTREEITEM htiParent = hItem;
		while ((htiParent = GetParentItem(htiParent)) != NULL)
		{
		if (htiParent == m_hItemDrag)
			return NULL;
		}
		return hItem;
	}
	return NULL;
}

//----------------------------------------------------------------------------------
//  Function IsDropSource
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Used during Drag & Drop. Indentifies target drive.
//
// @param	: HTREEITEM hItem
//
// @return	: BOOL, 
//
//----------------------------------------------------------------------------------
BOOL CPixTreeCtrl::IsDropSource(HTREEITEM hItem)
{
	return !IsDrive(hItem);
}

//----------------------------------------------------------------------------------
//  Function OnLButtonUp
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : On left mouse button up. Calls EndDreagging(FALSE) if cursor is on 
//			  an valid folder.
//
// @param	: UINT nFlags, Indicates whether various virtual keys are down. 
// @param	: CPoint point, Specifies the coordinates of the cursor.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CRect clientRect;
	GetClientRect(&clientRect);

	if (clientRect.PtInRect(point))
		EndDragging(FALSE);
	else
		EndDragging(TRUE);
		 
	//Let the parent class do its thing	
	CTreeCtrl::OnLButtonUp(nFlags, point);
}

//----------------------------------------------------------------------------------
//  Function EndDragging
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Used during Drag & Drop. End the dragging event and calls the 
//			  appropriate functions depending if it's Move ore Copy.
//
// @param	: BOOL bCancel, indicates whether the darging operation was aborded.
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::EndDragging(BOOL bCancel)
{
	if (m_pilDrag != NULL)
	{
		KillTimer(m_nTimerID);

		CImageList::DragLeave(this);
		CImageList::EndDrag();
		ReleaseCapture();

		delete m_pilDrag;
		m_pilDrag = NULL;

		//Remove drop target highlighting
		SelectDropTarget(NULL);

		m_hItemDrop = GetDropTarget(m_hItemDrop);
		if (m_hItemDrop == NULL)
			return;

		if (!bCancel)
		{
			//Also need to make the change on disk
			CString sFromPath = ItemToPath(m_hItemDrag);
			CString sToPath = ItemToPath(m_hItemDrop);

			int nFromLength = sFromPath.GetLength();
			int nToLength = sToPath.GetLength();
			SHFILEOPSTRUCT shfo;
			ZeroMemory(&shfo, sizeof(SHFILEOPSTRUCT));
			shfo.hwnd = GetSafeHwnd();

			if ((GetKeyState(VK_CONTROL) & 0x8000))
				shfo.wFunc = FO_COPY;
			else
				shfo.wFunc = FO_MOVE;

			shfo.fFlags = FOF_SILENT | FOF_NOCONFIRMMKDIR;
			//Undo is not allowed if the SHIFT key is held down
			if (!(GetKeyState(VK_SHIFT) & 0x8000))
				shfo.fFlags |= FOF_ALLOWUNDO;

			TCHAR* pszFrom = new TCHAR[nFromLength + 2];
			_tcscpy(pszFrom, sFromPath);
			pszFrom[nFromLength+1] = _T('\0');
			shfo.pFrom = pszFrom;

			TCHAR* pszTo = new TCHAR[nToLength + 2];
			_tcscpy(pszTo, sToPath);
			pszTo[nToLength+1] = _T('\0');
			shfo.pTo = pszTo;

			//Let the shell perform the actual deletion
			BOOL bSuccess = ((SHFileOperation(&shfo) == 0) && (shfo.fAnyOperationsAborted == FALSE));

			//Free up the memory we had allocated
			delete [] pszFrom;
			delete [] pszTo;

			if (bSuccess)
			{
				//Only copy the item in the tree if there is not an item with the same
				//text under m_hItemDrop
				CString sText = GetItemText(m_hItemDrag);
				if (!HasChildWithText(m_hItemDrop, sText))
				{
					//Do the actual copy
					BOOL bHadChildren = (GetChildItem(m_hItemDrop) != NULL);
					CopyBranch(m_hItemDrag, m_hItemDrop);

					//Update the children indicator for the folder we just dropped into
					if (!bHadChildren)
					{
						TV_ITEM tvItem;
						tvItem.hItem = m_hItemDrop;
						tvItem.mask = TVIF_CHILDREN;  
						tvItem.cChildren = 1;
						SetItem(&tvItem);
					}
				}

				BOOL bExpanded = (GetChildItem(m_hItemDrop) != NULL); 
				if (shfo.wFunc == FO_MOVE)
				{
					//Get the parent of the item we moved prior to deleting it
					HTREEITEM hParent = GetParentItem(m_hItemDrag);

					//Delete the item we just moved
					DeleteItem(m_hItemDrag);

					//Update the children indicator for the item we just dragged from
					BOOL bHasChildren = (GetChildItem(hParent) != NULL);
					if (hParent && !bHasChildren)
					{
						TV_ITEM tvItem;
						tvItem.hItem = hParent;
						tvItem.mask = TVIF_CHILDREN;  
						tvItem.cChildren = 0;
						SetItem(&tvItem);
					}
				}
				SetSelectedPath(sToPath, bExpanded);
			}
		}
	}
}

//----------------------------------------------------------------------------------
//  Function HasChildWithText
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Search through folder if there exists an tree item whith the same name.
//
// @param	: HTREEITEM hParent, Handle to the item's parent.
// @param	: const CString& sText, Name of the tree item.
//
// @return	: BOOL FALSE if there was no item found.
//
//----------------------------------------------------------------------------------
BOOL CPixTreeCtrl::HasChildWithText(HTREEITEM hParent, const CString& sText)
{
	HTREEITEM hChild = GetChildItem(hParent);
	while (hChild)
	{
		CString sItemText = GetItemText(hChild);
		if (sItemText.CompareNoCase(sText) == 0)
		return TRUE;
		hChild = GetNextSiblingItem(hChild);
	}
	return FALSE;
}

//----------------------------------------------------------------------------------
//  Function CopyItem
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Copy of one tree item.
//
// @param	: HTREEITEM hItem, Handle of the item to copy.
// @param	: HTREEITEM htiNewParent, Handle of the inserted item's parent. 
// @param	: HTREEITEM htiAfter, Handle of the item after which the new item is to be inserted.
//
// @return	: HTREEITEM Handle to the inserted item.
//
//----------------------------------------------------------------------------------
HTREEITEM CPixTreeCtrl::CopyItem(HTREEITEM hItem, HTREEITEM htiNewParent, HTREEITEM htiAfter)
{
	//Get the details of the item to copy
	TV_INSERTSTRUCT tvstruct;
	tvstruct.item.hItem = hItem;
	tvstruct.item.mask = TVIF_CHILDREN | TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM | TVIF_STATE;
	GetItem(&tvstruct.item);
	CString sText = GetItemText(hItem);
	tvstruct.item.cchTextMax = sText.GetLength();
	tvstruct.item.pszText = sText.GetBuffer(tvstruct.item.cchTextMax);

	//Insert the item at the proper location
	tvstruct.hParent = htiNewParent;
	tvstruct.hInsertAfter = htiAfter;
	tvstruct.item.mask |= TVIF_TEXT;
	HTREEITEM hNewItem = InsertItem(&tvstruct);

	//Don't forget to release the CString buffer  
	sText.ReleaseBuffer();

	return hNewItem;
}

//----------------------------------------------------------------------------------
//  Function CopyBranch
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Recursive copy of single tree item.
//
// @param	: HTREEITEM hItem, Handle of the item to copy.
// @param	: HTREEITEM htiNewParent, Handle of the inserted item's parent. 
// @param	: HTREEITEM htiAfter, Handle of the item after which the new item is to be inserted.
//
// @return	: HTREEITEM Handle to the inserted item.
//
//----------------------------------------------------------------------------------
HTREEITEM CPixTreeCtrl::CopyBranch(HTREEITEM htiBranch, HTREEITEM htiNewParent, HTREEITEM htiAfter)
{
	HTREEITEM hNewItem = CopyItem(htiBranch, htiNewParent, htiAfter);
	HTREEITEM hChild = GetChildItem(htiBranch);
	while (hChild != NULL)
	{
		//recursively transfer all the items
		CopyBranch(hChild, hNewItem);
		hChild = GetNextSiblingItem(hChild);
	}
	return hNewItem;
}


//----------------------------------------------------------------------------------
//  Function OnTimer
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Used during Darg & Drop. Shows the dragging effect and carries out 
//			  scrolling (if cursor is out of client area) and expanding of 
//			  folders (if to many timer ticks on a folder). 
//
// @param	: UINT nIDEvent, Specifies the identifier of the timer. 
//
//----------------------------------------------------------------------------------
void CPixTreeCtrl::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent != m_nTimerID)
	{
		CTreeCtrl::OnTimer(nIDEvent);
		return;
	}

	//Show the dragging effect
	POINT pt;
	GetCursorPos(&pt);
	RECT rect;
	GetClientRect(&rect);
	ClientToScreen(&rect);
	CImageList::DragMove(pt);

	HTREEITEM hFirstItem = GetFirstVisibleItem();
	CRect ItemRect;
	GetItemRect(hFirstItem, &ItemRect, FALSE);
	if (pt.y < (rect.top + (ItemRect.Height()*2)) && pt.y > rect.top)
	{
		//we need to scroll up
		CImageList::DragShowNolock(FALSE);
		SendMessage(WM_VSCROLL, SB_LINEUP);
		EnsureVisible(hFirstItem);
		SelectDropTarget(hFirstItem);
		m_hItemDrop = hFirstItem;
		CImageList::DragShowNolock(TRUE);
	}
	else if (pt.y > (rect.bottom - (ItemRect.Height()*2)) && pt.y < rect.bottom)
	{
		//we need to scroll down
		CImageList::DragShowNolock(FALSE);
		SendMessage(WM_VSCROLL, SB_LINEDOWN);
		HTREEITEM hLastItem = hFirstItem;
		int nCount = GetVisibleCount();
		for (int i=0; i<(nCount-1); i++)
		hLastItem = GetNextVisibleItem(hLastItem);
		SelectDropTarget(hLastItem);
		EnsureVisible(hLastItem);
		m_hItemDrop = hLastItem;
		CImageList::DragShowNolock(TRUE);
	}

	//Expand the item if the timer ticks has expired
	if (m_TimerTicks == 3)
	{
		m_TimerTicks = 0;
		Expand(m_hItemDrop, TVE_EXPAND);
	}

	//Expand the selected item if it is collapsed and
	//the timeout has occurred
	TV_ITEM tvItem;
	tvItem.hItem = m_hItemDrop;
	tvItem.mask = TVIF_HANDLE | TVIF_CHILDREN | TVIF_STATE;
	tvItem.stateMask = TVIS_EXPANDED;
	GetItem(&tvItem);
	if (tvItem.cChildren && ((tvItem.state & TVIS_EXPANDED) == 0))
	{
		m_TimerTicks++;
	}
}

void CPixTreeCtrl::SetParentHWnd(CWnd *parent)
{
	this->m_parentwnd = parent;
}

CString CPixTreeCtrl::GetSelectedFolder()
{
	return (this->m_selected);
}