//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CPixWizSub4
// File		:	PixWizSub4.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class is used to handle the sub page 4 from the wizard.
//              Sub page 4 is used to get the values how many links have to be 
//				checked.
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixWizSub4.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CPixWizSub4::CPixWizSub4(CWnd* pParent /*=NULL*/)
	: CNewWizPage(CPixWizSub4::IDD, pParent)
{
	m_PixLinks = _T("");
}


void CPixWizSub4::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ST_CAPTION, m_CaptionCtrl);
	DDX_Text(pDX, IDC_LINKS, m_PixLinks);
}


BEGIN_MESSAGE_MAP(CPixWizSub4, CNewWizPage)
	ON_BN_CLICKED(IDC_CHECK_LINK, OnCheckLink)
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnCheckLink
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to handle the enabling of the edit field by 
//			  using the checkbox.
//
//
//----------------------------------------------------------------------------------

void CPixWizSub4::OnCheckLink() 
{
	if (IsDlgButtonChecked(IDC_CHECK_LINK) == FALSE) 
	{
		SetDlgItemInt(IDC_LINKS, 0, FALSE);
		GetDlgItem(IDC_LINKS)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_LINKS)->EnableWindow(TRUE);
	}
}



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to make the edit field disable when the subpage 
//            is called and to set a default value in the edit field
//
// @return  : TRUE if successfull, ....
//
//---------------------------------------------------------------------------------- 

BOOL CPixWizSub4::OnInitDialog() 
{
	CNewWizPage::OnInitDialog();

// Set font for title
	m_CaptionCtrl.SetFont(&m_LargeFont, TRUE);

// Set default values

	CheckDlgButton(IDC_CHECK_LINK,1);

	int link = 2;
	
	SetDlgItemInt(IDC_LINKS, link);

	GetDlgItem(IDC_LINKS)->EnableWindow(TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control

}



//----------------------------------------------------------------------------------
//  Function OnWizardNext
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to check the edit field of a entered value, if 
//            nothing is entered, the function shows a message.
//
// @return  : 0 if successfull, ....
//
//----------------------------------------------------------------------------------

LRESULT CPixWizSub4::OnWizardNext()
{
	int link;
	int link_max = 5000;
	BOOL checklink;

	link = GetDlgItemInt(IDC_LINKS);
	checklink = IsDlgButtonChecked(IDC_CHECK_LINK);

	if (link == NULL && checklink == TRUE)
	{
		AfxMessageBox("If you like to follow links, you should enter a value");
		return -1;
	}
	if (link > link_max) 
	{
		AfxMessageBox("It is not possible to grab more then 5000 links");
		return -1;
	}
	if (link < 0)
	{
		AfxMessageBox("We can not follow a negativ amount of links...think again :)");
		return -1;
	}
	CNewWizPage::OnWizardNext();
	return 0;
}



//----------------------------------------------------------------------------------
//  Function GetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the entered values. It also convert the 
//			  entered values to CString if they are not.
// 
// @return  : m_PixLinks
//
//----------------------------------------------------------------------------------

CString CPixWizSub4::GetData()
{
	BOOL checklink;
	int link;
	char temp_link[10];

	checklink = IsDlgButtonChecked(IDC_CHECK_LINK);
	link = GetDlgItemInt(IDC_LINKS);

	if (checklink == TRUE) 
	{
		_itoa(link,temp_link,10);
		m_PixLinks = CString(temp_link);
	}
	else
		m_PixLinks = CString("0");
	
	return m_PixLinks;
}



//----------------------------------------------------------------------------------
//  Function SetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function has to be overwritten in this sub page. We do not set 
//            new values
// 
//
//----------------------------------------------------------------------------------

void CPixWizSub4::SetData(CString data)
{
	return;
}
