//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTabCtrl
// File     :   PixTabCtrl.cpp
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class controls the Tabs for the tree view. 
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixGrabber.h"

#include "PixTabCtrl.h"
#include "PixTreeGlobal.h"
#include "PixTreeLocal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNAMIC(CPixTabCtrl, CTabCtrl)

BEGIN_MESSAGE_MAP(CPixTabCtrl, CTabCtrl)
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


//----------------------------------------------------------------------------------
//  Function CPixTabCtrl
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Constructor
// 
//----------------------------------------------------------------------------------
CPixTabCtrl::CPixTabCtrl() : CTabCtrl()
{
	//Create CTreeCtrl objects
	m_tglob = new CPixTreeGlobDlg();
	m_tloc = new CPixTreeLocDlg();
}


//----------------------------------------------------------------------------------
//  Function CPixTabCtrl()
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Destructor
// 
//----------------------------------------------------------------------------------
CPixTabCtrl::~CPixTabCtrl()
{
	delete(m_tglob);
	delete(m_tloc);
}

//----------------------------------------------------------------------------------
//  Function Init
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Creates the Tab Dialogs for the tree view.
//
//----------------------------------------------------------------------------------
void CPixTabCtrl::Init(CWnd *hwnd)
{
	m_tabCurrent=0;

	m_tglob->Create(IDD_TAB_GLOBAL, this);
	m_tloc->Create(IDD_TAB_LOCAL, this);

	m_tglob->ShowWindow(SW_SHOW);
	m_tloc->ShowWindow(SW_HIDE);

	m_tglob->SetParentHWnd(hwnd);
	m_tloc->SetParentHWnd(hwnd);
}


//----------------------------------------------------------------------------------
//  Function OnSize
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Resizes the Tab Dialogs, when an OnSize message is received.
//			  The Dialog always occupies the entire area
// 
// @param	: UINT nType, Specifies the type of resizing requested.
// @param	: int cx, Specifies the new width of the client area. 
// @param	: int cy, Specifies the new height of the client area. 
//
//----------------------------------------------------------------------------------
void CPixTabCtrl::OnSize(UINT nType, int cx, int cy) 
{
	CRect tabRect, itemRect;
	int nX, nY, nXc, nYc;

	GetClientRect(&tabRect);
    GetItemRect(0, &itemRect);

	nX=itemRect.left;
	nY=itemRect.bottom+1;
	nXc=tabRect.right-itemRect.left-1;
	nYc=tabRect.bottom-nY-1;

	m_tabCurrent = GetCurFocus();

	m_tglob->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);
	m_tloc->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW);

	if (m_tabCurrent == 0)
	{
		m_tglob->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);
	}
	else m_tloc->SetWindowPos(&wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW);
}


//----------------------------------------------------------------------------------
//  Function OnLButtonDown
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Sets on left click the focus of the Tabs to clicked Tab and
//			  hide the other one. 
//
// @param	: UINT nFlags, Indicates which virtual keys are down.
// @param	: CPoint point, Specifies the x- and y-coordinate of the cursor.
//
//----------------------------------------------------------------------------------
void CPixTabCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CTabCtrl::OnLButtonDown(nFlags, point);

	if(m_tabCurrent != GetCurFocus())
	{
		if (m_tabCurrent == 1)
		{
			m_tloc->ShowWindow(SW_HIDE);
			m_tabCurrent=GetCurFocus();
			m_tglob->ShowWindow(SW_SHOW);
			m_tglob->SetFocus();
		}
		else
		{
			m_tglob->ShowWindow(SW_HIDE);
			m_tabCurrent=GetCurFocus();
			m_tloc->ShowWindow(SW_SHOW);
			m_tloc->SetFocus();
		}
	}
}