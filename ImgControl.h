//-----------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CImgControl
// File		:	ImgControl.h
// Author	:	Thierry "real" Haven
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once

// ImgControl.h : header file
//

#include "ThumbnailButton.h"
#include "Picture.h"

/////////////////////////////////////////////////////////////////////////////
// CImgControl window


//--[define]-------------------------------------------------------------

// we use those 2 values to define which kind of resizing is actually performed
// on a picture when we are in X:1 mode (= SMART REZIZE mode). SMART_DIM_ALWAYS 
// tells the app to resize the picture to client area, but keep aspect ratio
// since we are in SMART RESIZE mode. SMART_NO_DIM_IF_SMALLER do not resizes
// the picture if it is smaller than the client area. If the picture is bigger
// than the client area, it is always resized to (smart) fit the screen, no matted 
// if SMART_DIM_ALWAYS or SMART_NO_DIM_IF_SMALLER are set.
#define SMART_DIM_ALWAYS		0x11111111
#define SMART_NO_DIM_IF_SMALLER 0xFFFFFFFF

//--[class]--------------------------------------------------------------

class CImgControl : public CWnd
{

//--[variables]----------------------------------------------------------
public:
	long m_sx;				// scroll x
	long m_sy;				// scroll y
	double zoom;			// actual zoom
	long m_rex;				// actual size of a picture that has been zoomed (resolution x)
	long m_rey;				// idem for y
	CPicture *m_ppic;		// picture displayed
	BOOL m_rcImage;			// rect to display image in
	UINT m_iHowScale;		// how to scale image

	CRect rect;				// actual rect
	CRect hrect;			// old imagerect for optimizations on draw

	BOOL scrolled;

	DWORD m_smartmode;	// type of smart resizing. "smart resizing" resizes a picture
						// in a client area and try to keep original aspect ratio
						// i.e. val = (width/height) will be always the same value
						// no matter what the resizing factor is.
						// Can be SMART_DIM_ALWAYS or SMART_NO_DIM_IF_SMALLER
private:
	static CBrush m_bkBrush;	// brush used to fill areas that need to be black
								// (ie. we only invalidate some parts of the client area)
								// to prevent flickering... but we still have some..
								// FIXME

//--[construction]-------------------------------------------------------
public:
	CImgControl();
	virtual ~CImgControl();

//--[functions]-------------------------------------------------------
public:
	void LoadPic(CPicture *pppic);
	void UpdateScroll(int px, int py);
	void UpdateZoom(int value);
	void SetFit(BOOL fit);
	void SmartResize(DWORD pmode);
	void GetImageRect(CRect& rc);

private:
	BOOL RegisterWindowClass();

//--[overriden]-------------------------------------------------------
protected:
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()
};