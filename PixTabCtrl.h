//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTabCtrl
// File     :   PixTabCtrl.h
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class controls the Tabs for the tree view. 
//
//----------------------------------------------------------------------------------

#include "PixTreeGlobal.h"
#include "PixTreeLocal.h"
#pragma once


class CPixTabCtrl : public CTabCtrl
{
	DECLARE_DYNAMIC(CPixTabCtrl)

// Construction
public:
	CPixTabCtrl();
	virtual ~CPixTabCtrl();

	
	//CDialog *m_tabPages[2];
	CPixTreeGlobDlg *m_tglob;
	CPixTreeLocDlg *m_tloc;
	CDialog m_tabGlobal;
	CDialog m_tabLocal; 
	int m_tabCurrent;
	int m_nNumberOfTabs;

// Operations
public:
	void Init(CWnd *hwnd);
	void SetParentHWndToChildrenTabs(CWnd *);
//	virtual BOOL AttachControlToTab(CWnd * _pControl,INT _nTabNum);


// Generated message map functions
protected:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
};