//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CPixGrabDlg
// File		:	PixGrabDlg.cpp
// Author	:	Reto Buerki
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This is a from CDialog derived class which provides some
//				CPixGrabThread controlling functions and functions to display
//				all kind of status information. Normally this functions are called
//				from within the thread which uses a CPixGrabDlg object as its
//				associated window.
//
//----------------------------------------------------------------------------------
#include "stdafx.h"
#include "PixGrabber.h"
#include "PixGrabDlg.h"
#include ".\pixgrabdlg.h"
// debug
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifndef DEBUG_MSG
#define	DEBUG_MSG	FALSE
#endif

// thread start message
#define WM_START	WM_USER + 1
#define WM_CLEAN	WM_USER + 2

IMPLEMENT_DYNAMIC(CPixGrabDlg, CDialog)

BEGIN_MESSAGE_MAP(CPixGrabDlg, CDialog)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_VERBOSEO, OnBnClickedVerboseo)
	ON_BN_CLICKED(IDC_CLEARQ, OnBnClickedClearQ)
	ON_BN_CLICKED(IDC_CLEARLOG, OnBnClickedClearLog)
	ON_BN_CLICKED(IDC_DELETE, OnBnClickedDelete)
END_MESSAGE_MAP()

//--[Construction / Deconstruction]-------------------------------------------------
CPixGrabDlg::CPixGrabDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPixGrabDlg::IDD, pParent)
{
	// load icon
	m_hIcon = AfxGetApp()->LoadIcon( IDR_MAINFRAME );
}

CPixGrabDlg::~CPixGrabDlg()
{

}

//--[Overrides]---------------------------------------------------------------------
BOOL CPixGrabDlg::Create(UINT nID, CWnd *pWnd)
{
	m_pWnd = pWnd;

	return CDialog::Create(nID, pWnd);
}

void CPixGrabDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOG, m_Log);
	DDX_Control(pDX, IDC_CURRMSG, m_currMSG);
	DDX_Control(pDX, IDC_PROGRESS1, m_ProgressPix);
	DDX_Control(pDX, IDC_LIST2, m_Q);
	DDX_Control(pDX, IDC_CURRURL, m_currURL);
	DDX_Control(pDX, IDC_PROGRESS2, m_ProgressLinks);
}
//----------------------------------------------------------------------------------
//	protected Function OnInitDialog( void )
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.3
//
// @task	:
//  Override of CDialog::OnInitDialog(). Calls the base class function first and
//  then performs init tasks on the two CListCtrls and loads and sets the Icons.
//
// @return TRUE
//
//----------------------------------------------------------------------------------
BOOL CPixGrabDlg::OnInitDialog( void )
{
	CDialog::OnInitDialog( );

	// Set the icon for this dialog.
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// init ListCtrls
	m_Log.ModifyStyle(0, LVS_REPORT);
	m_Log.InsertColumn(0, "File", LVCFMT_LEFT, 150);
	m_Log.InsertColumn(1, "Size [kb]", LVCFMT_LEFT, 60);
	m_Log.InsertColumn(2, "Speed [kb/s]", LVCFMT_LEFT, 80);
	m_Log.InsertColumn(3, "Status", LVCFMT_LEFT, 64);

	m_Log.SetExtendedStyle(LVS_EX_GRIDLINES);

	m_Q.ModifyStyle(0, LVS_REPORT);
	m_Q.InsertColumn(0, "Site", LVCFMT_LEFT, 150);
	m_Q.InsertColumn(1, "MinSize [Kb]", LVCFMT_LEFT, 71);
	m_Q.InsertColumn(2, "MaxSize [Kb]", LVCFMT_LEFT, 74);
	m_Q.InsertColumn(3, "Type", LVCFMT_LEFT, 50);
	m_Q.InsertColumn(4, "FollowLinks", LVCFMT_LEFT, 68);
	m_Q.InsertColumn(5, "LinkCount", LVCFMT_LEFT, 65);

	m_Q.SetExtendedStyle(LVS_EX_CHECKBOXES);

	// create the image list that is attached to the list control
	InitImageList();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

//----------------------------------------------------------------------------------
//	protected Function OnPaint()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Override of CDialog::OnPaint(). Checks wheter the dialog is minimized and
//  then draws the icon or calls the base class function.
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::OnPaint()
{
	if (IsIconic())
	{
		// device context for painting
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPixGrabDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

//--[thread controlling functions]--------------------------------------------------

//----------------------------------------------------------------------------------
//	public Function RegisterThread(CPixGrabThread* myThread)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function associates a thread given as an argument (as a pointer) to its
//  member m_pThread. This function MUST be called before the thread is actually
//  started since the dialog uses the member m_pThread to control the behavior
//  of the thread.
//
// @param CPixGrabThread* myThread	thread to register with the dialog
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::RegisterThread(CPixGrabThread* myThread)
{
	m_pThread = (CPixGrabThread *)myThread;
}

//--[display status functions]------------------------------------------------------

//----------------------------------------------------------------------------------
//	public Function ShowFollowedLinks(int f_links)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function displays the number of links followed.
//
// @param int f_links				actual followed links count
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ShowFollowedLinks(int f_links)
{
	CString strBuffer;
	strBuffer.Format("%d", f_links);
	GetDlgItem(IDC_LINKSFOLLOWED)->SetWindowText(strBuffer);

}

//----------------------------------------------------------------------------------
//	public Function ShowMaxLink(int maxlink)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function displays the maximum links to follow.
//
// @param int maxlink				maximum follow link counter
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ShowMaxLink(int maxlink)
{
	CString strBuffer;
	strBuffer.Format("%d", maxlink);
	GetDlgItem(IDC_PMAXLINK)->SetWindowText(strBuffer);
}

//----------------------------------------------------------------------------------
//	public Function ShowDownloadedPix(int d_pix)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function displays the number of pictures download.
//
// @param int d_pix					actual number of pictures downloaded
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ShowDownloadedPix(int d_pix)
{
	CString strBuffer;
	strBuffer.Format("%d", d_pix);
	GetDlgItem(IDC_PIXDOWN)->SetWindowText(strBuffer);

}

//----------------------------------------------------------------------------------
//	public Function ShowDownloadedData(double data)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.1
//
// @task	:
//  Function displays the amount of data downloaded. Switches between Mb and Kb
//  depending on the amount of data.
//
// @param double data				actual downloaded data in bytes
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ShowDownloadedData(double data)
{
	CString strBuffer;
	double calcData;
	// display either Mb or Kb
    if((calcData = data / 1024.0) > 1024.0)
	{
		calcData = calcData / 1024.0;
		strBuffer.Format("%.2f Mb", (float)calcData);
	}
	else
	{
		strBuffer.Format("%.2f Kb", (float)calcData);
	}

	GetDlgItem(IDC_DATADOWN)->SetWindowText(strBuffer);
}

//----------------------------------------------------------------------------------
//	public Function ShowCachedLinks(int links)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function displays the actual links cached (= stored in an CStringArray).
//
// @param int links					actual links in cache
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ShowCachedLinks(int links)
{
	CString strBuffer;
	strBuffer.Format("%d", links);
	GetDlgItem(IDC_LINKSCACHE)->SetWindowText(strBuffer);
}

//----------------------------------------------------------------------------------
//	public Function ShowCachedPix(int pix)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function displays the actual pictures cached (= stored in an CStringArray).
//
// @param int pix					actual pix in cache
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ShowCachedPix(int pix)
{

	CString strBuffer;
	strBuffer.Format("%d", pix);
	GetDlgItem(IDC_PIXCACHE)->SetWindowText(strBuffer);

}

//----------------------------------------------------------------------------------
//	public Function ShowInformation(int IDC, LPCTSTR fmt, ...)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.1
//
// @task	:
//  Function displays information (LPCTSTR format) in the desired control
//  specified by IDC. The c style format can be used, e.g:
//  ShowInformation(IDC_WHATEVER, "Downloading %s...", myURL);
//
// @param int IDC					ID of control to display information
// @param LPCTSTR fmt				desired text to display
// @param ...						see example above how to use this
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ShowInformation(int IDC, LPCTSTR fmt, ...)
{
	va_list args;
    TCHAR buffer[512];

    va_start(args, fmt);
    _vstprintf(buffer, fmt, args);
    va_end(args);

	CString output = buffer;

	// tuncate output
	if(output.GetLength() > 55)
	{
		output = output.Left(55);
		output = output + "(...)";
	}


	GetDlgItem(IDC)->SetWindowText(output);

	if(DEBUG_MSG)
        Sleep(500);

	UpdateWindow();
}

//----------------------------------------------------------------------------------
//	public Function ShowSourceInfo(...)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.2
//
// @task	:
//  Function displays information about a downloaded picture, like size, speed and
//  the status code (OK, N_F, SKIP, TYPE). Depending on the parameter 'success',
//  a different icon is associated with the item (indicating success or failure)
//
// @param CString strName			name of the picture
// @param int size					picture size in bytes
// @param double speed				speed in Kb/s
// @param CString strStatus			a status string
// @param BOOL success				TRUE or FALSE
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ShowSourceInfo(CString strName,
								 int size,
								 double speed,
								 CString strStatus,
								 BOOL success)
{
	// use the LV_ITEM structure
	LVITEM lvi;
	CString strItem;

    lvi.mask =  LVIF_IMAGE | LVIF_TEXT;

	// insert the item
	lvi.iItem = 0;
	lvi.iSubItem = 0;
	lvi.pszText = (LPTSTR)(LPCTSTR)(strName);
	// set icon depending on success or failure
	if(success)
        lvi.iImage = 0;
	else
		lvi.iImage = 1;

	m_Log.InsertItem(&lvi);

	// set subitem 1
	strItem.Format(_T("%d"), size / 1024);
	lvi.iSubItem =1;
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	m_Log.SetItem(&lvi);

	if(speed == 0.0)
		strItem = '-';
	else
		strItem.Format(_T("%7.2f"), (float)speed);

	// set subitem 2
	lvi.iSubItem =2;
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	m_Log.SetItem(&lvi);

	// set subitem 3
	lvi.iSubItem =3;
	lvi.pszText = (LPTSTR)(LPCTSTR)(strStatus);
	m_Log.SetItem(&lvi);
}

//--[queue handling functions]------------------------------------------------------

//----------------------------------------------------------------------------------
//	public Function AddQueueItem(...)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function adds an item to the queue. The thread associated with this status
//  window will read from the queue to receive data like actual URL to process,
//  maximum and minimum sizes of images, wheter it should follow links and how many
//  link it should follow.
//
// @param CString strURL			URL to add
// @param int minSize			    minimum picture size for this entry
// @param int maxSize				maximum picture size for this entry
// @param char pixTypes				picture types (A=all,G=gif,J=jpg)
// @param BOOL followLinks			wheter links should be followed
// @param int linkCount				how many links to follow
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::AddQueueItem(CString strURL,
							   int minSize,
							   int maxSize,
							   char pixTypes,
							   BOOL followLinks,
							   int linkCount)
{
	// use the LV_ITEM structure
	LVITEM lvi;

	CString strItem;

    lvi.mask =  LVIF_IMAGE | LVIF_TEXT;

	// insert the item
	lvi.iItem = 0;
	lvi.iSubItem = 0;
	lvi.pszText = (LPTSTR)(LPCTSTR)(strURL);
	m_Q.InsertItem(&lvi);

	// set subitem 1
    lvi.iSubItem =1;
	if(minSize == 0)
		strItem = "-";
	else
        strItem.Format("%d", minSize);

	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	lvi.lParam = 0;
	m_Q.SetItem(&lvi);

	// set subitem 2
	lvi.iSubItem =2;
	if(maxSize == 0)
		strItem = "-";
	else
        strItem.Format("%d", maxSize);
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	lvi.lParam = 0;
	m_Q.SetItem(&lvi);

	// set subitem 3
	lvi.iSubItem =3;
	strItem.Format("%c", pixTypes);
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	lvi.lParam = 0;
	m_Q.SetItem(&lvi);

	// set subitem 4
	lvi.iSubItem =4;
	if(followLinks)
		strItem = "Yes";
	else
		strItem = "No";
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	m_Q.SetItem(&lvi);

	// set subitem 5
	lvi.iSubItem =5;
	strItem.Format("%d", linkCount);
	lvi.pszText = (LPTSTR)(LPCTSTR)(strItem);
	lvi.lParam = 0;
	m_Q.SetItem(&lvi);
}

//----------------------------------------------------------------------------------
//	public Function RemoveQueueItem(int pos)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function removes item at pos from the queue.
//
// @param int pos					delete item at this position
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::RemoveQueueItem(int pos)
{
	m_Q.DeleteItem(pos);
}

//----------------------------------------------------------------------------------
//	public Function GetNextQueueItem(...)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.3
//
// @task	:
//  Function returns the next URL to process and also other parameters related
//  to this URL (min/max Size of pictures, type of pix, follow links (yes/no)).
//  If there is no more item in the queue, the function will return FALSE.
//
// @param CString &strURL 			this will hold the URL to process
// @param int &minSize				stores the minimum size of a picture
// @param int &maxSize				stores the maximum size of a picture
// @param char &pixTypes			picture types to download (A=all,G=gif,J=jpg)
// @param BOOL &isFollowLinks		should we follow links?
// @param &maxFollowCount			maximum links to follow
//
// @return TRUE if more items are found, FALSE if not
//
//----------------------------------------------------------------------------------
BOOL CPixGrabDlg::GetNextQueueItem(CString &strURL,
								   int &minSize,
								   int &maxSize,
								   char &pixTypes,
								   BOOL &isFollowLinks,
								   int &maxFollowCount)
{
	// since the actual url to process is always at the end
	strURL = m_Q.GetItemText(m_Q.GetItemCount()-1, 0);

	// no more items?
	if(strURL == "")
		return FALSE;

	// convert CString objects to minSize/maxSize
	if(m_Q.GetItemText(m_Q.GetItemCount()-1, 1).GetAt(0) == '-')
		minSize = 0;
	else
        minSize = atoi(m_Q.GetItemText(m_Q.GetItemCount()-1, 1).GetBuffer());

	if(m_Q.GetItemText(m_Q.GetItemCount()-1, 2).GetAt(0) == '-')
		// we just specify a very "big" picture as max
		maxSize = 165000;
	else
		maxSize = atoi(m_Q.GetItemText(m_Q.GetItemCount()-1, 2).GetBuffer());

	// what type of picture do we want?
	pixTypes = m_Q.GetItemText(m_Q.GetItemCount()-1, 3).GetAt(0);
	// should we follow links?
	if(m_Q.GetItemText(m_Q.GetItemCount()-1, 4) == "Yes")
		isFollowLinks = TRUE;
	else
		isFollowLinks = FALSE;

	// how many links to follow?
	maxFollowCount = atoi(m_Q.GetItemText(m_Q.GetItemCount()-1, 5));

	return TRUE;
}

//--[dialog appearence handler functions]-------------------------------------------

//----------------------------------------------------------------------------------
//	public Function ResetStatus()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function resets the status of the dialog. That means all counters are set back
//  to zero and the two CListCtrls are cleared.
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::ResetStatus()
{
	m_Q.DeleteAllItems();
	m_Log.DeleteAllItems();
	m_ProgressPix.SetPos(0);
	m_ProgressLinks.SetPos(0);
	m_currMSG.SetWindowText("Ready...");
	m_currURL.SetWindowText("-");

	GetDlgItem(IDC_SERVERSOFT)->SetWindowText("-");
	GetDlgItem(IDC_PIXCACHE)->SetWindowText("0");
	GetDlgItem(IDC_LINKSCACHE)->SetWindowText("0");
	//GetDlgItem(IDC_DATADOWN)->SetWindowText("0");
	GetDlgItem(IDC_PIXDOWN)->SetWindowText("0");
	GetDlgItem(IDC_LINKSFOLLOWED)->SetWindowText("0");

}

//----------------------------------------------------------------------------------
//	protected Function InitImageList()
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  Function creates an image list and adds icons to this list. After that, the list
//  is 'attached' to m_Log (CListCtrl)
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::InitImageList()
{
	// create 256 color image lists
	HIMAGELIST hList = ImageList_Create(16, 16, ILC_COLOR8 | ILC_MASK, 8, 1);
	m_cImageList.Attach(hList);

	// load icons for CListCtrl
	CBitmap cBmp;
	cBmp.LoadBitmap(IDB_IMAGELIST);
	m_cImageList.Add(&cBmp, RGB(255,0, 255));

	// attach it to the CListCtrl
	m_Log.SetImageList(&m_cImageList, LVSIL_SMALL);

}

//----------------------------------------------------------------------------------
//	protected Function SetStatusFollowLinks(BOOL status, int followCount)
//----------------------------------------------------------------------------------
//
// @author	: Reto Buerki
// @version	: 1.0
//
// @task	:
//  This function is used to set the appearence of the dialog depending wheter
//  we follow links or not. If we don't follow links, some Controls are not enabled
//
// @param BOOL status				TRUE=follow links, FALSE=not
// @param int followCount			how many links to follow
//
//----------------------------------------------------------------------------------
void CPixGrabDlg::SetStatusFollowLinks(BOOL status, int followCount)
{
	m_ProgressLinks.EnableWindow(status);
	GetDlgItem(IDC_PMAXLINK)->EnableWindow(status);
	GetDlgItem(IDC_Q)->EnableWindow(status);
	GetDlgItem(IDC_LFOLLOWED_S)->EnableWindow(status);
	GetDlgItem(IDC_LINKSFOLLOWED)->EnableWindow(status);
	m_ProgressLinks.EnableWindow(status);

	if(status)
	{
        m_ProgressLinks.SetPos(0);
		m_ProgressLinks.SetStep(1);
		m_ProgressLinks.SetRange(0, (int)followCount);
		ShowMaxLink(followCount);
	}
	else
	{
        m_ProgressLinks.SetPos(0);
		GetDlgItem(IDC_PMAXLINK)->SetWindowText("");
	}

}

//--[message handler functions]-----------------------------------------------------

void CPixGrabDlg::OnBnClickedClearLog()
{
	// clear m_Log
	m_Log.DeleteAllItems();
}

void CPixGrabDlg::OnBnClickedClearQ()
{
	// clear m_Q
	m_Q.DeleteAllItems();
}

void CPixGrabDlg::OnBnClickedCancel()
{
	// stop the functions in the thread
	m_pThread->m_isCanceled = TRUE;
	m_pThread->PostThreadMessage(WM_CLEAN, NULL, NULL);

	OnCancel();
}

void CPixGrabDlg::OnBnClickedVerboseo()
{
	// 'invert' the verbose mode
	if(m_pThread->m_verboseMode == TRUE)
		m_pThread->m_verboseMode = FALSE;
	else
		m_pThread->m_verboseMode = TRUE;
}

void CPixGrabDlg::OnBnClickedDelete()
{
	// delete all checked checkboxes
	int count = m_Q.GetItemCount();
	for(int i=count; i >= 0; i--)
	{
		if(ListView_GetCheckState(m_Q.m_hWnd, i))
			RemoveQueueItem(i);
	}

}
