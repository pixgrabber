//-----------------------------------------------------------------------
//	Project PixGrabber
//-----------------------------------------------------------------------
//
// CLASS	:	CImgControl
// File		:	ImgControl.cpp
// Author	:	Thierry "real" Haven
//				Based on Paul DiLascia MSDN Magazine sample, October 2001
// Date		:	09.12.2003
// Version	:	0.1
// Descr	:	This class implements support for picture loading.
//				Many formats are supported (JPG, GIF, BMP etc..)
//				This class is used both in CThumbnailControl and CImgControl
//				custom controls in PixGrabber.
//
//-----------------------------------------------------------------------

#include "StdAfx.h"
#include "Picture.h"
#include "afxpriv2.h"

//--[MFC generated]------------------------------------------------------
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//--[constuctor]---------------------------------------------------------
CPicture::CPicture()
{
}

CPicture::~CPicture()
{
}

//-----------------------------------------------------------------------
//	Function BOOL Load(UINT nIDRes)
//-----------------------------------------------------------------------
//
// @author	: Paul DiLascia (MSDN mag)
// @version	: 0.1
//
// @task	:
//		Load picture from resource. Looks for "IMAGE" type.
//		(note : this function doesn't work for some reason FIXME)
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CPicture::Load(UINT nIDRes)
{
	// find resource in resource file
	HINSTANCE hInst = AfxGetResourceHandle();
	HRSRC hRsrc = ::FindResource(hInst,
		MAKEINTRESOURCE(nIDRes),
		"IMAGE"); // type
	if (!hRsrc)
		return FALSE;

	// load resource into memory
	DWORD len = SizeofResource(hInst, hRsrc);
	BYTE* lpRsrc = (BYTE*)LoadResource(hInst, hRsrc);
	if (!lpRsrc)
		return FALSE;

	// create memory file and load it
	CMemFile file(lpRsrc, len);
	BOOL bRet = Load(file);
	FreeResource(hRsrc);

	return bRet;
}

//-----------------------------------------------------------------------
//	Function BOOL Load(LPCTSTR pszPathName)
//-----------------------------------------------------------------------
//
// @author	: Paul DiLascia (MSDN mag) / Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Load picture from path name.
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CPicture::Load(LPCTSTR pszPathName)
{
	CFile file;
	
	char ext[8];
	lstrcpy(ext, pszPathName + lstrlen(pszPathName) - 5);
	// get file extension and convert it to lowercase
	for(int i =1; i < 5; i++)
	{
		ext[i] = (char) tolower(ext[i]);
	}
	// we only load files having those extensions ;
	if (!lstrcmp(ext+1, ".jpg") || !lstrcmp(ext, ".jpeg") || !lstrcmp(ext+1, ".bmp") || !lstrcmp(ext+1, ".gif"))
	{
		if (!file.Open(pszPathName, CFile::modeRead|CFile::shareDenyWrite))
			return FALSE;
		BOOL bRet = Load(file);
		file.Close();
		return bRet;
	}
	else
	{
		return FALSE;
	}
}

//-----------------------------------------------------------------------
//	Function BOOL Load(CFile& file)
//-----------------------------------------------------------------------
//
// @author	: Paul DiLascia (MSDN mag)
// @version	: 0.1
//
// @task	:
//		Load picture from CFile
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CPicture::Load(CFile& file)
{
	CArchive ar(&file, CArchive::load | CArchive::bNoFlushOnDelete);
	return Load(ar);
}

//-----------------------------------------------------------------------
//	Function BOOL Load(CArchive& ar)
//-----------------------------------------------------------------------
//
// @author	: Paul DiLascia (MSDN mag)
// @version	: 0.1
//
// @task	:
//		Load picture from CArchive
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CPicture::Load(CArchive& ar)
{
	CArchiveStream arcstream(&ar);
	return Load((IStream*)&arcstream);
}

//-----------------------------------------------------------------------
//	Function BOOL Load(IStream* pstm)
//-----------------------------------------------------------------------
//
// @author	: Paul DiLascia (MSDN mag)
// @version	: 0.1
//
// @task	:
//		Load from stream (IStream). This is the one that really does it: call
//		OleLoadPicture to do the work.
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CPicture::Load(IStream* pstm)
{
	Free();
	HRESULT hr = OleLoadPicture(pstm, 0, FALSE,
		IID_IPicture, (void**)&m_spIPicture);
	if (hr != S_OK) return FALSE;
	//ASSERT(SUCCEEDED(hr) && m_spIPicture);	
	return TRUE;
}

//-----------------------------------------------------------------------
//	Function BOOL Render(CDC* pDC, CRect rc, LPCRECT prcMFBounds, CRect client)
//-----------------------------------------------------------------------
//
// @author	: Paul DiLascia (MSDN mag) / Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Render to device context. Covert to HIMETRIC for IPicture..
//
//	@param :	CDC* pDC	pointer to device context
//	@param :	CRect rc	size of picture (see CImgControl for details on this)
//	@param :	LPCRECT		bounding box (???) (see CImgControl for details on this)
//	@param :	CRect		rect of client/destination area
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CPicture::Render(CDC* pDC, CRect rc, LPCRECT prcMFBounds, CRect client) const
{
	ASSERT(pDC);

	if (rc.IsRectNull()) {
		CSize sz = GetImageSize(pDC);
		rc.right = sz.cx;
		rc.bottom = sz.cy;
	}
	long hmWidth,hmHeight; // HIMETRIC units
	if (this->m_spIPicture != NULL)
	{
		GetHIMETRICSize(hmWidth, hmHeight);
		m_spIPicture->Render(*pDC,
							rc.left, // + (client.right - client.left - rc.right + rc.left)/2,
							rc.top, //+ (client.bottom - client.top + rc.top - rc.bottom)/2,
							rc.Width(),
							rc.Height(),
			0, hmHeight, hmWidth, -hmHeight, prcMFBounds);

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//-----------------------------------------------------------------------
//	Function BOOL Load(IStream* pstm)
//-----------------------------------------------------------------------
//
// @author	: Paul DiLascia (MSDN mag) / Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Get image size in pixels. Converts from HIMETRIC to device coords.
//
//	@param :		CDC* pDC	pointer to device context
//
//	@return :		CSize of the picture
//
//-----------------------------------------------------------------------
CSize CPicture::GetImageSize(CDC* pDC) const
{
	if (!m_spIPicture)
		return CSize(0,0);
	
	LONG hmWidth, hmHeight; // HIMETRIC units
	m_spIPicture->get_Width(&hmWidth);
	m_spIPicture->get_Height(&hmHeight);
	CSize sz(hmWidth,hmHeight);
	if (pDC==NULL)
	{
		CWindowDC dc(NULL);
		dc.HIMETRICtoDP(&sz); // convert to pixels
	}
	else
	{
		pDC->HIMETRICtoDP(&sz);
	}
	return sz;
}

