//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixGrabThread
// File		:	PixGrabThread.h
// Author	:	Reto Buerki
// Date		:	25.11.2003
// Version	:	1.2a
//
//----------------------------------------------------------------------------------
#pragma once

#include <afxinet.h>

//--[needed for HTML parsing]-------------------------------------------------------
#include <comdef.h>
#include <mshtml.h>
#pragma warning(disable : 4146)	//see Q231931 for explaintation
#import <mshtml.tlb> no_auto_exclude
//----------------------------------------------------------------------------------

#define MAX_LINK_COUNT		130
#define MAX_IMAGES_COUNT	120

#define BUFFER_SIZE			4095


class CPixGrabThread : public CWinThread
{
	DECLARE_DYNCREATE(CPixGrabThread)

//--[functions]---------------------------------------------------------------------
protected:
	CPixGrabThread();
	virtual ~CPixGrabThread();
	virtual BOOL InitInstance();
	virtual int ExitInstance();

	// thread controlling function
	// virtual BOOL PreTranslateMessage(MSG *pMsg);
	afx_msg void StartFunc(WPARAM n1, LPARAM n2);
	afx_msg void CleanFunc(WPARAM n1, LPARAM n2);
	DECLARE_MESSAGE_MAP()

private:
	BOOL	Initialise(LPCTSTR szAgentName = NULL);
	void	Close();

public:
	// main processing function
	BOOL	HandleURL(CString strURL);
	// URL processing functions
	CHttpFile* OpenWebFile(CString actURL,
        LPCTSTR szAgentName,
		int nPort);
	DWORD	GetWebFile(BOOL isPage,
        CString actURL,
        CString& strBuffer,
        LPCTSTR szAgentName,
        int nPort);
	BOOL	ParseHTML(CString& htmlBuffer);
	BOOL	PrepareURL(CString strObject, BOOL isLink);
	BOOL	DownloadPix();

	// check and util functions
	BOOL	CheckPictureSize(int actSize);
	BOOL	CheckPictureType(CString pixURL, char type);
	void    RemoveObject(CString &strURL);
	void    HandleRelativePath2P(CString &strObject);
	void    HandleRelativePath1P(CString &strObject);
	void    HandleRelativePath0P(CString &strObject);
	CString ExtractPicName(CString picURL);
	void    SetTimeOut(DWORD timeOut);
    BOOL	PrepareDirs();

	// status functions
	DWORD	GetPageStatusCode();
	double  GetTransferRate();



//--[member variables]--------------------------------------------------------------
protected:
	// internet session
    CInternetSession* m_pSession;
	// session timeOut
	DWORD		m_timeOut;

private:
	// keep track of current status
	CString		m_strURL;
	double		m_transferRate;
	UINT		m_Port;
	DWORD		m_infoStatusCode;
	CString		m_strServer;
	CString		m_serverSoft;
	CString		m_finalDir;
	int			m_lastSize;

	// keep track of actions
	int			m_linkFollowed;
	int			m_pixDownloaded;
	double		m_downloadedData;

	// did we find any pix at all?
	BOOL		m_isPixFound;

public:
	// has the thread been started once?
	BOOL m_hasStarted;
	// keep track of verbose mode
	BOOL		m_verboseMode;
	// this variable is used especially to finish the
	// functions in the thread when the user presses cancel
	// to avoid memory leaks when the main window is closed
	BOOL		m_isCanceled;
	// do we follow links?
	BOOL		m_isFollowLinks;
	int			m_maxFollowCount;

	// image and link array
	CStringArray m_Images;
	CStringArray m_Links;

	// picture types to download
	char		m_pixTypes;					// (A=all, G=gif only, J=jpg only)

    // picture min and max sizes
	int			m_maxSize;
	int			m_minSize;
};

