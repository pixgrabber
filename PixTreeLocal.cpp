//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeLocDlg
// File     :   PixTreeLocal.cpp
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class controls the appearance of the Local Tree.
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixGrabber.h"
#include "PixTreeLocal.h"
#include "PixTreeGlobal.h"
#include "PixGrabber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CPixTreeLocDlg dialog

IMPLEMENT_DYNAMIC(CPixTreeLocDlg, CDialog)

BEGIN_MESSAGE_MAP(CPixTreeLocDlg, CDialog)
		ON_WM_SIZE()
		ON_WM_SETFOCUS()
END_MESSAGE_MAP()


//----------------------------------------------------------------------------------
//  Function CPixTreeLocDlg
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Constructor
//
// @param	: CWnd* pParent, Pointer to the parent window of the object
// 
//----------------------------------------------------------------------------------
CPixTreeLocDlg::CPixTreeLocDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPixTreeLocDlg::IDD, pParent)
{
}

//----------------------------------------------------------------------------------
//  Function CPixTreeLocDlg
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Destructor
// 
//----------------------------------------------------------------------------------
CPixTreeLocDlg::~CPixTreeLocDlg()
{
}

//----------------------------------------------------------------------------------
//  Function DoDataExchange
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Called by the framework to exchange and validate dialog data.
// 
// @param	: CDataExchange* pDX, The pointer to a CDataExchange object. 
//
//----------------------------------------------------------------------------------
void CPixTreeLocDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_LOC, m_TreeLocFile);
}


//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Creates a tree object of download folder when the Dialog is initialised.
// 
// @return	: BOOL, TRUE if tree could be created.
//
//----------------------------------------------------------------------------------
BOOL CPixTreeLocDlg::OnInitDialog()
{
	//Create the tree view
	CRect r;
	if (!m_TreeLocFile.Create(WS_VISIBLE | WS_CHILD | TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT | TVS_EDITLABELS | TVS_SHOWSELALWAYS, r, this, 100))
		return -1;	

	//Set local Path
	CFileFind finder;
	if (finder.FindFile())
	{ 
		CString sPath;
		finder.FindNextFile();
		sPath = finder.GetFilePath();
		sPath.Remove('.');
		sPath.Format(sPath + "download");
		if(finder.FindFile(sPath))
			m_TreeLocFile.SetRootFolder(sPath);
		else
		{
			if (!SetCurrentDirectory(home))
			{
				AfxMessageBox("Error setting home Directory. Local view will not be generated correctly");
			}
			if (!SetCurrentDirectory("Download"))
			{
				if (::CreateDirectory("Download", 0))
				{
					if(finder.FindFile(sPath))
					{
						m_TreeLocFile.SetRootFolder(sPath);
					}
					else
					{
						AfxMessageBox("Error setting download Directory. Local view will not be generated correctly");
					}
			//		::ExitProcess(0);
				}
				else
				{
					AfxMessageBox("Error setting download Directory. Local view will not be generated correctly");
				}
			}
		//	sPath.Format("Local Path " + sPath + " does not exist");
		//	AfxMessageBox(sPath);
		}
	}

	return 0;
}


//----------------------------------------------------------------------------------
//  Function OnSize
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Resizes the Tree View area, when an OnSize message is received.
//			  The Tree always occupies the entire area Dialog area.
// 
// @param	: UINT nType, Specifies the type of resizing requested.
// @param	: int cx, Specifies the new width of the client area. 
// @param	: int cy, Specifies the new height of the client area. 
//
//----------------------------------------------------------------------------------
void CPixTreeLocDlg::OnSize(UINT nType, int cx, int cy) 
{
	if (m_TreeLocFile)
	{
	//Let the tree control occupy all the space of the client area
	CRect r;  
	GetClientRect(&r);
	m_TreeLocFile.MoveWindow(r);
	}
}

//----------------------------------------------------------------------------------
//  Function OnSetFocus
//----------------------------------------------------------------------------------
//
// @author  : MaEi
// @version : 1.0
//
// @task    : Resizes the Tab Dialogs, when an OnSize message is received.
//			  The Dialog always occupies the entire area
// 
// @param	: CWnd* pOldWnd, Contains the CWnd object that loses the input focus. 
//
//----------------------------------------------------------------------------------
void CPixTreeLocDlg::OnSetFocus(CWnd* pOldWnd) 
{
	m_TreeLocFile.SetFocus();
}

void CPixTreeLocDlg::SetParentHWnd(CWnd *parent)
{
	m_TreeLocFile.SetParentHWnd(parent);
}