//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CPixWizSub2
// File		:	PixWizSub2.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
// Descr	:	This class is used to handle the sub page 2 from the wizard.
//              Sub page 2 is used to get the an entered url back.
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "PixWizSub2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CPixWizSub2::CPixWizSub2(CWnd* pParent /*=NULL*/)
	: CNewWizPage(CPixWizSub2::IDD, pParent)
{
//	m_PixUrl = _T("");
}


void CPixWizSub2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ST_CAPTION, m_CaptionCtrl);
	DDX_Control(pDX, IDC_WIZURL, m_PixUrl);
	m_PixUrl.SetWindowText("");
}


BEGIN_MESSAGE_MAP(CPixWizSub2, CDialog)

END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to set a default value in the edit field (URL).
//			  It also set another font for the title
//
// @return  : TRUE if successfull, ....
//
//----------------------------------------------------------------------------------

BOOL CPixWizSub2::OnInitDialog() 
{
	CNewWizPage::OnInitDialog();
	
// Schrift vom Titel auf jedem Sub Window
//	m_Font.CreateFont(-16, 0, 0, 0, 
//		FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, 
//		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("MS Sans Serif"));

// Set Font for title
	m_CaptionCtrl.SetFont(&m_LargeFont, TRUE);

// Set default value for URL edit box
	CString url = "http://";
	SetDlgItemText(IDC_WIZURL, url);

	
	return TRUE;  // return TRUE unless you set the focus to a control
}


//----------------------------------------------------------------------------------
//  Function GetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to get the entered values (URL).
//
// @return  : TRUE if successfull, ....
//
//---------------------------------------------------------------------------------- 

CString CPixWizSub2::GetData()
{
	CString data;
	//this->GetDlgItemText(IDC_WIZURL, data);
	//this->GetDlgItem(IDC_WIZURL)->GetWindowText(data);
	m_PixUrl.GetWindowText(data);
	return (data);
}



//----------------------------------------------------------------------------------
//  Function OnWizardNext
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to check the edit field of a entered value, if nothing
//			  is entered, the function shows a message.
//
// @return 0 if successfull, ....
//
//----------------------------------------------------------------------------------

LRESULT CPixWizSub2::OnWizardNext()
{
	CString data;
	m_PixUrl.GetWindowText(data);
	
	if (data.GetLength() < 4)
	{
		AfxMessageBox("You have to enter a valid URL...");
		return -1;
	}
	//CNewWizPage::OnWizardNext();
	return 0;
}


//----------------------------------------------------------------------------------
//  Function SetData
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function has to be overwritten in this sub page. We do not set 
//            new values
// 
//
//----------------------------------------------------------------------------------

void CPixWizSub2::SetData(CString data)
{
	return;
}
