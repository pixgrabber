//-----------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixThumbView
// File		:	PixThumbView.h
// Author	:	Thierry "real" Haven
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once
#include "PixGrabberDoc.h"
#include "thumbnailcontrol.h"
#include "afxwin.h"

// Thumbnails dimensions
#define THUMB_WIDTH 240
#define THUMB_HEIGHT 180

//--[class]--------------------------------------------------------------
class CPixThumbView : public CFormView
{
	DECLARE_DYNCREATE(CPixThumbView)

//--[variables]----------------------------------------------------------
public:
	enum { IDD = IDD_THUMB };
	BOOL booted;

	CThumbNailControl m_cThumbFrame;	// custom control containing thumbnails
	CButton m_bdelete;					// delete button
	CEdit m_bcurrent;					// display current picture name & size
										// on top of thmbnail display

//--[construction]-------------------------------------------------------
protected:
	CPixThumbView();
public:
	virtual ~CPixThumbView();

//--[functions]----------------------------------------------------------
public:
	CPixGrabberDoc* GetDocument() const;
	void RefreshThumbDisplay();

//--[overrides]----------------------------------------------------------
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnSize(UINT nType, int cx, int cy);
	virtual void OnUpdate(CView *pSender, LPARAM lHint, CObject *pHint);
	void OnDataChange(void);
	virtual void DoDataExchange(CDataExchange* pDX);
	
//--[event handling]-----------------------------------------------------
protected:
    afx_msg LRESULT OnTnbLClicked( WPARAM wParam, LPARAM lParam );
    afx_msg LRESULT OnTnbRClicked( WPARAM wParam, LPARAM lParam );
	afx_msg void OnBnClickedDelete();
	DECLARE_MESSAGE_MAP()

//--[VC++ generated]-----------------------------------------------------
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//--[mfc stuff]----------------------------------------------------------
#ifndef _DEBUG  // debug version in PixGrabberView.cpp
inline CPixGrabberDoc* CPixThumbView::GetDocument() const
   { return reinterpret_cast<CPixGrabberDoc*>(m_pDocument); }
#endif

