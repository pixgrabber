//-----------------------------------------------------------------------
//	Project PixGrabber
//-----------------------------------------------------------------------
//
// CLASS	:	CThumbnailButton
// File		:	ThumbnailButton.cpp
// Author	:	Thierry "real" Haven
//				Based on Rex Fong rexfong@bac98.net control (www.codeproject.com)
//				New features implemented in this control thanks to PixGrabber are :
//				* Jpg and Gif loading
//				* New mouse handling
//				* Delete thumbnails on request
//				* Fixed scrollbar bugs
//				* Fixed message bug (SendMessageTimeout(HWND_BROADCAST...) didn't work
//				  for some reason..)
//				* Fixed display bug thanks to SetCapture / ReleaseCapture
//				-> still have to fix some update/display artifacts FIXME
// Date		:	09.12.2003
// Version	:	0.1
// Descr	:	This class represents a thumbnail button. Thumbnailbuttons
//				are attributes of a CThumbNailControl. A CThumbNailControl
//				may have many CThumbnailButton. A CThumbnailButton
//				is the "real" ThumbNail since it displays a tiny picture
//				preview (unlike the picture in CPixImgView which is much
//				bigger) and user can select it by clicking it.
//				Clicking a Thumbnailbuttons causes the CPixImgView to update
//				to the currently selected picture. See how
//				PreTranslateMessage() deals with mouse events.
//-----------------------------------------------------------------------
#include "stdafx.h"
#include <Commctrl.h>

#include "ThumbnailButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// used to register a new window class... CThumbnailButton is a custom control, remember !
#define CTHUMBNAILBUTTON_CLASSNAME _T( "CThumbnailButton" )
#define RGBTHUMBBKGD RGB( 230, 230, 230 )				// background

CBrush  CThumbnailButton::m_bkBrush;

//--[MFC message mapping]------------------------------------------------
BEGIN_MESSAGE_MAP(CThumbnailButton, CWnd)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

//--[constructor]--------------------------------------------------------
//-----------------------------------------------------------------------
//	CThumbnailButton constructor
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		Create a CThumbnailButton object
//
// @param :	CString			Picture file to be transformed into a thumbnail
// @param : int				position x of thumbnail in thumbnailcontrol
// @param : int				position y of thumbnail in thumbnailcontrol
// @param : CWnd *			pointer to thumbnailcontrol which owns the thumbnail.
//							It is used to send messages to this control.
//
//-----------------------------------------------------------------------
CThumbnailButton::CThumbnailButton( CString sPath, int cx, int cy, CWnd *powner )
{
	if( !RegisterWindowClass() )
		return;

	m_bTracking  = FALSE;
	m_bMouseClicked = FALSE;

	m_cX = cx;
	m_cY = cy;

	m_sFullpath = sPath;
	ExtractFilename( sPath );

	HBITMAP hbmp = (HBITMAP) LoadAnImage(sPath.GetBuffer(0), cx, cy);
	sPath.ReleaseBuffer();
		/*
		LoadImage( NULL, (LPCTSTR) m_sFullpath, 
		IMAGE_BITMAP, cx, cy, 
		LR_LOADFROMFILE | LR_CREATEDIBSECTION ) ;*/

	m_bmp.Attach( hbmp );
	
	owner = powner;

}

CThumbnailButton::~CThumbnailButton()
{
}

//-----------------------------------------------------------------------
//	overriden Function BOOL OnEraseBkgnd(CDC* pDC) 
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		Redraw bounding box in background, depending of the thumb position.
//
// @param  CDC* 			see msdn
// @return BOOL				see msdn
//
//-----------------------------------------------------------------------
BOOL CThumbnailButton::OnEraseBkgnd(CDC* pDC) 
{
	CRect rWnd;
	GetWindowRect( &rWnd );

	if( !m_bTracking )
		pDC->FillSolidRect( 1, 1, rWnd.Width()-1, rWnd.Height()-1, RGBTHUMBBKGD );
	else
		pDC->FillSolidRect( 1, 1, rWnd.Width()-1, rWnd.Height()-1, RGB(180,190,210) );

	CPen pen;
	CPen *pOPen;
	POINT pt;
	pt.x = 2;
	pt.y = 2;
	LOGPEN logPen;
	logPen.lopnStyle = PS_SOLID;
	logPen.lopnWidth = pt;

	if( !m_bTracking )
		logPen.lopnColor = RGB(33,33,33);
	else
		logPen.lopnColor = RGB(10,35,105);


	VERIFY( pen.CreatePenIndirect( &logPen ) );

	pOPen = pDC->SelectObject( &pen );

	pDC->MoveTo( 0, 0 );
	pDC->LineTo( rWnd.Width()-1, 0 );
	pDC->LineTo( rWnd.Width()-1, rWnd.Height()-1 );
	pDC->LineTo( 0, rWnd.Height()-1 );
	pDC->LineTo( 0, 0 );

	pDC->SelectObject( pOPen );
	pen.DeleteObject();

	return TRUE;
}

//-----------------------------------------------------------------------
//	overriden Function void OnPaint() 
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		Redraw the thumbnail content (picture & text) depending of the
//		thumb position.
//
//-----------------------------------------------------------------------
void CThumbnailButton::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	if( m_bmp.GetSafeHandle() != NULL )
	{
		// Prepare Font
		LOGFONT lf;
		memset( &lf, 0, sizeof(LOGFONT) );
		lf.lfHeight = 14;
		strcpy( lf.lfFaceName, _T("MS Serif") );

		dc.SetBkColor( RGBTHUMBBKGD );


		CFont font;
		CFont *pOFont;
		VERIFY( font.CreateFontIndirect( &lf ) );
		pOFont = dc.SelectObject( &font );

		// Draw Thumb Picture
		CRect rect;
		GetWindowRect( &rect );

		int nSX, nSY; // Starting point
		nSX = ( rect.Width() - m_cX ) / 2;
		nSY = ( rect.Height() - m_cY ) / 4;

		CBitmap *pOldbmp;

		CDC dcBmp;
		dcBmp.CreateCompatibleDC( &dc );
		pOldbmp = dcBmp.SelectObject( &m_bmp );

		dc.BitBlt( nSX, nSY, m_cX, m_cY, &dcBmp, 0, 0, SRCCOPY );

		//  Write ImageName
		TEXTMETRIC   tm ;
		dc.GetTextMetrics( &tm );

		// MAXIMUM Characters = 18
		int nSTX = (rect.Width() - ( tm.tmAveCharWidth * m_sFilename.Left(18).GetLength() ))/2;

		if( m_bTracking )
			dc.SetBkColor( RGB(180,190,210) );
		else
			dc.SetBkColor( RGBTHUMBBKGD );

		// MAXIMUM Characters = 18
		dc.TextOut( nSTX, nSY+m_cY+2, m_sFilename.Left(18) );

		dc.SelectObject( pOFont );
		dcBmp.SelectObject( pOldbmp );

		font.DeleteObject();
		dcBmp.DeleteDC();
	}
}


//-----------------------------------------------------------------------
//	Function BOOL RegisterWindowClass()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven / Rex Fong
// @version	: 0.1
//
// @task	:
//		Does some "administrative" work with its friend the windows OS.
//		Needed because this class is a custom control, so windows has to
//		know about it.
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CThumbnailButton::RegisterWindowClass()
{
	WNDCLASS wndcls;
	HINSTANCE hInst = AfxGetInstanceHandle();

	if (!(::GetClassInfo(hInst, CTHUMBNAILBUTTON_CLASSNAME, &wndcls)))
	{
		// otherwise we need to register a new class
		m_bkBrush.CreateSolidBrush( RGBTHUMBBKGD );

		wndcls.style            = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
		wndcls.lpfnWndProc      = ::DefWindowProc;
		wndcls.cbClsExtra       = wndcls.cbWndExtra = 0;
		wndcls.hInstance        = hInst;
		wndcls.hIcon            = NULL;
		wndcls.hCursor          = AfxGetApp()->LoadStandardCursor(IDC_ARROW);
		wndcls.hbrBackground    = (HBRUSH) m_bkBrush.GetSafeHandle();
		wndcls.lpszMenuName     = NULL;
		wndcls.lpszClassName    = CTHUMBNAILBUTTON_CLASSNAME;

		if (!AfxRegisterClass(&wndcls))
		{
			AfxThrowResourceException();
			return FALSE;
		}

	}

	return TRUE;
}

//-----------------------------------------------------------------------
//	overriden Function BOOL PreTranslateMessage(MSG* pMsg) 
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Handle mouse moves and update thumb display.
//		If mouse was clicked, a custom message (see .h) is sent
//		thanks to SendMessageTimeout.
//
// @param  MSG* 			see msdn
// @return BOOL				see msdn
//
//-----------------------------------------------------------------------
BOOL CThumbnailButton::PreTranslateMessage(MSG* pMsg) 
{
	CRect rect;
	GetWindowRect( &rect );
	GetParent()->ScreenToClient( &rect );

	switch( pMsg->message )
	{
		case WM_RBUTTONDBLCLK:
		{
			if( ::IsWindow(GetParent()->m_hWnd) )
			{
				DWORD dwResult = NULL;
				SendMessageTimeout(
					/*HWND_BROADCAST*/owner->m_hWnd, UWM_ON_TNB_RCLICKED, (WPARAM)this, (LPARAM)0,
					SMTO_ABORTIFHUNG|SMTO_NORMAL, 2000, &dwResult );
			}
		}
		return TRUE;
		break;

	case WM_LBUTTONDBLCLK:

		return TRUE;
		break;

	case WM_LBUTTONDOWN:
		{
			this->SetCapture();
			if( !m_bMouseClicked && m_bTracking )
			{
				m_bMouseClicked = TRUE;
				MoveWindow( rect.left+2, rect.top+2, rect.Width(), rect.Height() );
			}

			if( ::IsWindow(GetParent()->m_hWnd) )
			{
				DWORD dwResult = NULL;
				SendMessageTimeout(
					/*HWND_BROADCAST*/owner->m_hWnd, UWM_ON_TNB_LCLICKED, (WPARAM)this, (LPARAM)0,
					SMTO_ABORTIFHUNG|SMTO_NORMAL, 2000, &dwResult );
			}

		}
		return TRUE;
		break;

	case WM_MOUSELEAVE:
		{
			if( !m_bMouseClicked )
			{
			//	MoveWindow( rect.left+2, rect.top+2, rect.Width(), rect.Height() );
				Invalidate();
			}

			m_bMouseClicked = FALSE;
			m_bTracking     = FALSE;
		}
		return TRUE;
		break;

	case WM_LBUTTONUP:
		{
			if( m_bMouseClicked && m_bTracking )
			{
				m_bMouseClicked = FALSE;

				MoveWindow( rect.left-2, rect.top-2, rect.Width(), rect.Height() );
			}
			::ReleaseCapture();
		}

		return TRUE;
		break;

	case WM_MOUSEMOVE:
		{
			if( !m_bTracking )
			{
				m_bMouseClicked = FALSE;

				// Track Mouse Movement
				TRACKMOUSEEVENT tme;
				memset( &tme, 0, sizeof(TRACKMOUSEEVENT) );
				tme.cbSize = sizeof(TRACKMOUSEEVENT);
				tme.dwFlags = TME_LEAVE;
				tme.hwndTrack = m_hWnd ;
				m_bTracking = _TrackMouseEvent( &tme );

			//	MoveWindow( rect.left-2, rect.top-2, rect.Width(), rect.Height() );
				Invalidate();
			}
		}
		return TRUE;
		break;

	default:
		//TRACE( "%s MSG: %d\n", (LPCTSTR)m_sFilename, pMsg->message );
		break;
	}


	return CWnd::PreTranslateMessage(pMsg);
}

#if 0
LPCTSTR CThumbnailButton::GetBmpName() const		// DELETE_ME?
{
	return ((LPCTSTR) m_sImgName);
}
#endif

//-----------------------------------------------------------------------
//	Function BOOL CThumbnailButton::IsValid()
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		Tests of handle of the picture is valid
//
// @return BOOL				TRUE is success
//
//-----------------------------------------------------------------------
BOOL CThumbnailButton::IsValid() const
{
	return ( m_bmp.GetSafeHandle() != NULL ) ? TRUE : FALSE;
}

//-----------------------------------------------------------------------
//	Function void GetFullpath()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Handle mouse moves and update thumb display.
//		If mouse was clicked, a custom message (see .h) is sent
//		thanks to SendMessageTimeout.
//
// @param  MSG* 			see msdn
// @return BOOL				see msdn
//
//-----------------------------------------------------------------------
const CString& CThumbnailButton::GetFullpath() const
{

	return m_sFullpath;
}

//-----------------------------------------------------------------------
//	Function void ExtractFilename(const CString &sPath)
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		Extract file name from given path
//
// @param  CString&			given path
//
//-----------------------------------------------------------------------
void CThumbnailButton::ExtractFilename(const CString &sPath)
{
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];

	CString sTmpPath = sPath;
	LPTSTR lptstr = sTmpPath.GetBufferSetLength( _MAX_PATH );

	_splitpath( lptstr, drive, dir, fname, ext );

	m_sFilename = CString(fname) + CString(ext);

}

//-----------------------------------------------------------------------
//	Function void ResetTrackFlag( void )
//-----------------------------------------------------------------------
//
// @author	: Rex Fong
// @version	: 0.1
//
// @task	:
//		Reset track flag caused 
//
// @param  CString&			given path
//
//-----------------------------------------------------------------------
void CThumbnailButton::ResetTrackFlag( void )
{
	m_bTracking = FALSE;
	TRACKMOUSEEVENT tme;  // tracking information

	memset( &tme, 0, sizeof(TRACKMOUSEEVENT) );
	tme.cbSize = sizeof(TRACKMOUSEEVENT);
	tme.dwFlags = TME_CANCEL;
	tme.hwndTrack = m_hWnd;

	_TrackMouseEvent(  &tme  );
	Invalidate();
}

//-----------------------------------------------------------------------
//	Function void GetFullpath()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Loads an image and returns a handle to a bitmap !
//
// @param  char* 			File path & name or just name...
// @param  int				image position x on thumbnail
// @param  int				image position y on thumbnail
// @return HBITMAP			handle to a bitmap
//
//-----------------------------------------------------------------------
HBITMAP CThumbnailButton::LoadAnImage(char* FileName, int cx, int cy)
{
	// Use IPicture stuff to use JPG / GIF files
	IPicture* p;
	IStream* s;
	//IPersistStream* ps;
	HGLOBAL hG;
	void* pp;
	FILE* fp;


	// Read file in memory
	fp = fopen(FileName,"rb");  // FIXME < would be better to use readfile win32 func.
	if (!fp) return NULL;

	fseek(fp,0,SEEK_END);
	int fs = ftell(fp);
	fseek(fp,0,SEEK_SET);
	hG = GlobalAlloc(GPTR,fs);
	if (!hG)
	{
		fclose(fp);
		return NULL;
	}
	pp = (void*)hG;
	fread(pp,1,fs,fp);
	fclose(fp);

	// Create an IStream so IPicture can 
	CreateStreamOnHGlobal(hG,false,&s);
	if (!s)
	{
		GlobalFree(hG);
		return NULL;
	}

	OleLoadPicture(s,0,false,IID_IPicture,(void**)&p);

	if (!p)
	{
		s->Release();
		GlobalFree(hG);
		return NULL;
	}
	s->Release();
	GlobalFree(hG);

	HBITMAP hB = 0;
	p->get_Handle((unsigned int*)&hB);

	// Copy the image. Necessary, because upon p's release,
	// the handle is destroyed.
	HBITMAP hBB = (HBITMAP)CopyImage(hB,IMAGE_BITMAP,cx,cy,
		LR_COPYRETURNORG);

	p->Release();
	return hBB;
}
