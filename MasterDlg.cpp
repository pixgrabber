//----------------------------------------------------------------------------------
//	Project PixGrabber
//----------------------------------------------------------------------------------
//
// CLASS	:	CMasterDlg
// File		:	MasterDlg.cpp
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
//
//----------------------------------------------------------------------------------

#include "stdafx.h"
#include "MasterDlg.h"
#include "NewWizPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CMasterDlg::CMasterDlg(CWnd* pParent /*=NULL*/)
	: CNewWizDialog(CMasterDlg::IDD, pParent)
{
		// NOTE: the ClassWizard will add member initialization here
}

void CMasterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
		// NOTE: the ClassWizard will add DDX and DDV calls here
}

BEGIN_MESSAGE_MAP(CMasterDlg, CNewWizDialog)

END_MESSAGE_MAP()



//----------------------------------------------------------------------------------
//  Function OnInitDialog
//----------------------------------------------------------------------------------
//
// @author  : isc03meal
// @version : 1.0
//
// @task    : This function is used to initalize the dialog
//
// @return  : TRUE
//----------------------------------------------------------------------------------

BOOL CMasterDlg::OnInitDialog() 
{
	SetPlaceholderID(IDC_SHEETRECT);
	CNewWizDialog::OnInitDialog();
	return TRUE; 
}

