//-----------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPicture
// File		:	Picture.h
// Author	:	Thierry "real" Haven
//				Based on Paul DiLascia MSDN Magazine sample, October 2001
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------

#pragma once
#include <atlbase.h>

//--[class]--------------------------------------------------------------
class CPicture
{

//--[variables]----------------------------------------------------------
protected:
	CComQIPtr<IPicture>m_spIPicture;			 // ATL smart pointer to IPicture
	HRESULT m_hr;								 // last error code

//--[construction]-------------------------------------------------------
public:
	CPicture();
	~CPicture();

//--[functions]-------------------------------------------------------
	BOOL Load(UINT nIDRes);
	BOOL Load(LPCTSTR pszPathName);
	BOOL Load(CFile& file);
	BOOL Load(CArchive& ar);
	BOOL Load(IStream* pstm);
	BOOL Render(CDC* pDC, CRect rc=CRect(0,0,0,0),
		LPCRECT prcMFBounds=NULL, CRect client=NULL) const;
	CSize GetImageSize(CDC* pDC=NULL) const;

	operator IPicture*()
	{
		return m_spIPicture;
	}

	void GetHIMETRICSize(OLE_XSIZE_HIMETRIC& cx, OLE_YSIZE_HIMETRIC& cy) const
	{
		cx = cy = 0;
		const_cast<CPicture*>(this)->m_hr = m_spIPicture->get_Width(&cx);
		ASSERT(SUCCEEDED(m_hr));
		const_cast<CPicture*>(this)->m_hr = m_spIPicture->get_Height(&cy);
		ASSERT(SUCCEEDED(m_hr));
	}
	void Free()
	{
		if (m_spIPicture)
		{
			m_spIPicture.Release();
		}
	}
};
