//----------------------------------------------------------------------------------
//  Project PixGrabber
//----------------------------------------------------------------------------------
//
// File     :   PixTreeCtrl.h
// Author   :   MaEi
//
//----------------------------------------------------------------------------------
#pragma once
#include "PixGrabberDoc.h"

#define ON_TREE_LCLICKED _T( "WM_ON_TREE_LCLICKED_{DA90C85B-217C-4a91-8686-0C555559C67D}" )

static const UINT UWM_ON_TREE_LCLICKED = ::RegisterWindowMessage( ON_TREE_LCLICKED );

//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeCtrl
// File     :   PixTreeCtrl.h
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   Access to system icons to display in the tree.
//
//----------------------------------------------------------------------------------
class CSystemImageList
{
public:
//Constructors / Destructors
	CSystemImageList();
	~CSystemImageList();

//Methods
	CImageList& GetImageList();

protected:
	CImageList m_ImageList;
	HIMAGELIST hSystemImageList;

	static int m_nRefCount;
};



//----------------------------------------------------------------------------------
//
// CLASS    :   CPixTreeCtrl
// File     :   PixTreeCtrl.h
// Author   :   MaEi
// Date     :   25.11.2003
// Version  :   1.0
// Descr    :   This class implements a tree control for the entire file system.
//
//----------------------------------------------------------------------------------
class CPixTreeCtrl : public CTreeCtrl
{
public:
	CPixTreeCtrl();
	virtual ~CPixTreeCtrl();

	CString   ItemToPath(HTREEITEM hItem);
	CString   GetSelectedPath();
	HTREEITEM SetSelectedPath(const CString& sPath, BOOL bExpanded=FALSE);
	BOOL IsFile(HTREEITEM hItem);
	BOOL IsFolder(HTREEITEM hItem);
	BOOL IsDrive(HTREEITEM hItem);
	BOOL IsDrive(const CString& sPath);
	void UpOneLevel();
	void SetRootFolder(const CString& sPath);
	CString GetSelectedFolder();
    CString GetRootFolder() const { return m_sRootFolder; };

public:
	void SetParentHWnd(CWnd *wnd);

//Debug / Assert help
#ifdef _DEBUG
	//virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	afx_msg void FileProperties();
 	afx_msg void FileRename();
 	afx_msg void FileDelete();
	afx_msg void OnViewRefresh();
	afx_msg void OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLclick(NMHDR* pNMHDR, LRESULT* pResult); 
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnItemselected(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnPopulateTree(WPARAM wParam, LPARAM lParam); 
	DECLARE_MESSAGE_MAP()

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PreSubclassWindow();

	void DisplayPath(const CString& sPath, HTREEITEM hParent, BOOL bUseSetRedraw=TRUE);
	void DisplayDrives(HTREEITEM hParent, BOOL bUseSetRedraw=TRUE);
	HTREEITEM InsertDriveItem(CString& sDrive, HTREEITEM hParent);
	int GetIconIndex(const CString& sFilename);
	int GetSelIconIndex(const CString& sFilename);
	BOOL HasGotSubEntries(const CString& sDirectory);
	BOOL HasChildWithText(HTREEITEM hParent, const CString& sText);
	HTREEITEM InsertFileItem(const CString& sFile, const CString& sPath, HTREEITEM hParent);
	HTREEITEM FindSibling(HTREEITEM hParent, const CString& sItem);
	BOOL IsDropSource(HTREEITEM hItem);
	HTREEITEM GetDropTarget(HTREEITEM hItem);
	void EndDragging(BOOL bCancel);
	HTREEITEM CopyItem(HTREEITEM hItem, HTREEITEM htiNewParent, HTREEITEM htiAfter = TVI_LAST);
	HTREEITEM CopyBranch(HTREEITEM htiBranch, HTREEITEM htiNewParent, HTREEITEM htiAfter = TVI_LAST);
	static int CompareByFilenameNoCase(CString& element1, CString& element2) ;

	CSystemImageList m_SysImageList;
	CString m_sRootFolder;
	BOOL	m_bShowFiles;
	BOOL	m_bRootFolderSet;
	HTREEITEM	m_hItemDrag;
	HTREEITEM	m_hItemDrop;
	CImageList* m_pilDrag;
	UINT	m_nTimerID;
	HCURSOR m_DropCopyCursor;
	HCURSOR m_NoDropCopyCursor;
	HCURSOR m_DropMoveCursor;
	HCURSOR m_NoDropMoveCursor;
	UINT	m_TimerTicks;
	CWnd	*m_parentwnd;		// hwnd of the parent view
	CString	 m_selected;			// caption of selected item
	static int	m_nImListCount;


};
