//-----------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CThumbnailButton
// File		:	ThumbnailButton.h
// Author	:	Thierry "real" Haven
//				Based on Rex Fong rexfong@bac98.net control (www.codeproject.com)
//				New features implemented in this control thanks to PixGrabber are :
//				* Jpg and Gif loading
//				* New mouse handling
//				* Delete thumbnails on request
//				* Fixed scrollbar bugs
//				* Fixed message bug (SendMessageTimeout(HWND_BROADCAST...) didn't work
//				  for some reason..)
//				* Fixed display bug thanks to SetCapture / ReleaseCapture
//				-> still have to fix some update/display artifacts FIXME
//				
// Date		:	9 dec 2003
// Version	:	0.1
//-----------------------------------------------------------------------


#pragma once

// we define 2 strings for the custom message definition
#define ON_TNB_LCLICKED _T( "WM_ON_TNB_LCLICKED_{D190C85B-217C-4a91-8684-0C523559C67D}" )
#define ON_TNB_RCLICKED _T( "WM_ON_TNB_RCLICKED_{0BBF61D0-8379-4470-B30A-C11854B9938C}" )

// custom message definition. Those messages are send
static const UINT UWM_ON_TNB_LCLICKED = ::RegisterWindowMessage( ON_TNB_LCLICKED );
static const UINT UWM_ON_TNB_RCLICKED = ::RegisterWindowMessage( ON_TNB_RCLICKED );

//--[class]--------------------------------------------------------------
class CThumbnailButton : public CWnd
{
//--[variables]----------------------------------------------------------
public:
	static CBrush  m_bkBrush;

private:
	BOOL    m_bTracking;
	BOOL    m_bMouseClicked;
	CString m_sFilename;

	CBitmap m_bmp;

	int     m_cX;     // Image Width
	int     m_cY;     // Image Height

	CString m_sFullpath;

	CWnd *owner;

//--[construction]-------------------------------------------------------
public:
	CThumbnailButton( CString sPath, int cx, int cy, CWnd * );
	virtual ~CThumbnailButton();

//--[functions]----------------------------------------------------------
public:
	void  ResetTrackFlag( void );
	const CString& GetFullpath( void ) const;
	BOOL IsValid( void ) const;

private:
	void ExtractFilename( const CString& sPath );
	BOOL RegisterWindowClass(void);
	HBITMAP LoadAnImage(char* FileName, int cx, int cy);


//--[overrides]----------------------------------------------------------
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);


//--[event handling]-----------------------------------------------------
protected:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	DECLARE_MESSAGE_MAP()
};

