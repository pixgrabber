//----------------------------------------------------------------------------------
//
// Project PixGrabber
//
// CLASS	:	CPixWizSub3
// File		:	PixWizSub3.h
// Author	:	isc03meal
// Date		:	02.12.2003
// Version	:	1.0
//
//----------------------------------------------------------------------------------
#pragma once

#include "NewWizPage.h"
#include "resource.h"

class CPixWizSub3 : public CNewWizPage
{
// Construction
public:
	CPixWizSub3(CWnd* pParent = NULL);   // standard constructor
	void SetData(CString data);
	CString GetData();
// Dialog Data
	enum { IDD = IDD_WIZSUB3 };
	CStatic	m_CaptionCtrl;
	CString	m_strLicense;
	CString m_pixType;
	CFont m_Font;

// Overrides
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	LRESULT OnWizardNext();
	void OnSetActive();
	BOOL OnKillActive();
	virtual BOOL OnInitDialog();
	afx_msg void OnPixcheckall();
	DECLARE_MESSAGE_MAP()
};