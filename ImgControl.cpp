//-----------------------------------------------------------------------
//	Project PixGrabber
//-----------------------------------------------------------------------
//
// CLASS	:	CImgControl
// File		:	ImgControl.cpp
// Author	:	Thierry "real" Haven
// Date		:	09.12.2003
// Version	:	0.1
// Descr	:	This class defines the functionalities of a custom control
//				called CImgControl. This control is able to display pictures
//				(indeed CPicture objects) in its client area. It will also
//				zoom or resize pictures in order to fit in its client area,
//				depending of the settings.
//				
//-----------------------------------------------------------------------

#include "stdafx.h"

#include <math.h>

#include "ImgControl.h"
#include "PixGrabberDoc.h"

#define ID_VIEW_TOFIT		10
#define ID_VIEW_SMARTSIZE	20
#define ID_VIEW_OTHER		30

#define ID_VIEW25 0x11223345		// not used anymore (zoom 25%) DELETE_ME?
#define ID_VIEW33 0x11223346		// not used anymore (zoom 33%) DELETE_ME?
#define ID_VIEW50 0x11223347		// not used anymore (zoom 50%) DELETE_ME?
#define ID_VIEW75 0x11223348		// not used anymore (zoom 75%) DELETE_ME?

#define SBAR_NOT_CHANGED -1

#define RGBTHUMBCTRLBKGD RGB( 250, 50, 50 )			// debug, clean screen in this color
#define RGBTHUMBCTRLBORDER RGB( 50, 250, 50 )		// debug, clean screen in this color

#define CImgControl_CLASSNAME _T("CImgControl")		// DO NOT CHANGE OR APP WONT WORK !!!!
													// (kind of mfc convention)


//--[MFC message mapping]------------------------------------------------
BEGIN_MESSAGE_MAP(CImgControl, CWnd)
	ON_WM_PAINT()
	ON_WM_HSCROLL()		// scroll bar
	ON_WM_VSCROLL()		// scroll bar
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

//--[constructor]--------------------------------------------------------
CBrush  CImgControl::m_bkBrush;
CImgControl::CImgControl()
{
	m_iHowScale = ID_VIEW_TOFIT;		// default zoom is "fit to client area"

	extern char home[256];
	char popup[256];
	lstrcpy(popup, home);
	lstrcat(popup, "\\splash.jpg");		// load nice splash background when starting app
	m_ppic = new CPicture();
	m_ppic->Load(popup);
	

	m_sx = m_sy = m_rex = m_rey = (long)0.0f;
	zoom = 0L;
	scrolled = FALSE;					// picture was not scrolled yet

	m_smartmode = SMART_DIM_ALWAYS;		// smart resize mode (see .h for details)

	if( !RegisterWindowClass() )		// custom control needs this
		return;
}

CImgControl::~CImgControl()
{
	delete(m_ppic);
}

//-----------------------------------------------------------------------
//	Function BOOL RegisterWindowClass()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Does some "administrative" work with its friend the windows OS.
//		Needed because this class is a custom control, so windows has to
//		know about it.
//
//	@return :		TRUE if success
//
//-----------------------------------------------------------------------
BOOL CImgControl::RegisterWindowClass()
{
	CBrush m_bkBrush;
	WNDCLASS wndcls;
	HINSTANCE hInst = AfxGetInstanceHandle();

	if (!(::GetClassInfo(hInst, CImgControl_CLASSNAME, &wndcls)))
	{
		// otherwise we need to register a new class
		m_bkBrush.CreateSolidBrush( RGBTHUMBCTRLBKGD );

		wndcls.style            = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
		wndcls.lpfnWndProc      = ::DefWindowProc;
		wndcls.cbClsExtra       = wndcls.cbWndExtra = 0;
		wndcls.hInstance        = hInst;
		wndcls.hIcon            = NULL;
		wndcls.hCursor          = AfxGetApp()->LoadStandardCursor(IDC_ARROW);
		wndcls.hbrBackground    = NULL;//(HBRUSH) m_bkBrush.GetSafeHandle();
		wndcls.lpszMenuName     = NULL;
		wndcls.lpszClassName    = CImgControl_CLASSNAME;

		if (!AfxRegisterClass(&wndcls))
		{
			AfxThrowResourceException();
			return FALSE;
		}
	}
	return TRUE;
}

//-----------------------------------------------------------------------
//	Function void LoadPic(CPicture pppic)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Display a picture after copying parameter picture into member variable.
//		We should pass only a pointer to save some memory FIXME !
//
// @param: CPicture pppic			picture to display
//
//-----------------------------------------------------------------------
void CImgControl::LoadPic(CPicture *pppic)
{
	if (pppic == NULL) return;

	delete(m_ppic);
	m_ppic = new CPicture();
	*m_ppic = *pppic;
}

//-----------------------------------------------------------------------
//	Function void UpdateScroll(int px, int py)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		We update the scroll datas. We need to keep track of those scrolling
//		values in order to display the picture correctly, depending on 
//		"how-much-it-was-scrolled". Values m_sx/m_sy will be used to calculate 
//		the picture position.
//
// @param: int px					scroll on axis x (horizontal)
// @param: int py					scroll on axis y (vertical)
//
//-----------------------------------------------------------------------
void CImgControl::UpdateScroll(int px, int py)
{
	if (px == SBAR_NOT_CHANGED) this->m_sy = py;
	else if (py == SBAR_NOT_CHANGED) this->m_sx = px;
	else 
	{
		this->m_sy = py;		// actual y scroll
		this->m_sx = px;		// actual x scroll
	}
	this->Invalidate();	// DELETE_ME?
	scrolled = TRUE;			// flag we set. If true we don't redraw black background
								// This disable flickering when srolling pics.
}

//-----------------------------------------------------------------------
//	Function void SmartResize(DWORD pmode)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Switch to "smart resize" display mode. See .h for more details.
//
// @param: DWORD pmode					smart mode to enable (again, see .h)
//
//-----------------------------------------------------------------------
void CImgControl::SmartResize(DWORD pmode)
{
	this->m_iHowScale = ID_VIEW_SMARTSIZE;
	this->m_smartmode = pmode;
}

//-----------------------------------------------------------------------
//	Function void UpdateZoom(int value)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Change the zoom value.
//
// @param: int value					can be positive or negative value.
//										positive value will make "zoom in"
//										and negative "zoom out"
//										Zero mean zoom is reinitialized to
//										1:1 aspect ratio.
//										Values are cumulated in the zoom member
//										variable.
//
//-----------------------------------------------------------------------
void CImgControl::UpdateZoom(int value)
{
//	m_iHowScale = ID_VIEW_OTHER;	// DELETE_ME?
	switch (value)
	{
		case 0:
			zoom = 0;				// reset zoom if value is 0
		break;
		default:
			zoom += value;
		break;
	}
	this->Invalidate();				// is there some flickering? not sure (FIXME)

	CSize sz = m_ppic->GetImageSize();
	CSize bz = sz;	// backup
	if (zoom > 0)
	{
		sz.cx = (long)((double)sz.cx * (zoom + 1));
		sz.cy = (long)((double)sz.cy * (zoom + 1));
	}
	else if (zoom < 0)
	{
		sz.cx = (long)((double)sz.cx / ((-zoom) + 1));
		sz.cy = (long)((double)sz.cy / ((-zoom) + 1));
	}
	m_rex = sz.cx;					// update x and y resolution depending of the zoom
	m_rey = sz.cy;
}

//-----------------------------------------------------------------------
//	overriden Function OnPaint()
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Change the zoom value.
//
// @param: int value			Our beloved/famous OnPaint funtion.
//								Here we draw the picture afer querying its
//								properties (see GetImageRect() below).
//								Its dimensions/properties depend of zoom and
//								scroll values.
//
//-----------------------------------------------------------------------
void CImgControl::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	if (m_ppic)
	{
		CRect rclient;				// client rect (ctrl)
		this->GetClientRect(rclient);
		CBrush brush(RGB(0,0,0)); // black
		hrect = rect;				// backup the old shape of the picture
		this->GetImageRect(rect);
		if ((!scrolled) || (m_iHowScale == ID_VIEW_SMARTSIZE)) dc.FillRect(rclient, &brush);

		if (hrect != rect)			// only redraw the 2 needed areas
		{
			if ((hrect.bottom > rect.bottom) || (hrect.right > rect.right))
			{
				CBrush brush(RGB(0,0,0)); // other colours show the optimization when resizing
				CRect updaterect1(CPoint(rect.left, rect.bottom), CPoint(hrect.right, hrect.bottom));			// 
				dc.FillRect(updaterect1, &brush);
				CBrush brush2(RGB(0,0,0)); // black
				CRect updaterect2(CPoint(rect.right, rect.top), CPoint(hrect.right, rect.bottom));			// 
				dc.FillRect(updaterect2, &brush2);
			}
		}
	/*	rclient.top -= zoom;					// DELETE_ME?
		rclient.bottom += zoom;
		rclient.left -= zoom;
		rclient.right += zoom;*/

		// debug : show actual resizing.
	/*	char arf[256];
		wsprintf(arf, "x:%d y:%d\n%d %d", rect.left, rect.right, rect.top, rect.bottom);
		::MessageBox(0, arf, 0,0);*/
		m_ppic->Render(&dc,rect, NULL, rclient);
	}
	scrolled = FALSE;
}

//-----------------------------------------------------------------------
//	overriden Function SetFit(BOOL fit)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		Enable/Disable fit-to-client-area mode.
//
// @param: BOOL fit			If true, set fit-to-client-area mode.
//							Picture will be resized and aspect ratio
//							will certainly not be X:1 or 1:1.
//
//-----------------------------------------------------------------------
void CImgControl::SetFit(BOOL fit)
{
	if (fit) m_iHowScale = ID_VIEW_TOFIT;
	else m_iHowScale = 0;
}

//-----------------------------------------------------------------------
//	overriden Function GetImageRect(CRect& rc)
//-----------------------------------------------------------------------
//
// @author	: Thierry "real" Haven
// @version	: 0.1
//
// @task	:
//		This function performs all resizing/zoom operations depending on
//		members variables values.
//
// @param: CRect& rc		Image properties to fill.
//							We have to set up image borders.
//							If borders have negative values,
//							the picture will be zoomed or scrolled, see below.
//
//-----------------------------------------------------------------------
void CImgControl::GetImageRect(CRect& rc)
{
	if (m_ppic)
	{
		if (m_iHowScale==ID_VIEW_TOFIT)		// fit to client area mode
		{
			this->GetClientRect(&rc);		// image will have client dimensions
		}
		else if (m_iHowScale==ID_VIEW_SMARTSIZE)	// smart size mode (see .h for details)
		{
			int cx, cy;		// client size: x, y
			CSize ps;		// real picture size
			CRect client;	// client area
			int px, py;		// picture size: x, y
			int sx, sy;		// for centering image
			float ratio;	// when resize x(y) or y(x), keep current ratio so
							// picture is not morphed

			this->zoom = 0;	// we don't want a zoom here

			this->GetClientRect(client);

			cx = client.right - client.left;
			cy = client.bottom - client.top;
			ps = m_ppic->GetImageSize();
			px = ps.cx;
			py = ps.cy;
			sx = sy = 0;

			if (px > py)		// picture width > its height?
			{
				// picture.x > client area.x ? then we resize.
				// we resize also if we are in SMART_DIM_ALWAYS display mode.
				if ((px > cx) || (this->m_smartmode == SMART_DIM_ALWAYS))
				{
					ratio = (float)((float)cx / (float)px);	// keep track of aspect ratio
					m_rex = cx;
					m_rey = (long)(py * ratio);	// we resize while keeping aspect ratio
					if (m_rey > cy)
					{
						ratio = (float)((float)cy / (float)py);
						m_rey = cy;
						m_rex = (long)(px * ratio);	// we resize while keeping aspect ratio
					}
				}
			}
			else				// almost the same as before
			{
				// picture.y > client area.y ? then we resize.
				// we resize also if we are in SMART_DIM_ALWAYS display mode.
				if ((py > cy) || (this->m_smartmode == SMART_DIM_ALWAYS))
				{
					ratio = (float)((float)cy / (float)py);
					m_rey = cy;
					m_rex = (long)(px * ratio);
					if (m_rex > cx)
					{
						ratio = (float)((float)cx / (float)px);
						m_rex = cx;
						m_rey = (long)(py * ratio);
					}
				}
			}
			// center the picture in the client area
			sx = (cx - m_rex) / 2;
			m_rex += sx;
			sy = (cy - m_rey) / 2;
			m_rey += sy;

			// we record the changes
			rc.SetRect(sx, sy, m_rex, m_rey);
		}
		else		// "normal" 1:1 mode, or X:X if zoom != 0
		{
			CSize sz = m_ppic->GetImageSize();
			CSize bz = sz;	// backup				DELETE_ME?

			if (zoom > 0)		// zoom in
			{
				sz.cx = (long)((double)sz.cx * (zoom + 1));
				sz.cy = (long)((double)sz.cy * (zoom + 1));
			}
			else if (zoom < 0)	// zoom out
			{
				sz.cx = (long)((double)sz.cx / ((-zoom) + 1));
				sz.cy = (long)((double)sz.cy / ((-zoom) + 1));
			}
			m_rex = sz.cx;
			m_rey = sz.cy;
			// we record the changes
			rc.SetRect(-m_sx, -m_sy, sz.cx-m_sx, sz.cy-m_sy);
		}
	}
}
